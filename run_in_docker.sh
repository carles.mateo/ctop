#!/bin/bash

s_DOCKER_IMAGE_NAME="ctop"

echo "We will run the Docker Container and name it: ${s_DOCKER_IMAGE_NAME}"
echo "Please, note that you should have built the image first by using the supplied script: build_in_docker.sh"
echo ""

echo "Running ${s_DOCKER_IMAGE_NAME} Docker Container as a single instance (unique name)"
sudo docker run --name ${s_DOCKER_IMAGE_NAME} ${s_DOCKER_IMAGE_NAME}
