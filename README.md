# ctop.py v.0.8.9

An Open Source utility written in Python 3, by Carles Mateo.

Last update: 2022 July 3th.

Is Compatible with Python 2 since version 0.8.0. (Please check doc/changelog.txt as there are some issues)

https://blog.carlesmateo.com/2020/01/12/ctop/

ctop.py is an Open Source tool for Linux System Administration that I’ve written in Python3. It uses only the System (/proc), and not third party libraries, in order to get all the information required.
I use only this modules, so it’s ideal to run in all the farm of Servers and Dockers:

    os
    sys
    time
    shutil (for getting the Terminal width and height, as it auto-rescales. Not used in Python 2)

The purpose of this tool is to help to troubleshot and to identify problems with a single view to a single tool that has all the typical indicators.

It provides in a single view information that is typically provided by many programs:

    top, htop for the CPU usage, process list, memory usage
    meminfo
    cpuinfo
    hostname
    uptime
    df to see the free space in / and the free inodes
    iftop to see real-time bandwidth usage
    ip addr list to see the main Ip for the interfaces
    netstat or lsof to see the list of listening TCP Ports
    uname -a to see the Kernel version
    
Other cool things it does is:

    Identifying if you’re inside an Amazon EC2 Instance, Google Cloud GCP, OpenStack, Virtual Box, Docker or lxc container.
    Tell you what network interfaces are up and what are down.
    Uses colors, and marks in yellow the warnings and in red the errors, problems like few disk space reaming or high CPU usage according to the available cores and CPUs.
    Redraws the screen and adjust to the size of the Terminal, bigger terminal displays more information
    It doesn’t use external libraries, and does not escape to shell. It reads everything from /proc /sys or /etc files.
    Identifies the Linux distribution
    Shows the most repeated binaries, so you can identify DDoS attacks (like having 5,000 apache instances where you have normally 500 or many instances of Python)
    Indicates if an interface has the cable connected or disconnected
    Shows the Speed of the Network Connection (useful for Mellanox cards than can operate at 200Gbit/sec, 100, 50, 40, 25, 10…)
    It displays the local time and the Linux Epoch Time, which is universal (very useful for logs and to detect when there was an issue, for example if your system restarted, your SSH Session would keep latest Epoch captured)
    No root required.
    Shows errors in the logs, like NFS timeouts, process killed due to lack of Memory... 

Limitations:

    It only works for Linux, not for Mac or for Windows. Although the idea is to help with Server’s Linux Administration and Troubleshot.
    The list of processes of the System is read every 30 seconds, to avoid adding much overhead on the System.

I decided to code name the version 0.7 as “Catalan Republic” to support the dreams and hopes and democratic requests of the Catalans people to become and independent Republic.

I created this tool as Open Source and if you want to help I need people to test under different versions of:

    Atypical Linux distributions
    
My typical tests run in:

    Ubuntu, different versions PC, VirtualBox, Docker, RaspBerry Pi 4 and 3, Cloud: GCP, Amazon
    CentOS, different versions
    Amazon EC2 Linux

If you are a Cloud Provider and want me to implement the detection of your VMs, so the tool knows that is a instance of the Amazon, Google, Azure, Cloudsigma, Digital Ocean… contact me through my LinkedIn.

If you want me to add a Feature, and support economically my work, contact me at my LinkedIn: https://www.linkedin.com/in/carlesmateo/
