#
# Tests for ArgUtils class
#
# Author: Carles Mateo
# Creation Date: 2015 in Barcelona, by Carles Mateo
# Last Update: 2020-08-14 15:24 IST in Ireland, by Carles Mateo
#

import pytest
import os
import sys

s_path_program = os.path.dirname(__file__)
sys.path.append(s_path_program + '../')
sys.path.append(s_path_program + '../src/ctop/lib/')

from lib.argsutils import ArgsUtils


class TestArgsUtils(object):

    o_argutils = None
    a_s_args = ["test_argutils", "-n=10", "--iterations=11"]
    a_s_args2 = ["test_argutils", "-h"]

    def init_dependencies(self):
        self.o_argutils = ArgsUtils(self.a_s_args)

    def test_constructor_with_no_parameters(self):
        self.o_argutils = ArgsUtils([])

    def test_constructor_with_parameters_no_equal(self):
        self.o_argutils = ArgsUtils(self.a_s_args2)

    def test_get_args_for_cmdline(self):
        self.init_dependencies()

        l_a_s_args = self.o_argutils.get_dictionary_of_args()

        assert l_a_s_args == {"-n": "10", "--iterations": "11"}

    def test_get_arg_existing(self):
        self.init_dependencies()

        b_exists, s_arg_n = self.o_argutils.get_arg("-n")

        assert b_exists is True
        assert s_arg_n == "10"

    def test_get_arg_not_existing(self):
        self.init_dependencies()

        b_exists, s_arg = self.o_argutils.get_arg("not-existing-one")

        assert b_exists is False
        assert s_arg == ""

    def test_get_arg_from_list_found(self):
        self.init_dependencies()

        b_exists, s_arg = self.o_argutils.get_arg_from_list(a_s_args=["-n", "--iterations"])

        assert b_exists is True
        assert s_arg == "10"

        b_exists, s_arg = self.o_argutils.get_arg_from_list(a_s_args=["-u"], s_default="unknown")
        assert b_exists is False
        assert s_arg == "unknown"

    def test_get_arg_from_list_not_found(self):
        self.init_dependencies()

        b_found, s_value = self.o_argutils.get_arg_from_list(['--not_found', '-z'], "")
        assert b_found is False
        assert s_value == ""

    def test_get_list_of_args(self):
        self.init_dependencies()

        a_s_args = self.o_argutils.get_list_of_args()
        assert a_s_args == self.a_s_args
