#
# Tests for File class
#
# Author: Carles Mateo
# Creation Date: 2015 in Barcelona, by Carles Mateo
# Last Update: 2021-04-05 06:00 Ireland, by Carles Mateo
#

import pytest
import os
from lib.fileutils import FileUtils


@pytest.fixture
def open_raises_ioerror(monkeypatch):
    def mock_test_helper(o_self):
        raise IOError

    monkeypatch.setattr(FileUtils, 'test_helper', mock_test_helper)


class TestFile(object):

    s_path_folder = "/tmp/"
    s_path_test_file = "/tmp/test_file.txt"
    s_path_test_file_binary = "/tmp/test.bin"
    s_sample_file_text = "For testing purposes\nA second line\n# Comment\npath=/tmp\n" + \
                         "Random accents àéíîöòóç@~$%&Ç*\nFinal line"
    s_binary_content = chr(11) + chr(20) + chr(30) + "ABC" + chr(200) + chr(210) + chr(220) + chr(255)
    by_content = bytes([11, 20, 30, 65, 66, 67, 200, 210, 220, 255])

    s_path_folder_fail = "/this-has-to-fail"
    s_path_test_file_wrong_path = "/tmp-not-existing/test_file.txt"
    s_path_test_file_not_existing = "/tmp/not_real_file.txt"

    def create_sample_file(self):
        """
        It creates a sample file. It is used for certain tests.
        :return:
        """
        o_file = FileUtils()

        b_success = o_file.write(self.s_path_test_file, self.s_sample_file_text)

    def create_sample_binary_file(self):
        """
        It creates a sample file with binary contents.
        """

        o_file = FileUtils()

        b_success = o_file.write_binary(self.s_path_test_file_binary, self.by_content)

    def delete_sample_file(self):
        """
        Cleans up the sample file
        """
        o_file = FileUtils()

        o_file.delete(self.s_path_test_file)

    # Start Tests
    def test_append_ok(self):
        o_file = FileUtils()
        b_result = o_file.append("/tmp/test_file.txt", self.s_sample_file_text)

        assert b_result is True

    def test_append_ko(self):
        o_file = FileUtils()
        b_result = o_file.append("/not_existing/test_file.txt", self.s_sample_file_text)

        assert b_result is False

    def test_append_ioerror_exception(self, open_raises_ioerror):
        o_file = FileUtils()
        b_success = o_file.append(self.s_path_test_file, self.s_sample_file_text)

        assert b_success is False

    def test_write_ok(self):
        o_file = FileUtils()
        b_result = o_file.write(self.s_path_test_file, self.s_sample_file_text)

        assert b_result is True

    def test_write_ko(self):
        o_file = FileUtils()
        b_result = o_file.write("/not_existing/test_file.txt", self.s_sample_file_text)

        assert b_result is False

    def test_write_binary_ok(self):
        o_file = FileUtils()
        b_result = o_file.write_binary(self.s_path_test_file_binary, self.by_content)

        assert b_result is True

    def test_read_as_string_ok(self):
        o_file = FileUtils()
        b_result, s_text = o_file.read_file_as_string(self.s_path_test_file)

        assert b_result is True
        assert s_text == self.s_sample_file_text

    def test_read_file_as_string_ko(self):
        o_file = FileUtils()
        b_result, s_text = o_file.read_file_as_string(self.s_path_test_file_not_existing)

        assert b_result is False

    def test_read_file_as_string(self):
        o_file = FileUtils()
        b_result, s_text = o_file.read_file_as_string(self.s_path_test_file)

        assert b_result is True

    def test_get_read_binary(self):
        o_file = FileUtils()

        self.create_sample_binary_file()

        b_result, a_binary_content = o_file.read_binary(self.s_path_test_file_binary)
        assert b_result is True

        s_text_content = ""
        for i_char in a_binary_content:
            if i_char > 40 and i_char < 123:
                s_text_content += chr(i_char)

        assert s_text_content == "ABC"

    def test_get_read_binary_ioerror(self, open_raises_ioerror):
        o_file = FileUtils()

        b_result, a_binary_content = o_file.read_binary(self.s_path_test_file_binary)
        assert b_result is False

        assert len(a_binary_content) == 0

    def test_read_config_file_values(self):
        o_file = FileUtils()

        self.create_sample_file()

        b_success, d_s_config_values = o_file.read_config_file_values(self.s_path_test_file)

        assert b_success is True
        assert d_s_config_values["path"] == "/tmp"

        self.delete_sample_file()

        b_success, d_s_config_values = o_file.read_config_file_values(self.s_path_test_file)

        assert b_success is False
        assert len(d_s_config_values) == 0

    def test_strings(self):
        o_file = FileUtils()

        self.create_sample_binary_file()

        b_success, s_string = o_file.strings(self.s_path_test_file_binary)

        assert b_success is True
        assert s_string == "ABC"

    def test_folder_exists(self):

        o_file = FileUtils()

        b_result = o_file.folder_exists(self.s_path_folder)
        assert b_result is True

        b_result = o_file.folder_exists(self.s_path_folder)
        assert b_result is True

        b_result = o_file.folder_exists(self.s_path_folder_fail)
        assert b_result is False

    def test_folder_exists_ioerror(self, open_raises_ioerror):

        o_file = FileUtils()

        b_result = o_file.folder_exists(self.s_path_folder_fail)
        assert b_result is False

    def test_file_exists(self):
        o_file = FileUtils()

        b_result = o_file.file_exists(self.s_path_test_file_not_existing)
        assert b_result is False

        # create file to test for existing file
        o_file.write(self.s_path_test_file, "File for testing")

        b_result = o_file.file_exists(self.s_path_test_file)
        assert b_result is True

        # Clean up the file
        o_file.delete(self.s_path_test_file)

    def test_file_exists_ioerror(self, open_raises_ioerror):
        o_file = FileUtils()

        b_result = o_file.file_exists(self.s_path_test_file_not_existing)
        assert b_result is False

    def test_create_folder_ok(self):
        s_path_folder = "/tmp/test"

        o_file = FileUtils()

        b_result = o_file.create_folder(s_path_folder)
        assert b_result is True

        b_result = o_file.remove_folder(s_path_folder)
        assert b_result is True

    def test_create_folder_ko(self):
        s_path_folder = "/tmp/test"

        o_file = FileUtils()

        s_path_folder = "Ç://///this-should-give-exception-catched"
        b_result = o_file.create_folder(s_path_folder)
        assert b_result is False

    def test_path_exists(self):
        s_path = "/tmp"

        o_file = FileUtils()

        b_result = o_file.path_exists(s_path)
        assert b_result is True

        # test with file
        s_path = s_path + "/file.txt"
        o_file.write(s_path, "File for testing")

        b_result = o_file.path_exists(s_path)
        assert b_result is True

        # test with bad path
        s_path = "/idontexist"
        b_result = o_file.path_exists(s_path)
        assert b_result is False

    def test_path_exists_ioerror(self, open_raises_ioerror):

        o_file = FileUtils()

        b_result = o_file.path_exists(self.s_path_folder_fail)
        assert b_result is False

    def test_list_dir(self):
        s_path = "/tmp"

        o_file = FileUtils()

        # create a file for the test
        filename = "testfile"
        o_file.write(s_path + "/" + filename, "File for testing")

        b_success, a_files = o_file.list_dir(s_path)

        assert b_success is True
        assert filename in a_files

        # test with bad path
        s_path = "/idontexist"

        b_success, a_files = o_file.list_dir(s_path)
        assert b_success is False
        assert len(a_files) == 0

    def test_get_real_path(self):
        s_path = "/tmp"

        o_file = FileUtils()

        # create a file for the test
        filename = s_path + "/testfile"
        o_file.write(s_path + "/" + filename, "File for testing")

        # create a symbolic link
        s_sym_link = s_path + "/testsymlink"
        if not o_file.path_exists(s_sym_link):
            os.symlink(filename, s_sym_link)

        b_success, s_real_path = o_file.get_real_path(s_sym_link)
        assert b_success is True
        assert s_real_path == filename

        # try with non symlink path
        b_success, s_real_path = o_file.get_real_path(filename)
        assert b_success is True
        assert s_real_path == filename

        # try with bad path
        b_success, s_real_path = o_file.get_real_path("/tmp/idontexist")
        assert b_success is False

    def test_get_real_path_ioerror(self, open_raises_ioerror):
        s_path = "/tmp"

        o_file = FileUtils()

        s_sym_link = s_path + "/testsymlink"

        b_success, s_real_path = o_file.get_real_path(s_sym_link)
        assert b_success is False
        assert s_real_path == ""

    def test_get_all_with_mask(self):
        o_file = FileUtils()

        s_mask = "/etc/*nam*"

        b_success, l_files = o_file.get_all_files_or_directories_with_mask(s_mask)

        assert b_success is True
        assert len(l_files) > 0

    def test_get_all_with_mask_ioerror(self, open_raises_ioerror):
        o_file = FileUtils()

        s_mask = "/etc/*nam*"

        b_success, l_files = o_file.get_all_files_or_directories_with_mask(s_mask)

        assert b_success is False
        assert len(l_files) == 0

    def test_read_file_as_lines(self):
        # Create dummy file for testing
        self.create_sample_file()

        o_file = FileUtils()

        b_success, a_s_lines = o_file.read_file_as_lines(self.s_path_test_file)

        assert b_success is True
        assert len(a_s_lines) > 0
        assert isinstance(a_s_lines[0], str)

        self.delete_sample_file()

    def test_read_file_as_lines_ioerror(self, open_raises_ioerror):
        o_file = FileUtils()

        b_success, a_s_lines = o_file.read_file_as_lines(self.s_path_test_file)

        assert b_success is False
        assert len(a_s_lines) == 0

    def test_write_if_changed(self, monkeypatch):
        o_file = FileUtils()

        # test with file that does not exist, the file should be created
        b_written, s_err = o_file.write_if_changed(self.s_path_test_file, self.s_sample_file_text)
        assert b_written is True
        assert s_err == ""

        # test with same contents
        b_written, s_err = o_file.write_if_changed(self.s_path_test_file, self.s_sample_file_text)
        assert b_written is False
        assert s_err == "The contents are the same."

        # test with updated contents
        b_written, s_err = o_file.write_if_changed(self.s_path_test_file, self.s_sample_file_text + "extra content")
        assert b_written is True
        assert s_err == ""

        # test with read failure
        def mock_read(s_file_name):
            return False, ""
        monkeypatch.setattr(o_file, 'read_file_as_string', mock_read)

        b_written, s_err = o_file.write_if_changed(self.s_path_test_file, self.s_sample_file_text)
        assert b_written is False
        assert s_err == "Unable to read File."

        self.delete_sample_file()

    def test_delete_ko(self):
        o_file = FileUtils()
        b_result = o_file.delete("/not_existing/test_file.txt")

        assert b_result is False

        b_result = o_file.delete("/tmp/not_existing_test_file.txt")

        assert b_result is False

    def test_delete_ok(self):
        self.create_sample_file()

        o_file = FileUtils()
        b_result = o_file.delete(self.s_path_test_file)

        assert b_result is True

    def test_delete_ioerror_exception(self, open_raises_ioerror):
        self.create_sample_file()

        o_file = FileUtils()
        b_result = o_file.delete(self.s_path_test_file)

        assert b_result is False

    def test_delete_with_mask(self):
        self.create_sample_file()

        o_file = FileUtils()
        b_success, i_files_deleted = o_file.delete_all_with_mask(self.s_path_test_file)

        assert b_success is True
        assert i_files_deleted == 1

    def test_delete_with_mask_ioerror(self, open_raises_ioerror):
        self.create_sample_file()

        o_file = FileUtils()
        b_success, i_files_deleted = o_file.delete_all_with_mask(self.s_path_test_file_wrong_path)

        assert b_success is False
        assert i_files_deleted == 0
