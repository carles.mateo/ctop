#
# Tests for CTop main program
#
# Author: Carles Mateo
# Creation Date: 2021-04-04 18:30 Ireland Time
# Last Update:   2022-02-16 18:50 Ireland Time
#
# If you launch manually, launch it from ctop/src/ctop with this command:
# python3 -m pytest ../../tests/test_ctop.py


import pytest
import os
import sys

s_path_program = os.path.dirname(__file__)

sys.path.append(s_path_program + '../')
sys.path.append(s_path_program + '../src/ctop/')

from src.ctop import ctop


class TestCTop(object):

    def test_main_iterations_3(self):
        a_parameters = ["ctop.py", "-n=3"]

        with pytest.raises(SystemExit) as pytest_wrapped_e:
            ctop.main(a_test_params=a_parameters)

        assert pytest_wrapped_e.type == SystemExit
        assert pytest_wrapped_e.value.code == 0

    def test_main_iterations_2(self):
        a_parameters = ["ctop.py", "-n=2"]

        with pytest.raises(SystemExit) as pytest_wrapped_e:
            ctop.main(a_test_params=a_parameters)

        assert pytest_wrapped_e.type == SystemExit
        assert pytest_wrapped_e.value.code == 0

    def test_main_iterations_1(self):
        a_parameters = ["ctop.py", "-n=1"]

        with pytest.raises(SystemExit) as pytest_wrapped_e:
            ctop.main(a_test_params=a_parameters)

        assert pytest_wrapped_e.type == SystemExit
        assert pytest_wrapped_e.value.code == 0

    def test_show_version(self, capsys):
        a_parameters = ["ctop.py", "-v"]

        with pytest.raises(SystemExit) as pytest_wrapped_e:
            ctop.main(a_test_params=a_parameters)

        assert pytest_wrapped_e.type == SystemExit
        assert pytest_wrapped_e.value.code == 0

        o_captured = capsys.readouterr()
        s_version_fragment = "v."

        assert s_version_fragment in o_captured.out
        assert o_captured.err == ""

    def test_show_version_extended(self, capsys):
        a_parameters = ["ctop.py", "--version"]

        with pytest.raises(SystemExit) as pytest_wrapped_e:
            ctop.main(a_test_params=a_parameters)

        assert pytest_wrapped_e.type == SystemExit
        assert pytest_wrapped_e.value.code == 0

        o_captured = capsys.readouterr()
        s_version_fragment = "v."

        assert s_version_fragment in o_captured.out
        assert o_captured.err == ""

    def test_show_help(self, capsys):
        a_parameters = ["ctop.py", "-h"]

        with pytest.raises(SystemExit) as pytest_wrapped_e:
            ctop.main(a_test_params=a_parameters)

        assert pytest_wrapped_e.type == SystemExit
        assert pytest_wrapped_e.value.code == 0

        o_captured = capsys.readouterr()
        s_help_fragment = """CTOP.py An Open Source Systems Utility by Carles Mateo

Arguments:

 Short   Long
 =====   ====
"""

        assert s_help_fragment in o_captured.out
        assert o_captured.err == ""

    def test_show_help_extended(self, capsys):
        a_parameters = ["ctop.py", "--help"]

        with pytest.raises(SystemExit) as pytest_wrapped_e:
            ctop.main(a_test_params=a_parameters)

        assert pytest_wrapped_e.type == SystemExit
        assert pytest_wrapped_e.value.code == 0

        o_captured = capsys.readouterr()
        s_help_fragment = """CTOP.py An Open Source Systems Utility by Carles Mateo

Arguments:

 Short   Long
 =====   ====
"""

        assert s_help_fragment in o_captured.out
        assert o_captured.err == ""
