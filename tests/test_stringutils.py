#
# Tests for StringUtils class
#
# Author: Carles Mateo
# Creation Date: 2014-01-01
# Last Update: 2021-05-22
#

import pytest
from lib.stringutils import StringUtils


class TestStringUtils(object):

    def test_convert_integer_to_string_thousands(self):

        o_stringutils = StringUtils()

        s_number_formatted = o_stringutils.convert_integer_to_string_thousands(i_number=1000, s_thousand_sep=",")

        assert s_number_formatted == "1,000"

        s_number_formatted = o_stringutils.convert_integer_to_string_thousands(i_number=1000000, s_thousand_sep=",")

        assert s_number_formatted == "1,000,000"

        s_number_formatted = o_stringutils.convert_integer_to_string_thousands(i_number=-100, s_thousand_sep=",")

        assert s_number_formatted == "-100"

        s_number_formatted = o_stringutils.convert_integer_to_string_thousands(i_number=-1000000, s_thousand_sep=",")

        assert s_number_formatted == "-1,000,000"

    def test_convert_kb_to_gb(self):
        o_stringutils = StringUtils()

        s_gb = o_stringutils.convert_kb_to_gb("1000kb", b_add_units=True)
        assert s_gb == "0.00GB"

        s_gb = o_stringutils.convert_kb_to_gb("0", b_add_units=True)
        assert s_gb == "0GB"

        s_gb = o_stringutils.convert_kb_to_gb("1000kb", b_add_units=False)
        assert s_gb == "0.00"
        
        s_gb = o_stringutils.convert_kb_to_gb("0", b_add_units=False)
        assert s_gb == "0"

        s_gb = o_stringutils.convert_kb_to_gb("1000000000kb", b_add_units=False)
        assert s_gb == "953.67"

        # Weird request, malformed
        s_gb = o_stringutils.convert_kb_to_gb("1234", b_add_units=False)
        assert s_gb == "1234"

    def test_convert_bytes_to_best_size(self):
        o_stringutils = StringUtils()

        s_best_unit = o_stringutils.convert_bytes_to_best_size(10)
        assert s_best_unit == "10Bytes"

        s_best_unit = o_stringutils.convert_bytes_to_best_size(1)
        assert s_best_unit == "1Byte"

        s_best_unit = o_stringutils.convert_bytes_to_best_size(0)
        assert s_best_unit == "0Bytes"

        s_best_unit = o_stringutils.convert_bytes_to_best_size(1024)
        assert s_best_unit == "1.00KB"

        s_best_unit = o_stringutils.convert_bytes_to_best_size(1024*1024)
        assert s_best_unit == "1.00MB"

        s_best_unit = o_stringutils.convert_bytes_to_best_size(1024*1024*1024)
        assert s_best_unit == "1.00GB"

        s_best_unit = o_stringutils.convert_bytes_to_best_size(1000*1024*1024*1024)
        assert s_best_unit == "1000GB"

        s_best_unit = o_stringutils.convert_bytes_to_best_size(1024*1024*1024*1024)
        assert s_best_unit == "1.00TB"

        s_best_unit = o_stringutils.convert_bytes_to_best_size(1024*1024*1024*1024*1024)
        assert s_best_unit == "1.00PB"

        s_best_unit = o_stringutils.convert_bytes_to_best_size(5*1024*1024*1024*1024*1024)
        assert s_best_unit == "5.00PB"

    def test_convert_string_to_integer_ok(self):
        o_stringutils = StringUtils()

        b_success, i_number = o_stringutils.convert_string_to_integer("1000")
        assert b_success is True
        assert i_number == 1000

    def test_convert_string_to_integer_value_empty_string(self):
        o_stringutils = StringUtils()

        b_success, i_number = o_stringutils.convert_string_to_integer("")
        assert b_success is True
        assert i_number == 0

    def test_convert_string_to_integer_ko(self):
        o_stringutils = StringUtils()

        b_success, i_number = o_stringutils.convert_string_to_integer("1000a")
        assert b_success is False
        assert i_number == 0

    def test_convert_to_multiple_units(self):
        # , s_amount, b_add_units=False, i_decimals=2, b_remove_decimals_if_ge_1000=True
        o_stringutils = StringUtils()

        # Case 0 Bytes
        s_value_bytes, s_value_kb, s_value_mb, s_value_gb, s_value_tb, s_value_pb, s_biggest_suggested = \
            o_stringutils.convert_to_multiple_units(s_amount="0", b_add_units=True, i_decimals=2, b_remove_decimals_if_ge_1000=True)
        assert s_value_bytes == "0Bytes"
        assert s_value_kb == "0.00KB"
        assert s_value_mb == "0.00MB"
        assert s_value_gb == "0.00GB"
        assert s_value_tb == "0.00TB"
        assert s_value_pb == "0.00PB"
        assert s_biggest_suggested == s_value_bytes

        # Case 1 Byte
        s_value_bytes, s_value_kb, s_value_mb, s_value_gb, s_value_tb, s_value_pb, s_biggest_suggested = \
            o_stringutils.convert_to_multiple_units(s_amount="1", b_add_units=True, i_decimals=2, b_remove_decimals_if_ge_1000=True)
        assert s_value_bytes == "1Byte"
        assert s_value_kb == "0.00KB"
        assert s_value_mb == "0.00MB"
        assert s_value_gb == "0.00GB"
        assert s_value_tb == "0.00TB"
        assert s_value_pb == "0.00PB"
        assert s_biggest_suggested == s_value_bytes

        s_value_bytes, s_value_kb, s_value_mb, s_value_gb, s_value_tb, s_value_pb, s_biggest_suggested = \
            o_stringutils.convert_to_multiple_units(s_amount="1024", b_add_units=True, i_decimals=2, b_remove_decimals_if_ge_1000=True)
        assert s_value_bytes == "1024Bytes"
        assert s_value_kb == "1.00KB"
        assert s_value_mb == "0.00MB"
        assert s_value_gb == "0.00GB"
        assert s_value_tb == "0.00TB"
        assert s_value_pb == "0.00PB"
        assert s_biggest_suggested == s_value_kb

        # Test > 1000.00 show as 1000
        s_value_bytes, s_value_kb, s_value_mb, s_value_gb, s_value_tb, s_value_pb, s_biggest_suggested = \
            o_stringutils.convert_to_multiple_units(s_amount="1048576", b_add_units=True, i_decimals=2, b_remove_decimals_if_ge_1000=True)
        assert s_value_bytes == "1048576Bytes"
        assert s_value_kb == "1024KB"
        assert s_value_mb == "1.00MB"
        assert s_value_gb == "0.00GB"
        assert s_value_tb == "0.00TB"
        assert s_value_pb == "0.00PB"
        assert s_biggest_suggested == s_value_mb

        # 1024*1024*1024*1024*1024 = 1125899906842624
        s_value_bytes, s_value_kb, s_value_mb, s_value_gb, s_value_tb, s_value_pb, s_biggest_suggested = \
            o_stringutils.convert_to_multiple_units(s_amount="1125899906842624", b_add_units=True, i_decimals=2, b_remove_decimals_if_ge_1000=True)
        assert s_value_bytes == "1125899906842624Bytes"
        assert s_value_kb == "1099511627776KB"
        assert s_value_mb == "1073741824MB"
        assert s_value_gb == "1048576GB"
        assert s_value_tb == "1024TB"
        assert s_value_pb == "1.00PB"
        assert s_biggest_suggested == s_value_pb

        # 7*1024*1024*1024*1024*1024 = 7881299347898368
        s_value_bytes, s_value_kb, s_value_mb, s_value_gb, s_value_tb, s_value_pb, s_biggest_suggested = \
            o_stringutils.convert_to_multiple_units(s_amount="7881299347898368", b_add_units=True, i_decimals=2, b_remove_decimals_if_ge_1000=True)
        assert s_value_bytes == "7881299347898368Bytes"
        assert s_value_kb == "7696581394432KB"
        assert s_value_mb == "7516192768MB"
        assert s_value_gb == "7340032GB"
        assert s_value_tb == "7168TB"
        assert s_value_pb == "7.00PB"
        assert s_biggest_suggested == s_value_pb

        # 7000*1024*1024*1024*1024*1024 = 7881299347898368
        s_value_bytes, s_value_kb, s_value_mb, s_value_gb, s_value_tb, s_value_pb, s_biggest_suggested = \
            o_stringutils.convert_to_multiple_units(s_amount="7881299347898368000", b_add_units=True, i_decimals=2, b_remove_decimals_if_ge_1000=True)
        assert s_value_bytes == "7881299347898368000Bytes"
        assert s_value_kb == "7696581394432000KB"
        assert s_value_mb == "7516192768000MB"
        assert s_value_gb == "7340032000GB"
        assert s_value_tb == "7168000TB"
        assert s_value_pb == "7000PB"
        assert s_biggest_suggested == s_value_pb

        # 7000*1024*1024*1024*1024*1024 = 7881299347898368
        s_value_bytes, s_value_kb, s_value_mb, s_value_gb, s_value_tb, s_value_pb, s_biggest_suggested = \
            o_stringutils.convert_to_multiple_units(s_amount="7881299347898368000", b_add_units=True, i_decimals=2, b_remove_decimals_if_ge_1000=False)
        assert s_value_bytes == "7881299347898368000Bytes"
        assert s_value_kb == "7696581394432000.00KB"
        assert s_value_mb == "7516192768000.00MB"
        assert s_value_gb == "7340032000.00GB"
        assert s_value_tb == "7168000.00TB"
        assert s_value_pb == "7000.00PB"
        assert s_biggest_suggested == s_value_pb

        s_value_bytes, s_value_kb, s_value_mb, s_value_gb, s_value_tb, s_value_pb, s_biggest_suggested = \
            o_stringutils.convert_to_multiple_units(s_amount="1KB", b_add_units=True, i_decimals=2, b_remove_decimals_if_ge_1000=True)
        assert s_value_bytes == "1024Bytes"
        assert s_value_kb == "1.00KB"
        assert s_value_mb == "0.00MB"
        assert s_value_gb == "0.00GB"
        assert s_value_tb == "0.00TB"
        assert s_value_pb == "0.00PB"
        assert s_biggest_suggested == s_value_kb

        # Test KIB
        s_value_bytes, s_value_kb, s_value_mb, s_value_gb, s_value_tb, s_value_pb, s_biggest_suggested = \
            o_stringutils.convert_to_multiple_units(s_amount="1KIB", b_add_units=True, i_decimals=2, b_remove_decimals_if_ge_1000=True)
        assert s_value_bytes == "1024Bytes"
        assert s_value_kb == "1.00KB"
        assert s_value_mb == "0.00MB"
        assert s_value_gb == "0.00GB"
        assert s_value_tb == "0.00TB"
        assert s_value_pb == "0.00PB"
        assert s_biggest_suggested == s_value_kb

    def test_get_dict_value(self):

        o_stringutils = StringUtils()

        d_dict_for_the_test = {"exists": "yes"}

        b_found, s_value = o_stringutils.get_dict_value(s_key="exists", d_dict=d_dict_for_the_test, m_default="")
        assert b_found is True
        assert s_value == "yes"

        b_found, s_value = o_stringutils.get_dict_value(s_key="exists", d_dict=d_dict_for_the_test, m_default="default")
        assert b_found is True
        assert s_value == "yes"

        b_found, s_value = o_stringutils.get_dict_value(s_key="does-not-exists", d_dict=d_dict_for_the_test, m_default="")
        assert b_found is False
        assert s_value == ""

        b_found, s_value = o_stringutils.get_dict_value(s_key="does-not-exists-too", d_dict=d_dict_for_the_test, m_default="default")
        assert b_found is False
        assert s_value == "default"

    def test_get_bytes_per_second(self):

        o_stringutils = StringUtils()

        i_bytes_sec, s_units_sec = o_stringutils.get_bytes_per_second(1000, 2)
        assert i_bytes_sec == 500
        assert s_units_sec == "500Bytes"

        i_bytes_sec, s_units_sec = o_stringutils.get_bytes_per_second(1024, 2)
        assert i_bytes_sec == 512
        assert s_units_sec == "512Bytes"

        i_bytes_sec, s_units_sec = o_stringutils.get_bytes_per_second(2048, 2)
        assert i_bytes_sec == 1024
        assert s_units_sec == "1.00KB"

        i_bytes_sec, s_units_sec = o_stringutils.get_bytes_per_second(2049, 2)
        assert i_bytes_sec == 1024
        assert s_units_sec == "1.00KB"

        i_bytes_sec, s_units_sec = o_stringutils.get_bytes_per_second(20480, 2)
        assert i_bytes_sec == 10240
        assert s_units_sec == "10.00KB"

        i_bytes_sec, s_units_sec = o_stringutils.get_bytes_per_second(2048*1000, 2)
        assert i_bytes_sec == 1024000
        assert s_units_sec == "1000KB"

        i_bytes_sec, s_units_sec = o_stringutils.get_bytes_per_second(2*1024*1024, 2)
        assert i_bytes_sec == 1048576
        assert s_units_sec == "1.00MB"

    def test_get_bytes_per_second_zero_seconds(self):
        o_stringutils = StringUtils()

        i_bytes_sec, s_units_sec = o_stringutils.get_bytes_per_second(1000, 0)
        assert i_bytes_sec == 1000
        assert s_units_sec == "1000Bytes"

    def test_get_percent(self):

        o_stringutils = StringUtils()

        f_percent_free, i_percent_free, s_percent_decimals, s_percent_no_decimals = o_stringutils.get_percent(i_partial=10,
                                                                                                              i_total=100,
                                                                                                              b_add_percent=True)

        assert f_percent_free == 10
        assert i_percent_free == 10
        assert s_percent_decimals == "10.0%"
        assert s_percent_no_decimals == "10%"

        f_percent_free, i_percent_free, s_percent_decimals, s_percent_no_decimals = o_stringutils.get_percent(i_partial=11,
                                                                                                              i_total=33,
                                                                                                              b_add_percent=True)
        # 33.33333333..6
        assert f_percent_free > 33.33 and f_percent_free < 33.34
        assert i_percent_free == 33
        assert s_percent_decimals == "33.33%"
        assert s_percent_no_decimals == "33%"

        f_percent_free, i_percent_free, s_percent_decimals, s_percent_no_decimals = o_stringutils.get_percent(i_partial=0,
                                                                                                              i_total=0,
                                                                                                              b_add_percent=True)

        assert f_percent_free == 0
        assert i_percent_free == 0
        assert s_percent_decimals == "100%"
        assert s_percent_no_decimals == "100%"

    def test_get_time_and_bytes_per_second(self):

        o_stringutils = StringUtils()

        f_time, s_time, i_bytes_per_second, s_best_unit = o_stringutils.get_time_and_bytes_per_second(i_bytes=1000,
                                                                                                      f_time_start=1643982635.0,
                                                                                                      f_time_finish=1643982645.1)

        assert f_time > 10.0 and f_time <= 10.1
        assert s_time == "10.1"
        assert i_bytes_per_second == 99
        assert s_best_unit == "99Bytes"

        f_time, s_time, i_bytes_per_second, s_best_unit = o_stringutils.get_time_and_bytes_per_second(i_bytes=1000000,
                                                                                                      f_time_start=1643982635.0,
                                                                                                      f_time_finish=1643982645.1)

        assert f_time > 10.0 and f_time <= 10.1
        assert s_time == "10.1"
        assert i_bytes_per_second == 99009
        assert s_best_unit == "96.69KB"
