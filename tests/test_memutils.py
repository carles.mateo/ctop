#
# Tests for MemUtils class
#
# Author: Carles Mateo
# Creation Date: 2022-05-25 in Ireland, by Carles Mateo
# Last Update: 2022-05-27 11:37 Ireland, by Carles Mateo
#

import pytest
import os
from lib.fileutils import FileUtils
from lib.stringutils import StringUtils
from lib.memutils import MemUtils


class TestMemUtils(object):

    s_content_proc_meminfo = \
"""MemTotal:       65639012 kB
MemFree:         2837536 kB
MemAvailable:    6291188 kB
Buffers:         1209868 kB
Cached:          8753908 kB
SwapCached:            0 kB
Active:         30979152 kB
Inactive:        8098196 kB
Active(anon):   29115936 kB
Inactive(anon):  6329360 kB
Active(file):    1863216 kB
Inactive(file):  1768836 kB
Unevictable:          64 kB
Mlocked:              64 kB
SwapTotal:            10 kB
SwapFree:              1 kB
"""

    # Return as SwapTotal 1GB + 1 byte
    s_content_proc_meminfo2 = \
"""MemTotal:       65639012 kB
MemFree:         2837536 kB
MemAvailable:    6291188 kB
Buffers:         1209868 kB
Cached:          8753908 kB
SwapCached:            0 kB
Active:         30979152 kB
Inactive:        8098196 kB
Active(anon):   29115936 kB
Inactive(anon):  6329360 kB
Active(file):    1863216 kB
Inactive(file):  1768836 kB
Unevictable:          64 kB
Mlocked:              64 kB
SwapTotal:       1048577 kB
SwapFree:              1 kB
"""

    s_content_proc_meminfo_corrupted = \
        """MemTotal:       65639012 kB
        MemFree:         2837536 kB
        MemAvailable:    6291188 kB
        Buffers:         1209868 kB
        Cached:          8753908 kB
        SwapCached:
        Active:         30979152 kB
        Inactive:        8098196 kB
        Active(anon):   29115936 kB
        Inactive(anon):  6329360 kB
        Active(file):    1863216 kB
        Inactive(file):  1768836 kB
        Unevictable:          64 kB
        Mlocked:              64 kB
        SwapTotal:       1048577 kB
        SwapFree:              1 kB
        """

    def test_init(self):
        o_memutils = MemUtils(o_fileutils=FileUtils(), o_stringutils=StringUtils())

        assert isinstance(o_memutils.d_s_meminfo, dict)

    def test_update_meminfo_with_empty_results(self, monkeypatch):
        o_fileutils = FileUtils()
        o_stringutils = StringUtils()

        # test with read failure
        def mock_read(s_file_name):
            b_result = False
            a_s_meminfo = []
            return b_result, a_s_meminfo
        monkeypatch.setattr(o_fileutils, 'read_file_as_lines', mock_read)

        o_memutils = MemUtils(o_fileutils=o_fileutils, o_stringutils=o_stringutils)

        o_memutils.update_meminfo()

    def test_update_meminfo_with_proc_meminfo_results(self, monkeypatch):
        o_fileutils = FileUtils()
        o_stringutils = StringUtils()

        # test with read failure
        def mock_read(s_file_name):
            b_result = True

            a_s_meminfo = self.s_content_proc_meminfo.split("\n")

            return b_result, a_s_meminfo
        monkeypatch.setattr(o_fileutils, 'read_file_as_lines', mock_read)

        o_memutils = MemUtils(o_fileutils=o_fileutils, o_stringutils=o_stringutils)

        o_memutils.update_meminfo()

    def test_update_meminfo_with_proc_meminfo_results_wrong_values(self, monkeypatch):
        o_fileutils = FileUtils()
        o_stringutils = StringUtils()

        # test with read failure
        def mock_read(s_file_name):
            b_result = True

            a_s_meminfo = self.s_content_proc_meminfo_corrupted.split("\n")

            return b_result, a_s_meminfo
        monkeypatch.setattr(o_fileutils, 'read_file_as_lines', mock_read)

        o_memutils = MemUtils(o_fileutils=o_fileutils, o_stringutils=o_stringutils)

        o_memutils.update_meminfo()

    def test_get_mem_total(self, monkeypatch):
        o_fileutils = FileUtils()
        o_stringutils = StringUtils()

        # test with mocked values
        def mock_read(s_file_name):
            b_result = True

            a_s_meminfo = self.s_content_proc_meminfo.split("\n")

            return b_result, a_s_meminfo

        monkeypatch.setattr(o_fileutils, 'read_file_as_lines', mock_read)

        o_memutils = MemUtils(o_fileutils=o_fileutils, o_stringutils=o_stringutils)

        o_memutils.update_meminfo()

        s_mem_total = o_memutils.get_mem_total()
        assert s_mem_total == "65639012 kB"

    def test_get_memfree(self, monkeypatch):
        o_fileutils = FileUtils()
        o_stringutils = StringUtils()

        # test with mocked values
        def mock_read(s_file_name):
            b_result = True

            a_s_meminfo = self.s_content_proc_meminfo.split("\n")

            return b_result, a_s_meminfo

        monkeypatch.setattr(o_fileutils, 'read_file_as_lines', mock_read)

        o_memutils = MemUtils(o_fileutils=o_fileutils, o_stringutils=o_stringutils)

        o_memutils.update_meminfo()

        s_mem_free = o_memutils.get_memfree()
        assert s_mem_free == "2837536 kB"

    def test_get_memavailable(self, monkeypatch):
        o_fileutils = FileUtils()
        o_stringutils = StringUtils()

        # test with mocked values
        def mock_read(s_file_name):
            b_result = True

            a_s_meminfo = self.s_content_proc_meminfo.split("\n")

            return b_result, a_s_meminfo

        monkeypatch.setattr(o_fileutils, 'read_file_as_lines', mock_read)

        o_memutils = MemUtils(o_fileutils=o_fileutils, o_stringutils=o_stringutils)

        o_memutils.update_meminfo()

        s_mem_available = o_memutils.get_memavailable()
        assert s_mem_available == "6291188 kB"

    def test_get_swaptotal(self, monkeypatch):
        o_fileutils = FileUtils()
        o_stringutils = StringUtils()

        # test with mocked values
        def mock_read(s_file_name):
            b_result = True

            a_s_meminfo = self.s_content_proc_meminfo.split("\n")

            return b_result, a_s_meminfo

        monkeypatch.setattr(o_fileutils, 'read_file_as_lines', mock_read)

        o_memutils = MemUtils(o_fileutils=o_fileutils, o_stringutils=o_stringutils)

        o_memutils.update_meminfo()

        s_swaptotal = o_memutils.get_swaptotal()
        assert s_swaptotal == "10 kB"

    def test_get_swapfree(self, monkeypatch):
        o_fileutils = FileUtils()
        o_stringutils = StringUtils()

        # test with mocked values
        def mock_read(s_file_name):
            b_result = True

            a_s_meminfo = self.s_content_proc_meminfo.split("\n")

            return b_result, a_s_meminfo

        monkeypatch.setattr(o_fileutils, 'read_file_as_lines', mock_read)

        o_memutils = MemUtils(o_fileutils=o_fileutils, o_stringutils=o_stringutils)

        o_memutils.update_meminfo()

        s_swapfree = o_memutils.get_swapfree()
        assert s_swapfree == "1 kB"

    def test_get_swapinuse_less_than_1gb(self, monkeypatch):
        o_fileutils = FileUtils()
        o_stringutils = StringUtils()

        # test with mocked values
        def mock_read(s_file_name):
            b_result = True

            a_s_meminfo = self.s_content_proc_meminfo.split("\n")

            return b_result, a_s_meminfo

        monkeypatch.setattr(o_fileutils, 'read_file_as_lines', mock_read)

        o_memutils = MemUtils(o_fileutils=o_fileutils, o_stringutils=o_stringutils)

        o_memutils.update_meminfo()

        s_swapinuse = o_memutils.get_swapinuse_in_kb(b_add_unit=False)
        assert s_swapinuse == "9"

    def test_get_swapinuse_1gb_without_units(self, monkeypatch):
        o_fileutils = FileUtils()
        o_stringutils = StringUtils()

        # test with mocked values
        def mock_read(s_file_name):
            b_result = True

            a_s_meminfo = self.s_content_proc_meminfo2.split("\n")

            return b_result, a_s_meminfo

        monkeypatch.setattr(o_fileutils, 'read_file_as_lines', mock_read)

        o_memutils = MemUtils(o_fileutils=o_fileutils, o_stringutils=o_stringutils)

        o_memutils.update_meminfo()

        s_swapinuse_in_kb = o_memutils.get_swapinuse_in_kb(b_add_unit=False)
        assert s_swapinuse_in_kb == "1048576"

    def test_get_swapinuse_1gb_with_units(self, monkeypatch):
        o_fileutils = FileUtils()
        o_stringutils = StringUtils()

        # test with mocked values
        def mock_read(s_file_name):
            b_result = True

            a_s_meminfo = self.s_content_proc_meminfo2.split("\n")

            return b_result, a_s_meminfo

        monkeypatch.setattr(o_fileutils, 'read_file_as_lines', mock_read)

        o_memutils = MemUtils(o_fileutils=o_fileutils, o_stringutils=o_stringutils)

        o_memutils.update_meminfo()

        s_swapinuse_in_kb = o_memutils.get_swapinuse_in_kb(b_add_unit=True)
        assert s_swapinuse_in_kb == "1048576 kB"

# @TODO: Add broader test cases for test_get_swapinuse
