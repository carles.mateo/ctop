from lib.pythonutils import PythonUtils


class TestPythonUtils:

    def test_get_python_version(self):
        o_pythonutils = PythonUtils()
        s_version, i_major, i_minor, i_micro, s_releaselevel = o_pythonutils.get_python_version()

        assert isinstance(s_version, str)
        assert isinstance(i_major, int)
        assert i_major >= 2
        assert i_major <= 4

    def test_is_python3(self):
        o_pythonutils = PythonUtils()
        assert o_pythonutils.is_python_3() is True

    def test_is_python2(self):
        o_pythonutils = PythonUtils()
        assert o_pythonutils.is_python_2() is False
