import pytest
from lib.systemutils import SystemUtils


class TestSystemUtils:

    def test_exit_ctop_ok(self, capsys):
        o_systemutils = SystemUtils()

        with pytest.raises(SystemExit) as pytest_wrapped_e:
            o_systemutils.exit_ctop(s_text="bye", i_exit_code=0)

        assert pytest_wrapped_e.type == SystemExit
        assert pytest_wrapped_e.value.code == 0

        o_captured = capsys.readouterr()

        assert "bye" in o_captured.out
        assert o_captured.err == ""

    def test_exit_ctop_ko(self, capsys):
        o_systemutils = SystemUtils()

        with pytest.raises(SystemExit) as pytest_wrapped_e:
            o_systemutils.exit_ctop(s_text="IFoundAnError", i_exit_code=1)

        assert pytest_wrapped_e.type == SystemExit
        assert pytest_wrapped_e.value.code == 1

        o_captured = capsys.readouterr()

        assert "IFoundAnError" in o_captured.out
        assert o_captured.err == ""
