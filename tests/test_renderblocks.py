#
# Tests for DateTimeUtils class
#
# Author: Carles Mateo
# Creation Date: 2021-10-21 21:0 IST
# Last Update:
#

import pytest
from lib.appmessages import AppMessages
from lib.argsutils import ArgsUtils
from lib.config import Config
from lib.cpuutils import CpuUtils
from lib.datetimeutils import DateTimeUtils
from lib.fileutils import FileUtils
from lib.kernelutils import KernelUtils
from lib.logerrorscheckerutils import LogErrorsCheckerUtils
from lib.memutils import MemUtils
from lib.networkutils import NetworkUtils
from lib.osutils import OsUtils
from lib.processesutils import ProcessesUtils
from lib.renderblocks import RenderBlocks
from lib.screenutils import ScreenUtils
from lib.stringutils import StringUtils
from lib.systemutils import SystemUtils


class TestRenderBlocks(object):

    def test_get_max_width_for_platform(self, monkeypatch):

        a_test_params = []

        # Dependencies
        o_config = Config()
        o_argutils = ArgsUtils(a_test_params)
        o_fileutils = FileUtils()
        o_stringutils = StringUtils()
        o_datetimeutils = DateTimeUtils()
        o_osutils = OsUtils(o_fileutils=o_fileutils)
        o_processesutils = ProcessesUtils(o_fileutils)
        o_cpuutils = CpuUtils(o_fileutils)
        o_memutils = MemUtils(o_fileutils=o_fileutils, o_stringutils=o_stringutils)
        o_networkutils = NetworkUtils(o_fileutils=o_fileutils, o_datetimeutils=o_datetimeutils)
        o_kernelutils = KernelUtils(o_fileutils)
        o_logerrorscheckerutils = LogErrorsCheckerUtils(o_fileutils=o_fileutils)

        b_support_colors = True

        o_screenutils = ScreenUtils(b_support_colors=b_support_colors,
                                    i_fixed_terminal_width=100,
                                    i_fixed_terminal_height=50,
                                    b_debug=o_config.get_debug())
        o_systemutils = SystemUtils(o_screenutils=o_screenutils, o_datetimeutils=o_datetimeutils)
        o_appmessages = AppMessages(o_screenutils=o_screenutils)
        o_renderblocks = RenderBlocks(o_screenutils=o_screenutils,
                                      o_datetimeutils=o_datetimeutils,
                                      o_appmessages=o_appmessages,
                                      o_processesutils=o_processesutils,
                                      o_cpuutils=o_cpuutils,
                                      o_memutils=o_memutils,
                                      o_networkutils=o_networkutils,
                                      o_osutils=o_osutils,
                                      o_kernelutils=o_kernelutils,
                                      o_logerrorscheckerutils=o_logerrorscheckerutils,
                                      o_stringutils=o_stringutils)

        # Monkey patch get hostname
        def mock_get_platform_hostname():
            return "fast-test"
        monkeypatch.setattr(o_osutils, 'get_platform_hostname', mock_get_platform_hostname)

        i_max_width_for_platform = o_renderblocks.get_max_width_for_platform()

        i_width_available = o_screenutils.get_terminal_width_to_use()
        i_width_in_use = o_renderblocks.i_width_block_uid_and_block_uptime + \
                         o_renderblocks.i_width_block_hostname + o_renderblocks.i_width_block_distribution + 3

        assert i_width_available - i_width_in_use == i_max_width_for_platform

    def test_get_string_for_block_hostinfo_hostname(self, monkeypatch):

        a_test_params = []

        # Dependencies
        o_config = Config()
        o_argutils = ArgsUtils(a_test_params)
        o_fileutils = FileUtils()
        o_stringutils = StringUtils()
        o_datetimeutils = DateTimeUtils()
        o_osutils = OsUtils(o_fileutils=o_fileutils)
        o_processesutils = ProcessesUtils(o_fileutils)
        o_cpuutils = CpuUtils(o_fileutils)
        o_memutils = MemUtils(o_fileutils=o_fileutils, o_stringutils=o_stringutils)
        o_networkutils = NetworkUtils(o_fileutils=o_fileutils, o_datetimeutils=o_datetimeutils)
        o_kernelutils = KernelUtils(o_fileutils)
        o_logerrorscheckerutils = LogErrorsCheckerUtils(o_fileutils=o_fileutils)

        b_support_colors = True

        o_screenutils = ScreenUtils(b_support_colors=b_support_colors,
                                    i_fixed_terminal_width=100,
                                    i_fixed_terminal_height=50,
                                    b_debug=o_config.get_debug())
        o_systemutils = SystemUtils(o_screenutils=o_screenutils, o_datetimeutils=o_datetimeutils)
        o_appmessages = AppMessages(o_screenutils=o_screenutils)

        # Monkey patch get hostname
        def mock_get_platform_hostname():
            return "fast-test"
        monkeypatch.setattr(o_osutils, 'get_platform_hostname', mock_get_platform_hostname)

        o_renderblocks = RenderBlocks(o_screenutils=o_screenutils,
                                      o_datetimeutils=o_datetimeutils,
                                      o_appmessages=o_appmessages,
                                      o_processesutils=o_processesutils,
                                      o_cpuutils=o_cpuutils,
                                      o_memutils=o_memutils,
                                      o_networkutils=o_networkutils,
                                      o_osutils=o_osutils,
                                      o_kernelutils=o_kernelutils,
                                      o_logerrorscheckerutils=o_logerrorscheckerutils,
                                      o_stringutils=o_stringutils)

        s_hostname_fragment_bw, s_hostname_fragment_color = o_renderblocks.get_string_for_block_hostinfo_hostname()
        assert s_hostname_fragment_bw == "hostname: fast-test"
        assert len(s_hostname_fragment_color) > len(s_hostname_fragment_bw)

    def test_get_string_for_distribution_description(self, monkeypatch):

        a_test_params = []

        # Dependencies
        o_config = Config()
        o_argutils = ArgsUtils(a_test_params)
        o_fileutils = FileUtils()
        o_stringutils = StringUtils()
        o_datetimeutils = DateTimeUtils()
        o_osutils = OsUtils(o_fileutils=o_fileutils)
        o_processesutils = ProcessesUtils(o_fileutils)
        o_cpuutils = CpuUtils(o_fileutils)
        o_memutils = MemUtils(o_fileutils=o_fileutils, o_stringutils=o_stringutils)
        o_networkutils = NetworkUtils(o_fileutils=o_fileutils, o_datetimeutils=o_datetimeutils)
        o_kernelutils = KernelUtils(o_fileutils)
        o_logerrorscheckerutils = LogErrorsCheckerUtils(o_fileutils=o_fileutils)

        b_support_colors = True

        o_screenutils = ScreenUtils(b_support_colors=b_support_colors,
                                    i_fixed_terminal_width=100,
                                    i_fixed_terminal_height=50,
                                    b_debug=o_config.get_debug())
        o_systemutils = SystemUtils(o_screenutils=o_screenutils, o_datetimeutils=o_datetimeutils)
        o_appmessages = AppMessages(o_screenutils=o_screenutils)

        # Monkey patch get hostname
        def mock_get_distribution_description():
            return "cthulhu 20.04.7 LTS"
        monkeypatch.setattr(o_osutils, 'get_distribution_description', mock_get_distribution_description)

        o_renderblocks = RenderBlocks(o_screenutils=o_screenutils,
                                      o_datetimeutils=o_datetimeutils,
                                      o_appmessages=o_appmessages,
                                      o_processesutils=o_processesutils,
                                      o_cpuutils=o_cpuutils,
                                      o_memutils=o_memutils,
                                      o_networkutils=o_networkutils,
                                      o_osutils=o_osutils,
                                      o_kernelutils=o_kernelutils,
                                      o_logerrorscheckerutils=o_logerrorscheckerutils,
                                      o_stringutils=o_stringutils)

        s_distro_fragment_bw, s_distro_fragment_color = o_renderblocks.get_string_for_distribution_description()
        assert s_distro_fragment_bw == "Distro: cthulhu 20.04.7 LTS"
        assert len(s_distro_fragment_color) > len(s_distro_fragment_bw)

    def test_get_string_for_distribution_description_empty(self, monkeypatch):
        a_test_params = []

        # Dependencies
        o_config = Config()
        o_argutils = ArgsUtils(a_test_params)
        o_fileutils = FileUtils()

        # Monkey patch for FileUtils
        def mock_read_file_as_string(s_file):
            return False, ""

        monkeypatch.setattr(o_fileutils, 'read_file_as_string', mock_read_file_as_string)

        o_stringutils = StringUtils()
        o_datetimeutils = DateTimeUtils()
        o_osutils = OsUtils(o_fileutils=o_fileutils)
        o_processesutils = ProcessesUtils(o_fileutils)
        o_cpuutils = CpuUtils(o_fileutils)
        o_memutils = MemUtils(o_fileutils=o_fileutils, o_stringutils=o_stringutils)
        o_networkutils = NetworkUtils(o_fileutils=o_fileutils, o_datetimeutils=o_datetimeutils)
        o_kernelutils = KernelUtils(o_fileutils)
        o_logerrorscheckerutils = LogErrorsCheckerUtils(o_fileutils=o_fileutils)

        b_support_colors = True

        o_screenutils = ScreenUtils(b_support_colors=b_support_colors,
                                    i_fixed_terminal_width=100,
                                    i_fixed_terminal_height=50,
                                    b_debug=o_config.get_debug())
        o_systemutils = SystemUtils(o_screenutils=o_screenutils, o_datetimeutils=o_datetimeutils)
        o_appmessages = AppMessages(o_screenutils=o_screenutils)

        o_renderblocks = RenderBlocks(o_screenutils=o_screenutils,
                                      o_datetimeutils=o_datetimeutils,
                                      o_appmessages=o_appmessages,
                                      o_processesutils=o_processesutils,
                                      o_cpuutils=o_cpuutils,
                                      o_memutils=o_memutils,
                                      o_networkutils=o_networkutils,
                                      o_osutils=o_osutils,
                                      o_kernelutils=o_kernelutils,
                                      o_logerrorscheckerutils=o_logerrorscheckerutils,
                                      o_stringutils=o_stringutils)

        s_distro_fragment_bw, s_distro_fragment_color = o_renderblocks.get_string_for_distribution_description()
        assert s_distro_fragment_bw == ""

    def test_get_string_for_block_uid(self, monkeypatch):
        a_test_params = []

        # Dependencies
        o_config = Config()
        o_argutils = ArgsUtils(a_test_params)
        o_fileutils = FileUtils()
        o_stringutils = StringUtils()
        o_datetimeutils = DateTimeUtils()
        o_osutils = OsUtils(o_fileutils=o_fileutils)

        # Monkey patch for OsUtils
        def mock_get_euid():
            return 0

        monkeypatch.setattr(o_osutils, 'get_euid', mock_get_euid)

        def mock_get_username():
            return True, "root"

        monkeypatch.setattr(o_osutils, 'get_username', mock_get_username)

        o_processesutils = ProcessesUtils(o_fileutils)
        o_cpuutils = CpuUtils(o_fileutils)
        o_memutils = MemUtils(o_fileutils=o_fileutils, o_stringutils=o_stringutils)
        o_networkutils = NetworkUtils(o_fileutils=o_fileutils, o_datetimeutils=o_datetimeutils)
        o_kernelutils = KernelUtils(o_fileutils)
        o_logerrorscheckerutils = LogErrorsCheckerUtils(o_fileutils=o_fileutils)

        b_support_colors = True

        o_screenutils = ScreenUtils(b_support_colors=b_support_colors,
                                    i_fixed_terminal_width=100,
                                    i_fixed_terminal_height=50,
                                    b_debug=o_config.get_debug())
        o_systemutils = SystemUtils(o_screenutils=o_screenutils, o_datetimeutils=o_datetimeutils)
        o_appmessages = AppMessages(o_screenutils=o_screenutils)

        o_renderblocks = RenderBlocks(o_screenutils=o_screenutils,
                                      o_datetimeutils=o_datetimeutils,
                                      o_appmessages=o_appmessages,
                                      o_processesutils=o_processesutils,
                                      o_cpuutils=o_cpuutils,
                                      o_memutils=o_memutils,
                                      o_networkutils=o_networkutils,
                                      o_osutils=o_osutils,
                                      o_kernelutils=o_kernelutils,
                                      o_logerrorscheckerutils=o_logerrorscheckerutils,
                                      o_stringutils=o_stringutils)

        s_line_userid_bw, s_line_userid_color = o_renderblocks.get_string_for_block_uid()

        assert s_line_userid_bw == "User Id: 0 root"
        assert len(s_line_userid_color) >= len(s_line_userid_bw)
        assert "User Id:" in s_line_userid_color

    def test_render_block_uid(self):
        a_test_params = []

        # Dependencies
        o_config = Config()
        o_argutils = ArgsUtils(a_test_params)
        o_fileutils = FileUtils()
        o_stringutils = StringUtils()
        o_datetimeutils = DateTimeUtils()
        o_osutils = OsUtils(o_fileutils=o_fileutils)
        o_processesutils = ProcessesUtils(o_fileutils)
        o_cpuutils = CpuUtils(o_fileutils)
        o_memutils = MemUtils(o_fileutils=o_fileutils, o_stringutils=o_stringutils)
        o_networkutils = NetworkUtils(o_fileutils=o_fileutils, o_datetimeutils=o_datetimeutils)
        o_kernelutils = KernelUtils(o_fileutils)
        o_logerrorscheckerutils = LogErrorsCheckerUtils(o_fileutils=o_fileutils)

        b_support_colors = True

        o_screenutils = ScreenUtils(b_support_colors=b_support_colors,
                                    i_fixed_terminal_width=100,
                                    i_fixed_terminal_height=50,
                                    b_debug=o_config.get_debug())
        o_systemutils = SystemUtils(o_screenutils=o_screenutils, o_datetimeutils=o_datetimeutils)
        o_appmessages = AppMessages(o_screenutils=o_screenutils)

        o_renderblocks = RenderBlocks(o_screenutils=o_screenutils,
                                      o_datetimeutils=o_datetimeutils,
                                      o_appmessages=o_appmessages,
                                      o_processesutils=o_processesutils,
                                      o_cpuutils=o_cpuutils,
                                      o_memutils=o_memutils,
                                      o_networkutils=o_networkutils,
                                      o_osutils=o_osutils,
                                      o_kernelutils=o_kernelutils,
                                      o_logerrorscheckerutils=o_logerrorscheckerutils,
                                      o_stringutils=o_stringutils)

        i_terminal_width = o_renderblocks.o_screenutils.get_terminal_width_to_use()
        assert i_terminal_width == 100

        o_renderblocks.render_block_uid()

        assert o_renderblocks.i_width_block_uid_and_block_uptime > 25
        assert i_terminal_width - o_renderblocks.get_width_block_uid_and_block_uptime() > 50
