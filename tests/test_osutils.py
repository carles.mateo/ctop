import pytest
import os
import sys

# **********************************************************
# Libraries will find there dependencies when being tested
# **********************************************************
s_path_program = os.path.dirname(__file__)
sys.path.append(s_path_program + '../src/')
sys.path.append(s_path_program + '../')
sys.path.append(s_path_program + '../src/ctop/lib/')
# **********************************************************

from lib.fileutils import FileUtils
from lib.osutils import OsUtils


class TestOsUtils:

    def test_detect_system(self):
        o_fileutils = FileUtils()
        o_osutils = OsUtils(o_fileutils=o_fileutils)

        o_osutils.detect_system()

        assert o_osutils.get_distribution_description() != ""
        assert o_osutils.get_platform_system() == "Linux"
        assert o_osutils.is_linux() is True
        assert o_osutils.get_platform_release() != ""
        assert o_osutils.get_platform_hostname() != ""
        assert o_osutils.get_platform_machine() != ""
        assert o_osutils.get_platform_version() != ""

    def test_get_uptime(self):
        o_fileutils = FileUtils()
        o_osutils = OsUtils(o_fileutils=o_fileutils)

        b_success, s_uptime = o_osutils.get_uptime()
        assert b_success is True
        # We know the computer had been started at least 100 seconds ago
        assert int(s_uptime) > 100

    def test_get_total_and_free_space_in_gib(self):
        o_fileutils = FileUtils()
        o_osutils = OsUtils(o_fileutils=o_fileutils)

        f_total_gb, f_free_gb, s_total_gb, s_free_gb = o_osutils.get_total_and_free_space_in_gib(s_folder="/", b_add_units=True)

        assert f_total_gb > 0
        assert f_free_gb >= 0
        assert "GiB" in s_total_gb
        assert "GiB" in s_free_gb
        assert s_total_gb.replace("GiB", "") == str(round(f_total_gb, 2))
        assert s_free_gb.replace("GiB", "") == str(round(f_free_gb,2))

    def test_get_inodes_in_use_and_free(self):
        o_fileutils = FileUtils()
        o_osutils = OsUtils(o_fileutils=o_fileutils)

        i_total_inode, i_free_inode, f_percent_free = o_osutils.get_inodes_in_use_and_free()

        assert i_free_inode <= i_total_inode
        assert f_percent_free >=0 and f_percent_free <= 100

    def test_get_error_accessing_dmi(self):
        o_fileutils = FileUtils()
        o_osutils = OsUtils(o_fileutils=o_fileutils)

        b_error_accessing = o_osutils.get_error_accessing_dmi()
        assert isinstance(b_error_accessing, bool)

    def test_get_uptime_fail(self, monkeypatch):
        o_fileutils = FileUtils()
        o_osutils = OsUtils(o_fileutils=o_fileutils)

        # Monkey patch to simulate a fail
        def mock_read_file_as_string(s_file):
            return False, ""
        monkeypatch.setattr(o_fileutils, 'read_file_as_string', mock_read_file_as_string)

        b_success, s_seconds = o_osutils.get_uptime()
        assert b_success is False
        assert s_seconds == ""
