#
# Tests for DateTimeUtils class
#
# Author: Carles Mateo
# Creation Date: 2019-11-20 17:38 IST
# Last Update:   2022-03-18 20:22 IST
#

import pytest
from lib.datetimeutils import DateTimeUtils


class TestDateTimeUtils(object):

    # Start Tests
    def test_get_datetime(self):
        o_datetime = DateTimeUtils()

        s_datetime = o_datetime.get_datetime(b_milliseconds=False)

        assert s_datetime != ""
        assert len(s_datetime) == 19
        assert s_datetime[4] == "-"
        assert s_datetime[7] == "-"
        assert s_datetime[10] == " "
        assert s_datetime[13] == ":"
        assert s_datetime[16] == ":"

        # The Data has to be more recent than now
        assert s_datetime > "2020-08-16 10:10:10"

    def test_get_datetime_with_milliseconds(self):
        o_datetime = DateTimeUtils()

        s_datetime = o_datetime.get_datetime(b_milliseconds=True)

        assert s_datetime != ""
        assert len(s_datetime) > 19
        assert s_datetime[4] == "-"
        assert s_datetime[7] == "-"
        assert s_datetime[10] == " "
        assert s_datetime[13] == ":"
        assert s_datetime[16] == ":"

        assert s_datetime[19] == "."
        # The Data has to be more recent than now
        assert s_datetime > "2020-08-16 10:10:10"

    def test_get_unix_epoch(self):
        o_datetime = DateTimeUtils()

        s_epoch = o_datetime.get_unix_epoch()
        assert s_epoch != ""
        assert len(s_epoch) >= 10
        assert int(s_epoch) > 1591210336

    def test_get_unix_epoch_as_float(self):
        o_datetime = DateTimeUtils()

        f_datetime = o_datetime.get_unix_epoch_as_float()

        assert isinstance(f_datetime, float)
        assert f_datetime > 1591210336
