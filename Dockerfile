FROM ubuntu:20.04

MAINTAINER Carles

ARG DEBIAN_FRONTEND=noninteractive

# This will make sure printing in the Screen when running in dettached mode
ENV PYTHONUNBUFFERED=1

#RUN echo "nameserver 8.8.8.8" > /etc/resolv.conf

RUN apt update && apt install -y python3 python3-pip mc htop less strace zip gzip lynx && apt upgrade -y && apt-get clean

RUN pip install pytest virtualenv

ENV CTOP /var/ctop

RUN mkdir -p $CTOP

COPY src/ctop $CTOP

RUN ln -s /var/ctop/ctop.py /bin/ctop

CMD ["/var/ctop/ctop.py", "--iterations=1", "--columns=130", "--rows=25"]
