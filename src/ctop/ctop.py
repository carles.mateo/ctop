#!/usr/bin/env python3
#
# CTop Main File
#
# Author: Carles Mateo
# Creation Date: 2019-11-20 Ireland
# Last Update:   2020-07-23 10:47 Ireland
# Description: Main, callable from command line, ctop
#

import os
import sys
from lib.config import Config
from lib.pythonutils import PythonUtils
from lib.fileutils import FileUtils
from lib.stringutils import StringUtils
from lib.screenutils import ScreenUtils
from lib.datetimeutils import DateTimeUtils
from lib.systemutils import SystemUtils
from lib.processesutils import ProcessesUtils
from lib.appmessages import AppMessages
from lib.renderblocks import RenderBlocks
from lib.cpuutils import CpuUtils
from lib.memutils import MemUtils
from lib.networkutils import NetworkUtils
from lib.osutils import OsUtils
from lib.kernelutils import KernelUtils
from lib.logerrorscheckerutils import LogErrorsCheckerUtils
from lib.argsutils import ArgsUtils
from lib.importutils import ImportUtils
from do.processdo import ProcessDO
from do.stringcolordo import StringColorDO


class CTop:

    i_EXIT_CODE_OK = 0
    i_EXIT_CODE_ERROR = 1
    i_EXIT_CODE_ERROR_PLUGIN = 10

    i_refresh_data_seconds = 1
    i_refresh_screen_seconds = 30
    o_pythonutils = None
    o_fileutils = None
    o_stringutils = None
    o_datetimeutils = None
    o_screenutils = None
    o_systemutils = None
    o_processesutils = None
    o_appmessages = None
    o_renderblocks = None
    o_cpuutils = None
    o_memutils = None
    o_networkutils = None
    o_kernelutils = None
    o_osutils = None
    o_argutils = None
    o_logerrorscheckerutils = None
    o_importutils = None

    # Hooks for Plugins
    a_h_s_actions_each_loop = []
    a_h_s_actions_exit = []

    def __init__(self, i_refresh_screen_seconds=30, o_pythonutils=None, o_fileutils=None, o_screenutils=None, o_systemutils=None,
                 o_processesutils=None, o_datetimeutils=None, o_cpuutils=None, o_appmessages=None, o_renderblocks=None,
                 o_memutils=None, o_stringutils=None, o_networkutils=None, o_osutils=None, o_kernelutils=None,
                 o_argutils=None,
                 o_logerrorscheckerutils=None, o_importutils=None
                 ):
        self.i_refresh_screen_seconds = i_refresh_screen_seconds
        self.o_pythonutils = o_pythonutils
        self.o_fileutils = o_fileutils
        self.o_stringutils = o_stringutils
        self.o_screenutils = o_screenutils
        self.o_systemutils = o_systemutils
        self.o_processesutils = o_processesutils
        self.o_datetimeutils = o_datetimeutils
        self.o_appmessages = o_appmessages
        self.o_renderblocks = o_renderblocks
        self.o_cpuutils = o_cpuutils
        self.o_memutils = o_memutils
        self.o_networkutils = o_networkutils
        self.o_osutils = o_osutils
        self.o_kernelutils = o_kernelutils
        self.o_argutils = o_argutils
        self.o_logerrorscheckerutils = o_logerrorscheckerutils
        self.o_importutils = o_importutils

        self.a_h_s_actions_each_loop = []
        self.a_h_s_actions_exit = []

        self.s_own_path = self.get_own_path()

    def get_version_name(self):
        """
        This is used on the displaying Loading message
        :return: version in bw, version in color
        """
        # return VERSION_NAME + " v." + VERSION_NUMBER + " Codename: " + VERSION_CODE
        s_version_name_bw, s_version_name_color = self.o_appmessages.get_version_name()
        s_code_name_bw, s_code_name_color = self.o_appmessages.get_code_name()

        return s_version_name_bw + " " + s_code_name_bw, s_version_name_color + " " + s_code_name_color

    def get_own_path(self):
        s_own_path = os.path.dirname(os.path.abspath(__file__))
        if s_own_path[-1] != "/":
            s_own_path = s_own_path + "/"

        return s_own_path

    def register_hook_action_each_loop(self, o_class, s_method):
        h_s_action = {"class": o_class,
                      "method": s_method}

        self.a_h_s_actions_each_loop.append(h_s_action)

    def register_hook_action_exit(self, o_class, s_method):
        """
        This method will be added to the methods executed when leaving CTOP.
        """
        h_s_action = {"class": o_class,
                      "method": s_method}

        self.a_h_s_actions_exit.append(h_s_action)

    def run(self, i_iterations=0, i_sleep=1):
        """
        Principal execution for Carles Top program
        :return:
        """

        # Note: On the first iteration we will refresh
        #i_seconds_since_data_fetch = self.i_refresh_data_seconds
        i_seconds_since_data_fetch = 0
        #i_seconds_since_screen_refresh = self.i_refresh_screen_seconds
        i_seconds_since_screen_refresh = 0
        i_page = 0

        if self.o_osutils.is_linux() is False:
            self.o_systemutils.exit_ctop(s_text="ERROR: This program only runs in Linux.",
                                         i_exit_code=ProcessesUtils.EXIT_CODE_ERROR_NO_LINUX)

        #if self.o_osutils.get_euid() != 0:
        #    self.o_systemutils.exit_ctop(s_text="ERROR: You need to have root privileges to run this program.",
        #                                 i_exit_code=ProcessesUtils.EXIT_CODE_ERROR_NO_ROOT)

        s_version_name_bw, s_version_name_color = self.get_version_name()
        self.o_screenutils.p("Loading... " + s_version_name_color)
        self.o_screenutils.p("Loading Kernel logs...")
        self.o_screenutils.p("Loading messages logs...")
        self.o_logerrorscheckerutils.load_all_logs()
        b_found_nfs_timeout, i_found_nfs_timeout, a_found = self.o_logerrorscheckerutils.search_for_nfs_timeout_errors()
        if b_found_nfs_timeout is True:
            self.o_screenutils.p("Found NFS Kernel Timed outs: " + str(i_found_nfs_timeout))
            self.o_screenutils.p("Sample: " + a_found[0])
        b_found_read_memory_errors, i_errors_found_read_memory, a_errors_read_memory = self.o_logerrorscheckerutils.search_for_memory_read_errors()
        if b_found_read_memory_errors is True:
            self.o_screenutils.p("Found Read Memory errors: " + str(i_errors_found_read_memory))
            self.o_screenutils.p("Sample: " + a_errors_read_memory[0])

        self.o_logerrorscheckerutils.load_logs_messages()
        b_found_out_of_memory_errors, i_errors_found_out_of_memory, a_errors_out_of_memory = self.o_logerrorscheckerutils.search_for_memory_read_errors()
        if b_found_out_of_memory_errors is True:
            self.o_screenutils.p("Found Out of Memory errors: " + str(i_errors_found_out_of_memory))
            self.o_screenutils.p("Sample: " + a_errors_out_of_memory[0])
        b_found_rabbitmq_errors, i_errors_found_rabbitmq, a_errors_rabbitmq = self.o_logerrorscheckerutils.search_for_rabbitmq_errors()
        if b_found_rabbitmq_errors is True:
            self.o_screenutils.p("Found RabbitMQ errors: " + str(i_errors_found_rabbitmq))
            self.o_screenutils.p("Sample: " + a_errors_rabbitmq[0])

        self.o_screenutils.p("Press CTRL + C to interrupt at any moment... ")

        try:
            #
            # Init screen components
            self.o_screenutils.clear_screen()
            self.o_screenutils.reset_virtual_terminal()
            b_success = self.o_processesutils.update_list_of_processes()
            i_num_processes = self.o_processesutils.get_num_current_processes_in_dict()
            self.o_processesutils.update_repeated_processes_count()
            self.o_kernelutils.update_system_swappiness()

            b_loop = True

            while b_loop is True:
                # Loop por updating Screen
                while b_loop is True:
                    # Loop for updating Data

                    # Information will be only updated every 1 second, every 5, or every loop is defined
                    if i_seconds_since_data_fetch >= self.i_refresh_data_seconds:
                        i_seconds_since_screen_refresh = i_seconds_since_screen_refresh + i_seconds_since_data_fetch
                        i_seconds_since_data_fetch = 0

                        # Update information about CPU Usage
                        b_success = self.o_cpuutils.update_cpu_usage()
                        # Update information about processes
                        b_success = self.o_processesutils.update_list_of_processes()
                        i_num_processes = self.o_processesutils.get_num_current_processes_in_dict()
                        if b_success is False:
                            # There was a problem fetching the Processes
                            self.o_systemutils.exit_ctop("Problem reading processes info",
                                                         ProcessesUtils.EXIT_CODE_ERROR_PROCESSES)
                        self.o_processesutils.update_repeated_processes_count()

                        self.o_networkutils.update_listening_ports()

                        # Update the Swappiness to internal variable
                        self.o_kernelutils.update_system_swappiness()

                    # Updating screen
                    self.o_screenutils.clear_screen()
                    self.o_screenutils.reset_virtual_terminal()
                    # Render the screen
                    self.o_renderblocks.render_virtual_terminal(i_page)

                    self.o_screenutils.print_virtual_terminal()

                    if i_seconds_since_screen_refresh >= self.i_refresh_screen_seconds:
                        i_seconds_since_screen_refresh = 0
                        # Paging for the processes
                        i_page = i_page + 1
                        i_processes_per_page = self.o_renderblocks.get_processes_per_page()
                        i_total_pages = int(i_num_processes / i_processes_per_page)
                        if i_page > i_total_pages:
                            i_page = 0

                        # Now Update Info about the Errors in the Kernel and Logs
                        self.o_logerrorscheckerutils.update_logs()
                        break

                    # Loop through the Hooks of the plugins
                    for h_s_action in self.a_h_s_actions_each_loop:
                        o_class = h_s_action["class"]
                        s_method = h_s_action["method"]
                        o_pluginmethod = self.o_importutils.get_method_handler(o_class=o_class, s_method=s_method)
                        b_plugin_method_success = o_pluginmethod()

                    if i_iterations == 1:
                        # Leave on next Loop
                        b_loop = False

                    if i_iterations > 0:
                        i_iterations = i_iterations - 1

                    if b_loop is True:
                        # Sleep just 1 second or i_sleeep seconds, if we slept the time expected to fetch new Data do it
                        self.o_systemutils.sleep(i_sleep)
                        i_seconds_since_data_fetch = i_seconds_since_data_fetch + i_sleep

        except KeyboardInterrupt:
            self.ctop_exit("\nExiting CTop.", ProcessesUtils.EXIT_CODE_NO_ERRORS)
        # Only enable in Python 3 or Python 2.7. Kept commented for compatibility with older Python versions
        # except Exception as e:
        except:
            # print(e)
            self.ctop_exit("Unknown Error", ProcessesUtils.EXIT_CODE_ERROR_UNKNOWN)

        self.ctop_exit("\nExiting CTop.", ProcessesUtils.EXIT_CODE_NO_ERRORS)

    def ctop_exit(self, s_error_message="", i_code=0):
        if len(self.a_h_s_actions_exit) > 0:
            self.o_screenutils.p("Executing exit hooks from the plugins...")
            for h_s_action in self.a_h_s_actions_exit:
                o_class = h_s_action["class"]
                s_method = h_s_action["method"]
                o_pluginmethod = self.o_importutils.get_method_handler(o_class=o_class, s_method=s_method)
                b_plugin_method_success = o_pluginmethod()

        if s_error_message != "":
            if i_code == 0:
                self.o_screenutils.p(s_error_message)
            else:
                self.o_screenutils.perror(s_error_message)
        ctop_exit(i_code)


def ctop_exit(i_code=0):
    exit(i_code)


def main(a_test_params=sys.argv):
    """
    main() Called from Unit Testing
    :param: a_test_params: Helps to run Unit Testing
    """
    # Nice to have implement Command line for parameters
    i_refresh_screen_seconds = 30

    # Dependencies
    o_config = Config()
    o_argutils = ArgsUtils(a_test_params)
    o_fileutils = FileUtils()
    o_stringutils = StringUtils()
    o_datetimeutils = DateTimeUtils()
    o_osutils = OsUtils(o_fileutils=o_fileutils)
    o_processesutils = ProcessesUtils(o_fileutils)
    o_cpuutils = CpuUtils(o_fileutils)
    o_memutils = MemUtils(o_fileutils=o_fileutils, o_stringutils=o_stringutils)
    o_networkutils = NetworkUtils(o_fileutils=o_fileutils, o_datetimeutils=o_datetimeutils)
    o_kernelutils = KernelUtils(o_fileutils)
    o_logerrorscheckerutils = LogErrorsCheckerUtils(o_fileutils=o_fileutils)
    o_importutils = ImportUtils(o_fileutils, o_stringutils)
    print(o_fileutils.get_own_path())
    # Check command Line
    # This is done here in order to initialize o_screenutils in color or in B&W.
    b_version, s_value = o_argutils.get_arg_from_list(["-v", "--version"])

    b_help, s_value = o_argutils.get_arg_from_list(["-h", "--help"])

    s_iterations = "0"
    s_sleep = "1"
    b_iterations, s_value = o_argutils.get_arg_from_list(["-n", "--iterations"], s_iterations)
    b_conversion, i_iterations = o_stringutils.convert_string_to_integer(s_value)

    b_sleep, s_value = o_argutils.get_arg_from_list(["-s", "--sleep"], s_sleep)
    b_conversion, i_sleep = o_stringutils.convert_string_to_integer(s_value)

    b_blackandwhite, s_value = o_argutils.get_arg_from_list(["-b", "--blackandwhite"], "")
    b_support_colors = not b_blackandwhite

    # Fixed Width
    i_x = 0
    b_x, s_value = o_argutils.get_arg_from_list(["-x", "--columns"], "0")
    if b_x is True:
        b_success, i_x = o_stringutils.convert_string_to_integer(s_value)
        # Minimum width 130 columns to prevent breaking output, specially with colors
        if i_x < 130:
            i_x = 130

    # Fixed Height
    i_y = 0
    b_y, s_value = o_argutils.get_arg_from_list(["-y", "--rows"], "0")
    if b_y is True:
        b_success, i_y = o_stringutils.convert_string_to_integer(s_value)
        if i_y < 25:
            i_y = 25

    b_debug, s_value = o_argutils.get_arg_from_list(["--debug"], "")
    if b_debug is True:
        o_config.set_debug(b_debug)

    # Continue Dependency initialization
    o_screenutils = ScreenUtils(b_support_colors=b_support_colors,
                                i_fixed_terminal_width=i_x,
                                i_fixed_terminal_height=i_y,
                                b_debug=o_config.get_debug())
    o_systemutils = SystemUtils(o_screenutils=o_screenutils, o_datetimeutils=o_datetimeutils)
    o_appmessages = AppMessages(o_screenutils=o_screenutils)
    o_renderblocks = RenderBlocks(o_screenutils=o_screenutils,
                                  o_datetimeutils=o_datetimeutils,
                                  o_appmessages=o_appmessages,
                                  o_processesutils=o_processesutils,
                                  o_cpuutils=o_cpuutils,
                                  o_memutils=o_memutils,
                                  o_networkutils=o_networkutils,
                                  o_osutils=o_osutils,
                                  o_kernelutils=o_kernelutils,
                                  o_logerrorscheckerutils=o_logerrorscheckerutils,
                                  o_stringutils=o_stringutils)

    # Instance Main Program
    o_ctop = CTop(i_refresh_screen_seconds=i_refresh_screen_seconds,
                  o_fileutils=o_fileutils,
                  o_screenutils=o_screenutils,
                  o_systemutils=o_systemutils,
                  o_processesutils=o_processesutils,
                  o_datetimeutils=o_datetimeutils,
                  o_cpuutils=o_cpuutils,
                  o_appmessages=o_appmessages,
                  o_renderblocks=o_renderblocks,
                  o_memutils=o_memutils,
                  o_stringutils=o_stringutils,
                  o_networkutils=o_networkutils,
                  o_osutils=o_osutils,
                  o_kernelutils=o_kernelutils,
                  o_argutils=o_argutils,
                  o_logerrorscheckerutils=o_logerrorscheckerutils,
                  o_importutils=o_importutils
                  )

    if b_version or b_help is True:
        s_version_name_bw, s_version_name_color = o_ctop.get_version_name()
        print(s_version_name_color + " by Carles Mateo https://blog.carlesmateo.com")

    if b_help is True:
        s_help = \
"""
CTOP.py An Open Source Systems Utility by Carles Mateo

Arguments:

 Short   Long
 =====   ====

 -h      --help            Print this help.
 
 -b      --blackandwhite   Uses black and wait instead of colors.
 -n=1    --iterations=10   Number or iterations, then leave.
 -p=name --plugin=name     Attempts lo load plugin name. Plugins should be located in plugins/ folder.
 -s=1    --sleep=5         Seconds to sleep between screen refresh. Try -s=2 for SSH to AWS, GCE, Azure, Digital Ocean...
 -x=150  --columns=150     Fixed width in characters. Specially using for Docker. Minimum is 130.
 -y=50   --rows=50         Fixed lines (or rows). Minimum is 25.
 -v      --version         Shows version.

"""
        print(s_help)

    if b_version is True or b_help is True:
        ctop_exit(CTop.i_EXIT_CODE_OK)

    # Check for Plugins
    b_p, s_plugin_selected = o_argutils.get_arg_from_list(["-p", "--plugin"])
    if b_p is True:
        if s_plugin_selected != "":
            b_success, b_enabled, o_pluginclass, s_default_action, h_s_config_values = o_importutils.import_plugin(s_plugin_selected)
            if b_success is False:
                print("Error. Plugin '" + s_plugin_selected + "' cannot be loaded.")
                ctop_exit(CTop.i_EXIT_CODE_ERROR_PLUGIN)
            if b_enabled is False:
                print("Error. Plugin '" + s_plugin_selected + "' is disabled. Enable it first.")
                ctop_exit(CTop.i_EXIT_CODE_ERROR_PLUGIN)

            o_screenutils.p("Plugin '" + s_plugin_selected + "' is Enabled. Loaded successfully")
            o_plugin = o_pluginclass(h_s_params=sys.argv,
                                     h_s_config_values=h_s_config_values,
                                     o_ctop=o_ctop)
            s_plugin_id, s_plugin_name, s_plugin_version, s_plugin_author, s_plugin_url = o_plugin.get_plugin_version()
            print(s_plugin_name + " " + s_plugin_version + " id: " + s_plugin_id)
            # s_command_full = o_plugin.get_command_to_backup()
            # print(s_command_full)

            #o_default_action = o_pluginmodel.get_method_handler(o_class=o_plugin, s_method=s_default_action)
            #print("Launching default action: " + s_default_action)
            #o_default_action()
            b_success = o_plugin.default_start()


    o_ctop.run(i_iterations=i_iterations, i_sleep=i_sleep)


# That's for Python2 and for controlling where it is called from Command Line or Imported or Tests.
if __name__ == "__main__":
    main()
