#
# Process Data Object
#
# Author: Carles Mateo
# Creation Date: 2019-11-20 16:06 IST
# Description: Object that stores process information
#


class ProcessDO:
    s_id = ""  # Ideally (pidname + "-" + pid + "-" + datetimefirstseen), now just (pidname + "-" + pid)
    # Please note even if PID is a number we store as String for convenience, as it match directory folder
    s_pid = ""
    s_name = ""
    s_cmdline = ""
    # PID of the parent process
    s_pid_parent = ""
    s_owner = ""
    # CPU used now
    i_cpu = 0

    # IO
    d_s_io = {}

    # Is process still alive? I mean if it exists
    b_alive = True
    b_zombie = False

    i_seen_times = 0
    f_avg_cpu = 0.0

    a_fds = []

    def __init__(self, s_pid, s_name):
        self.s_pid = s_pid
        self.s_name = s_name
        # Init to 1 if has pid
        # if s_pid != '':
        self.set_seen_times(1)
        self.a_fds = []

    def increase_seen_times(self):
        self.i_seen_times = self.i_seen_times + 1

    def get_name(self):
        return self.s_name

    def get_pid(self):
        return self.s_pid

    def get_seen_times(self):
        return self.i_seen_times

    def set_seen_times(self, i_seen_times):
        self.i_seen_times = i_seen_times

    def set_cmdline(self, s_cmdline):
        self.s_cmdline = s_cmdline

    def get_cmdline(self):
        return self.s_cmdline

    def set_fds(self, a_fds):
        self.a_fds = a_fds

    def get_fds(self):
        return self.a_fds

    def get_count_fds(self):
        return len(self.a_fds)

    def set_io_from_string(self,s_io):
        """
        Sets the io information from the process from a string obtained from /proc/${PID}/io

        rchar: number of bytes the process read, using any read-like system call (from files, pipes, tty...).
        wchar: number of bytes the process wrote using any write-like system call.
        syscr: number of read-like system call invocations that the process performed.
        syscw: number of write-like system call invocations that the process performed.
        read_bytes: number of bytes the process directly read from disk.
        write_bytes: number of bytes the process originally dirtied in the page-cache (assuming they will go to disk later).
        cancelled_write_bytes: number of bytes the process "un-dirtied" - e.g. using an "ftruncate" call that truncated pages from the page-cache.

        :param s_io:
        :return:
        """
        # @TODO: Complete
        self.d_s_io = {}
