#
# String Color Data Object
#
# Author: Carles Mateo
# Creation Date: 2019-11-23 11:54 IST
# Description: Object that stores an string and its color version.
#              Specially helpful for rendering the virtual screen


class StringColorDO:

    s_original = ""
    s_color_rendered = ""

    def __init__(self, s_original_text, s_color_rendered_version):
        self.s_original = s_original_text
        self.s_color_rendered = s_color_rendered_version

    def get_s_color_rendered(self):
        return self.s_color_rendered

    def get_s_original(self):
        return self.s_original

    def get_len_s_original(self):
        return len(self.s_original)
