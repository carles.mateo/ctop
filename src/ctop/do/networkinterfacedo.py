
class NetworkInterfaceDO:

    s_name = ""
    s_speed = ""
    s_duplex = ""
    s_mtu = ""
    s_iflink = ""
    # /sys/class/net/enp0s3/device/enable

    # Info from /proc/net/dev
    i_collisions = 0
    i_total_rx_errors = 0
    i_total_tx_errors = 0
    i_rx_crc_errors = 0
    i_tx_aborted_errors = 0

    # Total Data Sent in GB
    i_rx_bytes = 0
    i_tx_bytes = 0
    i_rx_bytes_prev = 0
    i_tx_bytes_prev = 0
    f_tx_data_gib = 0
    f_rx_data_gib = 0
    s_tx_data_gib_2decimals = ""
    s_rx_data_gib_2decimals = ""

    # operstate up/down
    s_operstate = ""

    # Keep the avg of the latest readings
    i_count_rx_bytes = 0
    i_MAX_COUNT_RX_BYTES = 59
    a_i_rx_bytes = []

    i_count_tx_bytes = 0
    i_MAX_COUNT_TX_BYTES = 59
    a_i_tx_bytes = []

    # Those fields will allow for clean up of non seen for a while Ethernet devices (specially Docker)
    i_first_epoch_time_seen = 0
    i_last_epoch_time_updated = 0

    # @TODO: In my way implementing
    # cat /sys/class/net/enp2s0/statistics/tx_packets
    # 99729090 (reserve 11 positions)
    # collisions -OK       rx_crc_errors -OK    rx_frame_errors      rx_over_errors        tx_carrier_errors    tx_fifo_errors
    # multicast            rx_dropped           rx_length_errors     rx_packets            tx_compressed        tx_heartbeat_errors
    # rx_bytes -OK         rx_errors            rx_missed_errors     tx_aborted_errors -OK tx_dropped           tx_packets
    # rx_compressed        rx_fifo_errors       rx_nohandler         tx_bytes -OK          tx_errors            tx_window_errors

    s_main_ip = ""

    def __init__(self, s_iface_name, s_speed, s_main_ip):
        self.s_name = s_iface_name
        self.set_speed(s_speed)
        self.set_main_ip(s_main_ip)
        self.set_operstate("")

    def get_name(self):
        return self.name

    def set_speed(self, s_speed):
        self.s_speed = s_speed

    def get_speed(self):
        return self.s_speed

    def set_main_ip(self, s_main_ip):
        self.s_main_ip = s_main_ip

    def get_main_ip(self):
        return self.s_main_ip

    def set_operstate(self, s_operstate):
        self.s_operstate = s_operstate

    def get_operstate(self):
        return self.s_operstate

    def get_mtu(self):
        return self.s_mtu

    def get_collisions(self):
        return self.i_collisions

    def set_collisions(self, i_collisions):
        self.i_collisions = i_collisions

    def get_total_rx_errors(self):
        return self.i_total_rx_errors

    def set_total_rx_errors(self, i_errors):
        self.i_total_rx_errors = i_errors

    def get_total_tx_errors(self):
        return self.i_total_tx_errors

    def set_total_tx_errors(self, i_errors):
        self.i_total_tx_errors = i_errors

    def get_rx_crc_errors(self):
        return self.i_rx_crc_errors

    def set_rx_crc_errors(self, i_errors):
        self.i_rx_crc_errors = i_errors

    def get_tx_aborted_errors(self):
        return self.i_tx_aborted_errors

    def set_tx_aborted_errors(self, i_errors):
        self.i_tx_aborted_errors = i_errors

    def get_tx_data_gib(self):
        return self.f_tx_data_gib

    def set_tx_data_gib(self, f_tx_data_gib):
        self.f_tx_data_gib = f_tx_data_gib

    def set_tx_bytes(self, i_tx_bytes):
        self.i_tx_bytes = i_tx_bytes
        self.add_new_tx_bytes_reading(i_tx_bytes)

    def set_tx_bytes_prev(self, i_tx_bytes_prev):
        self.i_tx_bytes_prev = i_tx_bytes_prev

    def get_tx_bytes(self):
        return self.i_tx_bytes

    def get_tx_bytes_prev(self):
        return self.i_tx_bytes_prev

    def get_rx_data_gib(self):
        return self.f_rx_data_gib

    def set_rx_data_gib(self, f_rx_data_gib):
        self.f_rx_data_gib = f_rx_data_gib

    def set_rx_bytes(self, i_rx_bytes):
        self.i_rx_bytes = i_rx_bytes
        self.add_new_rx_bytes_reading(i_rx_bytes)

    def set_rx_bytes_prev(self, i_rx_bytes_prev):
        self.i_rx_bytes_prev = i_rx_bytes_prev

    def get_rx_bytes(self):
        return self.i_rx_bytes

    def get_rx_bytes_prev(self):
        return self.i_rx_bytes_prev

    def set_tx_data_gib_2decimals(self, s_tx_data_gib_2decimals):
        self.s_tx_data_gib_2decimals = s_tx_data_gib_2decimals

    def get_tx_data_gib_2decimals(self):
        return self.s_tx_data_gib_2decimals

    def set_rx_data_gib_2decimals(self, s_rx_data_gib_2decimals):
        self.s_rx_data_gib_2decimals = s_rx_data_gib_2decimals

    def get_rx_data_gib_2decimals(self):
        return self.s_rx_data_gib_2decimals

    def add_new_rx_bytes_reading(self, i_rx_bytes_new):
        if len(self.a_i_rx_bytes) < (self.i_count_rx_bytes + 1):
            self.a_i_rx_bytes.append(i_rx_bytes_new)
        else:
            self.a_i_rx_bytes[self.i_count_rx_bytes] = i_rx_bytes_new

        self.i_count_rx_bytes += 1
        if self.i_count_rx_bytes > self.i_MAX_COUNT_RX_BYTES:
            self.i_count_rx_bytes = 0

    def get_avg_rx_bytes(self):
        if self.i_count_rx_bytes < 1:
            # No data yet, we avoid dividing by 0
            return 0

        i_total_bytes = 0
        i_min_value = self.a_i_rx_bytes[0]
        for i_bytes in self.a_i_rx_bytes:
            i_total_bytes = i_total_bytes + i_bytes
            if i_min_value > i_bytes:
                i_min_value = i_bytes

        i_total_bytes = i_total_bytes - (i_min_value * len(self.a_i_rx_bytes))

        i_avg_bytes = i_total_bytes / len(self.a_i_rx_bytes)

        return i_avg_bytes

    def add_new_tx_bytes_reading(self, i_tx_bytes_new):
        if len(self.a_i_tx_bytes) < (self.i_count_tx_bytes + 1):
            self.a_i_tx_bytes.append(i_tx_bytes_new)
        else:
            self.a_i_tx_bytes[self.i_count_tx_bytes] = i_tx_bytes_new

        self.i_count_tx_bytes += 1
        if self.i_count_tx_bytes > self.i_MAX_COUNT_TX_BYTES:
            self.i_count_tx_bytes = 0

    def get_avg_tx_bytes(self):
        """
        Get Avg bytes Transmitted in the period
        :return: i_bytes
        """
        if self.i_count_tx_bytes < 1:
            # No data yet, we avoid dividing by 0
            return 0

        i_total_bytes = 0
        i_min_value = self.a_i_tx_bytes[0]
        for i_bytes in self.a_i_tx_bytes:
            i_total_bytes = i_total_bytes + i_bytes
            if i_min_value > i_bytes:
                i_min_value = i_bytes

        i_total_bytes = i_total_bytes - (i_min_value * len(self.a_i_tx_bytes))

        i_avg_bytes = i_total_bytes / len(self.a_i_tx_bytes)

        return i_avg_bytes

    def set_last_epoch_time_updated(self, i_epoch_time):
        self.i_last_epoch_time_updated = i_epoch_time

    def get_last_epoch_time_updated(self):
        return self.i_last_epoch_time_updated

    def set_first_epoch_time_seen(self, i_epoch_time):
        self.i_first_epoch_time_seen = i_epoch_time

    def get_first_epoch_time_seen(self):
        return self.i_first_epoch_time_seen
