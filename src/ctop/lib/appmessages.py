#
# App Messages
#
# Author: Carles Mateo
# Creation Date: 2019-11-23 12:45 IST
# Description: Contains strings rendered in color
#

from lib.config import Config
from lib.screenutils import ScreenUtils
from lib.pythonutils import PythonUtils
from do.stringcolordo import StringColorDO


class AppMessages:

    o_s = None
    o_pythonutils = None

    d_s_messages = {}

    def get_version_name(self):
        s_version_name_bw = Config.VERSION_NAME + " v. " + Config.VERSION_NUMBER
        s_version_name_color = self.o_s.color_string(self.o_s.BOLD_BLUE, Config.VERSION_NAME) + \
                               " v. " + Config.VERSION_NUMBER
        return s_version_name_bw, s_version_name_color

    def get_code_name(self):
        s_code_name_bw = "Codename:" + " " + Config.VERSION_CODE
        s_code_name_color = self.o_s.color_label("Codename:") + " " + Config.VERSION_CODE
        return s_code_name_bw, s_code_name_color

    def get_version_and_code(self):
        s_version_name_bw, s_version_name_color = self.get_version_name()
        s_code_name_bw, s_code_name_color = self.get_code_name()

        s_version_and_code_bw = s_version_name_bw + " " + s_code_name_bw
        s_version_and_code_color = s_version_name_color + " " + s_code_name_color

        return s_version_and_code_bw, s_version_and_code_color

    def get_python_version_with_label(self):
        s_python_version, i_major, i_minor, i_micro, s_release_level = self.o_pythonutils.get_python_version()
        s_python_version_bw = "Python v.:"
        s_python_version_color = self.o_s.color_label(s_python_version_bw)
        s_python_version_bw = s_python_version_bw + " " + s_python_version
        s_python_version_color = s_python_version_color + " " + s_python_version

        return s_python_version_bw, s_python_version_color

    def get_version_and_code_and_python_version_with_label(self):
        s_version_and_code_bw, s_version_and_code_color = self.get_version_and_code()
        s_python_version_with_label_bw, s_python_version_with_label_color = self.get_python_version_with_label()

        s_version_and_code_and_python_version_with_label_bw = s_version_and_code_bw + " " + s_python_version_with_label_bw
        s_version_and_code_and_python_version_with_label_color = s_version_and_code_color + " " + s_python_version_with_label_color

        return s_version_and_code_and_python_version_with_label_bw, s_version_and_code_and_python_version_with_label_color

    def __init__(self, o_screenutils=None, o_pythonutils=PythonUtils()):
        self.o_s = o_screenutils
        self.o_pythonutils = o_pythonutils

        s_version_name_bw, s_version_name_color = self.get_version_name()
        o_version_name = StringColorDO(s_version_name_bw, s_version_name_color)

        s_code_name_bw, s_code_name_color = self.get_code_name()
        o_code_name = StringColorDO(s_code_name_bw, s_code_name_color)

        s_version_and_code_bw, s_version_and_code_color = self.get_version_and_code()
        o_version_and_code = StringColorDO(s_version_and_code_bw, s_version_and_code_color)

        s_python_version_with_label_bw, s_python_version_with_label_color = self.get_python_version_with_label()
        o_python_version_with_label = StringColorDO(s_python_version_with_label_bw, s_python_version_with_label_color)

        s_version_and_code_and_python_version_with_label_bw, s_version_and_code_and_python_version_with_label_color = self.get_version_and_code_and_python_version_with_label()
        o_version_and_code_and_python_version_with_label = StringColorDO(s_version_and_code_and_python_version_with_label_bw, s_version_and_code_and_python_version_with_label_color)

        self.d_s_messages["version_name"] = o_version_name
        self.d_s_messages["code_name"] = o_code_name
        self.d_s_messages["version_and_code"] = o_version_and_code
        self.d_s_messages["python_version_with_label"] = o_python_version_with_label
        self.d_s_messages["version_and_code_and_python_version_with_label"] = o_version_and_code_and_python_version_with_label

    def get_stringcolor(self, s_key):
        if s_key in self.d_s_messages:
            return self.d_s_messages[s_key]

        return None
