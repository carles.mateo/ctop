#
# CPU Utils
#
# Author: Carles Mateo
# Creation Date: 2019-11-26 12:40 IST
# Description: Class to deal with CPUs
#

from lib.fileutils import FileUtils
from do.cpudo import CpuDO

class CpuUtils:

    o_fileutils = None

    # Dictionary that will hold the data under keys "${PROCESSNAME}-${PID}"
    d_s_cpuinfo = {}
    i_processor_count = 0
    i_core_count = 0
    f_cpu_use = 0.0
    f_cpu_last_idle = 0.0
    f_cpu_last_total = 0.0

    s_cpu_family = ""
    s_CPU_RASPBERRY = "RASPBERRY PI"
    s_CPU_INTEL = "INTEL"
    s_CPU_AMD = "AMD"
    s_CPU_ARM_NOT_RASPBERRY = "ARM"

    def __init__(self, o_fileutils=FileUtils()):
        self.o_fileutils = o_fileutils

        self.update_cpuinfo()

    def update_cpuinfo(self):
        """
        Updates the d_s_cpuinfo dict from /proc/cpuinfo

        For Raspberry Pi 4 in Ubuntu 20.04LST cat /proc/cpuinfo returns:
        processor	: 0
        BogoMIPS	: 108.00
        Features	: fp asimd evtstrm crc32 cpuid
        CPU implementer	: 0x41
        CPU architecture: 8
        CPU variant	: 0x0
        CPU part	: 0xd08
        CPU revision	: 3

        processor	: 1
        BogoMIPS	: 108.00
        Features	: fp asimd evtstrm crc32 cpuid
        CPU implementer	: 0x41
        CPU architecture: 8
        CPU variant	: 0x0
        CPU part	: 0xd08
        CPU revision	: 3

        processor	: 2
        BogoMIPS	: 108.00
        Features	: fp asimd evtstrm crc32 cpuid
        CPU implementer	: 0x41
        CPU architecture: 8
        CPU variant	: 0x0
        CPU part	: 0xd08
        CPU revision	: 3

        processor	: 3
        BogoMIPS	: 108.00
        Features	: fp asimd evtstrm crc32 cpuid
        CPU implementer	: 0x41
        CPU architecture: 8
        CPU variant	: 0x0
        CPU part	: 0xd08
        CPU revision	: 3

        Hardware	: BCM2835
        Revision	: c03111
        Serial		: 100000000571eedf
        Model		: Raspberry Pi 4 Model B Rev 1.1


        :return:
        """
        s_filename = "/proc/cpuinfo"
        d_s_cpu_physicalid = {}
        b_result, a_s_cpuinfo = self.o_fileutils.read_file_as_lines(s_filename)

        self.i_processor_count = 0
        self.i_core_count = 0

        if b_result is True:

            # First Check for Detection of Raspberry Pi
            if "Raspberry Pi" in a_s_cpuinfo[-1]:
                # Is a Raspberry Pi
                self.s_cpu_family = CpuUtils.s_CPU_RASPBERRY
                # Assume single processor
                self.i_processor_count = 1
                d_s_cpu_physicalid[0] = 0

                for s_line in a_s_cpuinfo:
                    a_s_line = s_line.split(":")
                    if len(a_s_line) < 1:
                        break
                    a_s_line[0] = a_s_line[0].replace(' ', '')
                    a_s_line[0] = a_s_line[0].replace("\t", '')
                    if a_s_line[0] == "Model" and len(a_s_line) > 1:
                        self.d_s_cpuinfo["model_name"] = a_s_line[1].replace("\n", '').replace("\t", '').lstrip().rstrip()
                    if a_s_line[0] == "processor" and len(a_s_line) > 1:
                        self.i_core_count = self.i_core_count + 1

            else:
                # Assume Intel or AMD
                # @TODO: I need to do the Tests in the AMD Computer, so far Hardcoding Intel
                self.s_cpu_family = CpuUtils.s_CPU_INTEL

                for s_line in a_s_cpuinfo:
                    a_s_line = s_line.split(":")
                    if len(a_s_line) < 1:
                        break
                    a_s_line[0] = a_s_line[0].replace(' ', '')
                    a_s_line[0] = a_s_line[0].replace("\t", '')
                    if a_s_line[0] == "modelname" and len(a_s_line) > 1:
                        self.d_s_cpuinfo["model_name"] = a_s_line[1].replace("\n", '').replace("\t", '').lstrip().rstrip()
                    if a_s_line[0] == "physicalid" and len(a_s_line) > 1:
                        s_cpu_physicalid = a_s_line[1]
                        if s_cpu_physicalid not in d_s_cpu_physicalid:
                            d_s_cpu_physicalid[s_cpu_physicalid] = s_cpu_physicalid
                            self.i_processor_count = self.i_processor_count + 1
                    if a_s_line[0] == "coreid" and len(a_s_line) > 1:
                        self.i_core_count = self.i_core_count + 1

    def update_cpu_usage(self):

        #while True:
        with open('/proc/stat') as f:
            fields = [float(column) for column in f.readline().strip().split()[1:]]
        idle, total = fields[3], sum(fields)
        idle_delta, total_delta = idle - self.f_cpu_last_idle, total - self.f_cpu_last_total
        self.f_cpu_last_idle, self.f_cpu_last_total = idle, total
        f_utilisation = 100.0 * (1.0 - idle_delta / total_delta)

        #s_utilisation_formated = '%5.1f%%' % utilisation
        self.f_cpu_use = f_utilisation

        #return s_utilisation_formated

    def get_processor_count(self):
        return self.i_processor_count

    def get_core_count(self):
        return self.i_core_count

    def get_processor_name(self):
        s_model_name = ""
        if "model_name" in self.d_s_cpuinfo:
            s_model_name = self.d_s_cpuinfo['model_name']

        return s_model_name

    def get_cpu_use(self):
        """
        Returns the CPU use in float and as a string formatted
        """
        s_use_formated = ('%5.1f%%' % self.f_cpu_use).strip()

        return self.f_cpu_use, s_use_formated

    def get_cpu_family(self):
        """
        Returns the CPU family detected
        """

        return self.s_cpu_family
