#
# Class to work with Screen via ssh, and colors
#
# Author: Carles Mateo
# Creation Date: 2005 Barcelona
# Last Update: 2021-03-06 21:03 Ireland Added support for Python2
#

import sys
import shutil

#Not sure why it is in the module shutil, but it landed there in Python 3.3, Querying the size of the output terminal:

#>>> import shutil
#>>> shutil.get_terminal_size((80, 20))  # pass fallback
#os.terminal_size(columns=87, lines=23)  # returns a named-tuple

#A low-level implementation is in the os module.

#A backport is now available for Python 3.2 and below:

#    https://pypi.python.org/pypi/backports.shutil_get_terminal_size

#shareimprove this answer
#edited Mar 30 '17 at 17:45


class ScreenUtils:

    b_support_colors = True

    b_debug = False

    i_terminal_width = 80
    i_terminal_height = 25
    i_fixed_terminal_width = 0
    i_fixed_terminal_height = 0
    i_fixed_terminal_width_default = 130
    i_fixed_terminal_height_default = 25

    # Some colors, in case we want to give Colors to our CTop Application
    # For example Red for Errors.
    # Also in order to clear the screen, and repaint it from top, like top does, instead of just printing lines
    CLEAR = "\x1B[0m"
    CLEAR_SCREEN = "\033c"

    ITALIC_BLUE = "\x1B[34;3m"

    BOLD_BLACK = "\x1B[1;1m"
    BOLD_RED = "\x1B[31;1m"
    BOLD_GREEN = "\x1B[32;1m"
    BOLD_YELLOW = "\x1B[33;1m"
    BOLD_BLUE = "\x1B[34;1m"
    BOLD_DARK_GREY = "\x1B[37;1m"
    BOLD_YELLOW_BLINK = "\x1B[33;5;1m"

    BOLD_BLACK_BG_RED = "\x1B[1;41m"
    BOLD_BLACK_BG_GREEN = "\x1B[1;42m"
    BOLD_BLACK_BG_BLUE = "\x1B[1;44m"

    BLACK = "\x1B[2;49m"
    RED = "\x1B[31;49m"
    GREEN = "\x1B[32;49m"
    YELLOW = "\x1B[33;49m"
    BLUE = "\x1B[34;49m"
    MAGENTA = "\x1B[35;49m"
    CYAN = "\x1B[36;49m"
    WHITE = "\x1B[37;49m"
    DARK_GREY = "\x1B[37;2m"
    ORANGE = "\x1B[33;3m"

    def __init__(self, b_support_colors=True, i_fixed_terminal_width=0, i_fixed_terminal_height=0, b_debug=False):
        """
        If b_support_colors is False, like in the case of the crons or poor SSH Terminals colors will not be
        added.
        Also in the future when we support different Templates for colors, colors for types, like e.g. LABELS
        would be able to be customized very easily.
        :param b_support_colors:
        """
        # Future feature, to support resizing of output depending on the Terminal screen size
        # self.MIN_TERMINAL_WIDTH = 76

        self.b_debug = b_debug

        if i_fixed_terminal_width > 0:
            self.i_fixed_terminal_width = i_fixed_terminal_width

        if i_fixed_terminal_height > 0:
            self.i_fixed_terminal_height = i_fixed_terminal_height

        # Detect screen resolution and inits self.a_s_virtual_terminal[]
        self.update_terminal_size()

        self.b_support_colors = b_support_colors

        if b_support_colors is False:
            # Doesn't support colors, so nothing will be rendered without changes in the code
            self.ITALIC_BLUE = ""

            self.BLACK = ""
            self.RED = ""
            self.GREEN = ""
            self.WHITE = ""
            self.BLUE = ""
            self.YELLOW = ""
            self.CYAN = ""
            self.MAGENTA = ""
            self.DARK_GREY = ""
            self.ORANGE = ""

            # Clears current color
            self.CLEAR = ""

            self.BOLD_BLACK = ""
            self.BOLD_RED = ""
            self.BOLD_GREEN = ""
            self.BOLD_YELLOW = ""
            self.BOLD_BLUE = ""
            self.BOLD_DARK_GREY = ""
            self.BOLD_YELLOW_BLINK = ""

            self.BOLD_BLACK_BG_RED = ""
            self.BOLD_BLACK_BG_GREEN = ""
            self.BOLD_BLACK_BG_BLUE = ""

            self.CLEAR_SCREEN = ""

    def update_terminal_size(self):
        if "get_terminal_size" in dir(shutil):
            self.i_terminal_width, self.i_terminal_height = shutil.get_terminal_size((130, 25))  # pass fallback
        else:
            # It's Python 2. Force defaults as shutil doesn't have method get_germinal_size()
            if self.i_fixed_terminal_width > 0:
                self.i_terminal_width = self.i_fixed_terminal_width
            else:
                self.i_terminal_width = self.i_fixed_terminal_width_default

            if self.i_fixed_terminal_height > 0:
                self.i_terminal_height = self.i_fixed_terminal_height
            else:
                self.i_fixed_terminal_height = self.i_fixed_terminal_height_default

        i_terminal_height = self.get_terminal_height_to_use()

        self.a_s_virtual_terminal = [""] * i_terminal_height

    def reset_virtual_terminal(self):
        self.update_terminal_size()

        i_terminal_height = self.get_terminal_height_to_use()

        for i_index in range(0, i_terminal_height):
            # String formed by Spaces
            self.reset_virtual_terminal_line(i_index)

    def reset_virtual_terminal_line(self, i_y):
        #self.a_s_virtual_terminal[i_y] = " " * self.i_terminal_width
        #self.a_s_virtual_terminal[i_y] = "+" * self.i_terminal_width
        i_counter = 0

        i_terminal_width = self.get_terminal_width_to_use()

        if self.b_debug is False:
            self.a_s_virtual_terminal[i_y] = " " * i_terminal_width
        else:
            self.a_s_virtual_terminal[i_y] = ""
            for i_pos_x in range(0, i_terminal_width):
                self.a_s_virtual_terminal[i_y] = self.a_s_virtual_terminal[i_y] + str(i_counter)
                i_counter = i_counter + 1
                if i_counter >= 9:
                    i_counter = 0

    def get_terminal_width_to_use(self):
        if self.i_fixed_terminal_width > 0:
            i_terminal_width = self.i_fixed_terminal_width
        else:
            i_terminal_width = self.i_terminal_width

        return i_terminal_width

    def print_virtual_terminal(self):
        """
        Outputs the virtual terminal in the buffer
        :return:
        """

        i_terminal_height = self.get_terminal_height_to_use()

        for i_index in range(0, i_terminal_height):
            if i_index >= (i_terminal_height - 1):
                self.p_no_cr(self.a_s_virtual_terminal[i_index])
            else:
                self.p(self.a_s_virtual_terminal[i_index])

    def replace_a_string_from_a_pos_with_stringcolor(self, i_x, s_string_original, o_stringcolor):

        i_len_original = o_stringcolor.get_len_s_original()

        # @TODO: Fix bug
        s_rest = s_string_original[(i_x + i_len_original):]
        i_len_rest = len(s_rest)

        s_new_string = s_string_original[:i_x] + o_stringcolor.get_s_color_rendered() + s_rest

        # @ TODO: Continue debugging
        return s_new_string

    def replace_a_string_from_a_pos_with_string(self, i_x, s_string_original, s_string_to_add):

        i_len_string_to_add = len(s_string_to_add)
        s_string_original_to_add_to_tail = s_string_original[(i_x + i_len_string_to_add):]

        s_new_string = s_string_original[:i_x] + s_string_to_add + s_string_original_to_add_to_tail
        # @ TODO: Continue debugging
        return s_new_string

    def replace_a_string_from_a_pos(self, i_x, s_string, s_text_to_add):
        """
        Intended for replacing one character only, with one character
        :param i_x:
        :param s_string:
        :param s_text_to_add:
        :return:
        """
        return s_string[:i_x] + s_text_to_add + s_string[i_x + 1:]

    def vprint_in_x_y_stringcolor(self, i_x, i_y, o_stringcolor):
        if o_stringcolor is None:
            return

        if i_y >= len(self.a_s_virtual_terminal):
            # Resizing made the Virtual screen small
            return

        self.a_s_virtual_terminal[i_y] = self.replace_a_string_from_a_pos_with_stringcolor(i_x,
                                                                                         self.a_s_virtual_terminal[i_y],
                                                                                         o_stringcolor)

    def vprint_in_x_y(self, i_x, i_y, s_text):
        if s_text == "":
            return

        # I've to replace strings and remove spaces if I'm working with color
        # for i_index in range(0, len(s_text)):
        #     self.a_s_virtual_terminal[i_y] = self.replace_a_string_from_a_pos(i_x + i_index,
        #                                                                       self.a_s_virtual_terminal[i_y],
        #                                                                       s_text[i_index])

        if i_y >= len(self.a_s_virtual_terminal):
            # Resizing made the Virtual screen small
            return

        self.a_s_virtual_terminal[i_y] = self.replace_a_string_from_a_pos_with_string(i_x,
                                                                                      self.a_s_virtual_terminal[i_y],
                                                                                      s_text)

    def vprint_in_bottom(self, s_text):
        i_terminal_height = self.get_terminal_height_to_use()

        self.vprint_in_x_y(0, i_terminal_height - 1, s_text)

    def get_terminal_height_to_use(self):
        if self.i_fixed_terminal_height > 0:
            i_terminal_height = self.i_fixed_terminal_height
        else:
            i_terminal_height = self.get_i_terminal_height()

        return i_terminal_height

    def vcalc_x_aligned_to_right_y_with_virtual_screen_fixed_width(self, i_x_margin=0, s_text=""):
        """
        Calculates the x pos
        :param i_x_margin: Margin from the right
        :param s_text:
        :return:
        """

        i_terminal_width = self.get_terminal_width_to_use()
        i_x_position = i_terminal_width - i_x_margin - len(s_text)

        return i_x_position

    def vcalc_x_aligned_to_right_y_with_string(self, i_x_margin, i_width, s_text):
        """
        Calculates the x pos for a specified i_width of the line/screen
        :param i_x_margin:
        :param i_width:
        :param s_text:
        :return:
        """
        i_x_position = i_width - i_x_margin - len(s_text)

        return i_x_position

    def vcalc_x_aligned_to_right_y_with_virtual_line_y(self, i_x_margin, i_y, s_text):
        """
        Calculates the x pos for a specified i_width of the line/screen
        :param i_x_margin:
        :param i_width:
        :param s_text:
        :return:
        """

        if i_y >= len(self.a_s_virtual_terminal):
            # Resizing made the Virtual screen small
            return 0

        i_x_position = self.vcalc_x_aligned_to_right_y_with_string(i_x_margin,
                                                                   len(self.a_s_virtual_terminal[i_y]),
                                                                   s_text)

        return i_x_position

    def vprint_aligned_to_right_y(self, i_x_margin=0, i_y=0, s_text=""):
        """
        Will align to the right of the line i_y the text.
        :param i_x_margin:
        :param i_y:
        :param s_text:
        :return:
        """

        # Check if we are in the virtual screen
        if i_y >= len(self.a_s_virtual_terminal):
            return

        i_x_position = self.vcalc_x_aligned_to_right_y_with_virtual_line_y(i_x_margin,
                                                                           i_y,
                                                                           s_text)

        self.vprint_in_x_y(i_x_position, i_y, s_text)

    def vprint_aligned_to_right_stringcolor(self, i_x_margin=0, i_y=0, o_stringcolor=None):
        # Check if we are in the virtual screen
        if i_y >= self.i_terminal_height:
            return

        i_x = self.vcalc_x_aligned_to_right_y_with_virtual_line_y(i_x_margin=i_x_margin,
                                                                  i_y=i_y,
                                                                  s_text=o_stringcolor.get_s_original())
        self.vprint_in_x_y_stringcolor(i_x=i_x, i_y=i_y, o_stringcolor=o_stringcolor)

    def color_string(self, s_color, s_text):
        s_text = s_color + s_text + self.CLEAR

        return s_text

    def color_error(self, s_text):
        """
        Returns an string with the color code for error, typically red
        :param s_text:
        :return: String
        """
        s_text = self.RED + s_text + self.CLEAR

        return s_text

    def color_label(self, s_text):
        """
        Returns an string with the color code for a label, typically white
        :param s_text:
        :return: String
        """
        s_text = self.BOLD_BLUE + s_text + self.CLEAR

        return s_text

    def color_additional_info(self, s_text):
        """
        Returns an string with the color code for additional info, typically grey
        :param s_text:
        :return: String
        """
        s_text = self.DARK_GREY + s_text + self.CLEAR

        return s_text

    def p(self, s_text):
        """
        Prints whatever is sent to the Standard Output.
        This method could allow to capture the text printed and Log to a file.
        Also allows easily portability from Python2.7 to Python3.6
        :param s_text: Text to Print
        :return:
        """
        print(s_text)

    def p_no_cr(self, s_text):
        """
        Prints whatever is sent to the Standard Output without finishing Enter.
        This method could allow to capture the text printed and Log to a file.
        Also allows easily portability from Python2.7 to Python3.6
        :param s_text: Text to Print
        :return:
        """
        sys.stdout.write(s_text)
        sys.stdout.flush()

    def perror(self, s_text):
        """
        Prints whatever is sent to the Standard Output surrounded by the error escape sequence.
        This method could allow to capture the text printed and Log to a file.
        Also allows easily portability from Python2.7 to Python3.6
        :param s_text: Text to Print
        :return:
        """
        print(self.color_error(s_text))

    def clear_screen(self):
        """"
        Clears the screen if support for colors is enabled
        """
        if self.b_support_colors is False:
            return

        # self.reset_virtual_terminal()
        self.p_no_cr(self.CLEAR_SCREEN)

    def get_i_terminal_height(self):
        return self.i_terminal_height

    def get_i_terminal_width(self):
        return self.i_terminal_width

    def color_alert(self, s_text):
        s_text = self.BOLD_RED + s_text + self.CLEAR

        return s_text

    def color_warning(self, s_text):
        s_text = self.BOLD_YELLOW + s_text + self.CLEAR

        return s_text

    def color_ok(self, s_text):
        s_text = self.BOLD_GREEN + s_text + self.CLEAR

        return s_text
