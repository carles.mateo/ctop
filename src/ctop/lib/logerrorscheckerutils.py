#
# File Utils
#
# Author: Carles Mateo
# Creation Date: 2017-07-01 10:10 GMT+1
# Description: Class to deal with Files and Directories.
#              Please try to keep it lightweight and without other library dependencies.
#

import os

# Error messages pending:
# =======================
# Buffer I/O error on dev sda3
# JBD2: Error
# EXT4-fs (sda3): previous I/O error to superblock detected

# Error messages implemented:
# ===========================
# NFS Timed out:
# [Sun May 31 14:25:01 2020] nfs: server  not responding, timed out

# Out of Memory:
# Out of memory: Kill process 11227 (rsyslogd) score 359 or sacrifice child
# Out of memory: Kill process
# Out of memory: Killed process 30382

# RabbitMQ:
# <AMQPError: unknown error>
# AMQP server on xxxx:5672 is unreachable: timed out.
# AMQP server xxxx:5672 closed the connection. Check login credentials: Socket closed: IOError: Socket closed

# (Errors in hard drive)
# blk_update_request: I/O error, dev sda, sector 23580784


class LogErrorsCheckerUtils:

    # Warning Special file
    s_dmesg_file = "/dev/kmsg"

    s_logs_kernel = "/var/log/kern.log"
    s_logs_messages = "/var/log/messages"

    a_s_all_logs = [s_logs_kernel,
                    s_logs_messages]

    o_fileutils = None

    # Loaded file in array of strings, format
    a_lines = []

    i_errors_nfs_timed_out = 0
    i_errors_memory_read = 0
    i_errors_out_of_memory = 0
    i_errors_rabbitmq = 0
    i_errors_hard_drive = 0

    def __init__(self, o_fileutils=None):
        self.o_fileutils = o_fileutils

    def load_all_logs(self):
        # Clear the logs
        self.a_lines = []

        # Load the logs
        for s_log_file in self.a_s_all_logs:
            b_success = self.append_file(s_file=s_log_file)

    def load_dmesg(self):
        b_success = self.load_file(self.s_dmesg_file)

        return b_success

    def load_logs_kernel(self):
        b_success = self.load_file(self.s_logs_kernel)

        return b_success

    def load_logs_messages(self):
        b_success = self.load_file(self.s_logs_messages)

        return b_success

    def append_file(self, s_file):
        """
        Appends a log file to the existing
        :param s_file: The Filename
        :return: Boolean
        """
        a_lines = self.a_lines
        b_success = self.load_file(s_file)
        if b_success is True and len(self.a_lines) > 0:
            self.a_lines = a_lines + self.a_lines

        return b_success

    def load_file(self, s_file):
        """
        Loads a file into memory
        :param s_file: The Filename
        :return: Boolean
        """
        b_success, a_lines = self.o_fileutils.read_file_as_lines(s_file)
        if b_success is True:
            self.a_lines = a_lines

        return b_success

    def find_string_in_loaded_file(self, s_string):
        """
        Search for a string in the previously loaded log file
        :param s_string: String to search
        :return: Boolean, number of occurrences, array with the errors
        """
        b_found = False
        i_found = 0
        a_found = []

        s_string = s_string.lower()

        for s_line in self.a_lines:
            if s_string in s_line.lower():
                b_found = True
                i_found = i_found + 1
                a_found.append(s_line)

        return b_found, i_found, a_found

    def search_in_array(self, a_lines, s_search):
        b_found = False
        i_found = 0
        a_subset_found = []

        s_search = s_search.lower()

        for s_line in a_lines:
            if s_search in s_line.lower():
                b_found = True
                i_found = i_found + 1
                a_subset_found.append(s_line)

        return b_found, i_found, a_subset_found

    def search_for_hard_drive_errors(self):
        """
        Search for Errors in the Hard drives, of the type:
            blk_update_request: I/O error
        :return: Boolean
        :return: Integer: Number of errors
        :return: List: List of errors
        """

        b_found_errors = False
        i_found_errors = 0
        a_subset_found_times = []

        b_found, i_found_errors, a_subset_found = self.find_string_in_loaded_file("blk_update_request:")
        if b_found is True:
            b_found_errors, i_found_errors_io, a_subset_found_timed = self.search_in_array(a_subset_found, "I/O error")

        self.i_errors_hard_drive = i_found_errors_io

        return b_found_errors, i_found_errors, a_subset_found_times

    def search_for_nfs_timeout_errors(self):
        """
        Search for NFS error messages.
        :return: Boolean
        :return: Integer: Number of errors
        :return: List: List of errors
        """
        b_found = False
        i_found = 0
        a_subset_found_times = []

        b_found, i_found, a_subset_found = self.find_string_in_loaded_file("nfs:")

        if b_found is True:
            b_found, i_found, a_subset_found_timed = self.search_in_array(a_subset_found, "timed")

        self.i_errors_nfs_timed_out = i_found

        return b_found, i_found, a_subset_found_times

    def search_for_memory_read_errors(self):
        """
        Search for memory read error
        :return: Boolean
        :return: Integer: Number of errors
        :return: List: List of errors
        """
        b_found = False
        i_errors_found = 0
        a_subset_found_times = []

        b_found, i_errors_found, a_subset_found = self.find_string_in_loaded_file("memory read error")

        self.i_errors_memory_read = i_errors_found

        b_found, i_errors_found2, a_subset_found2 = self.find_string_in_loaded_file("memory error")

        self.i_errors_memory_read = self.i_errors_memory_read + i_errors_found2

        i_errors_found = i_errors_found + i_errors_found2

        a_subset_found_times = a_subset_found + a_subset_found2

        return b_found, i_errors_found, a_subset_found_times

    def search_for_out_of_memory_errors(self):
        """
        Search for Out of Memory error messages. When a process was killed due to this
        :return: Boolean
        :return: Integer: Number of errors
        :return: List: List of errors
        """
        b_found = False
        i_found = 0
        a_subset_found_times = []

        b_found, i_found, a_subset_found = self.find_string_in_loaded_file("Out of memory:")

        self.i_errors_out_of_memory = i_found

        return b_found, i_found, a_subset_found_times

    def search_for_rabbitmq_errors(self):
        """
        Search for RabbitMQ errors
        :return: Boolean
        :return: Integer: Number of errors
        :return: List: List of errors
        """
        b_found = False
        i_errors_found = 0
        i_errors_found_timed = 0
        i_errors_found_ioerror = 0
        a_subset_found_timed = []
        a_subset_found_ioerror = []
        a_subset_found_total = []

        # First Error
        b_found, i_errors_found, a_subset_found = self.find_string_in_loaded_file("AMQPError: unknown error")

        self.i_errors_rabbitmq = i_errors_found

        # Second and Third Errors
        b_found, i_errors_found_possible, a_subset_found = self.find_string_in_loaded_file("AMQP server")
        if b_found is True:
            b_found_timed, i_errors_found_timed, a_subset_found_timed = self.search_in_array(a_subset_found, "timed")
            b_found_ioerror, i_errors_found_ioerror, a_subset_found_ioerror = self.search_in_array(a_subset_found, "IOError")

            if b_found_timed is True or b_found_ioerror is True:
                b_found = True

            a_subset_found_total = a_subset_found_timed + a_subset_found_ioerror

        i_errors_found2 = i_errors_found + i_errors_found_timed
        self.i_errors_rabbitmq = self.i_errors_rabbitmq + i_errors_found2

        return b_found, i_errors_found, a_subset_found_total

    def get_errors_nfs_timed_out(self):
        return self.i_errors_nfs_timed_out

    def get_errors_memory_read(self):
        return self.i_errors_memory_read

    def get_errors_out_of_memory(self):
        return self.i_errors_out_of_memory

    def get_errors_rabbitmq(self):
        return self.i_errors_rabbitmq

    def get_errors_hard_drive(self):
        return self.i_errors_hard_drive

    def reset_counters(self):
        self.i_errors_nfs_timed_out = 0
        self.i_errors_memory_read = 0
        self.i_errors_out_of_memory = 0
        self.i_errors_rabbitmq = 0

    def update_logs(self):
        """
        Updates the internal counters based on the errors found on the logs.
        """
        self.reset_counters()

        # Errors that can be in the Kernel log
        self.load_all_logs()
        b_found, i_errors_found_nfs_timeout, a_subset_found_times = self.search_for_nfs_timeout_errors()
        self.i_errors_nfs_timed_out = i_errors_found_nfs_timeout
        b_found, i_errors_found_memory_read, a_subset_found_times = self.search_for_memory_read_errors()
        self.i_errors_memory_read = i_errors_found_memory_read
        b_found, i_errors_found_out_of_memory, a_subset_found_times = self.search_for_out_of_memory_errors()
        self.i_errors_out_of_memory = i_errors_found_out_of_memory
        b_found, i_errors_found_rabbitmq, a_subset_found_times = self.search_for_rabbitmq_errors()
        self.i_errors_rabbitmq = i_errors_found_rabbitmq
