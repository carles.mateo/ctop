#
# Network Utils
#
# Author: Carles Mateo
# Creation Date: 2017-01-01 21:59 IST
# Description: Class to return Network Interfaces
#

import socket
import fcntl
import struct
from do.networkinterfacedo import NetworkInterfaceDO


class NetworkUtils:

    s_net_path = "/sys/class/net"

    # Sockets open
    # cat / proc / net / sockstat
    # Open ports
    # /proc/net/tcp
    # Next, we will need to retrieve information from the proc filesystem. For TCP connections,
    # we will need to analyse the /proc/net/tcp file.
    # We will need to filter for source port 0x0050 (hexadecimal for port 80 and zero padded to 4 digits)
    # and the state must be 0x0A - LISTEN state.
    #
    # cat /proc/net/tcp | head -1 ; cat /proc/net/tcp | grep :0050
    #   sl  local_address rem_address   st tx_queue rx_queue tr tm->when retrnsmt   uid  timeout inode
    #    2: 00000000:0050 00000000:0000 0A 00000000:00000000 00:00000000 00000000     0        0 8516 1 ffff8801fec69a00 100 0 0 10 -1
    #    7: 0B0AA8C0:ED11 195EBD5B:0050 08 00000000:00000001 00:00000000 00000000   107        0 10813 1 ffff8801fec6b400 74 4 16 10 -1
    #


    o_fileutils = None

    d_o_known_interfaces = {}

    a_listening_ports = []

    def __init__(self, o_fileutils, o_datetimeutils):
        self.o_fileutils = o_fileutils
        self.o_datetimeutils = o_datetimeutils

    def update_listening_ports(self):
        # Reset the list
        self.a_listening_ports = []

        b_success, s_listening_ports_tcp = self.o_fileutils.read_file_as_string("/proc/net/tcp")
        b_success6, s_listening_ports_tcp6 = self.o_fileutils.read_file_as_string("/proc/net/tcp6")

        s_listening_ports_tcp = s_listening_ports_tcp + s_listening_ports_tcp6
        s_listening_ports_tcp = s_listening_ports_tcp.strip()

        # 2: 00000000:0050 00000000:0000 0A 00000000:00000000 00:00000000 00000000     0        0 8516 1 ffff8801fec69a00 100 0 0 10 -1
        a_listening_ports = s_listening_ports_tcp.split("\n")

        for s_line in a_listening_ports:
            # Remove initial spaces
            s_line = s_line.lstrip()
            a_net_line = s_line.split(' ')
            if len(a_net_line) > 10:
                # Seems like an OK Line
                s_local_listening_port_hex = a_net_line[1]
                a_listening_port_line = s_local_listening_port_hex.split(":")
                if len(a_listening_port_line) == 2:
                    s_local_ip_hex = a_listening_port_line[0]
                    # @TODO: Associate by Ip address.
                    # @TODO: Mark in red listening in more than one Ip Address.
                    s_listening_port_hex = a_listening_port_line[1]
                    i_listening_port = int(s_listening_port_hex, 16)
                    s_listening_port_dec = str(i_listening_port)
                    s_status = a_net_line[3]
                    if s_status == '0A':
                        # 0x0A Means listening
                        # List is numeric to sort it as numbers easily
                        self.a_listening_ports.append(i_listening_port)

        self.a_listening_ports.sort()

    def get_listening_ports(self):
        return self.a_listening_ports

    def get_all_network_interfaces_as_array(self):
        # get network interfaces
        b_success, a_s_net_interfaces = self.o_fileutils.list_dir(self.s_net_path)

        return b_success, a_s_net_interfaces

    def get_network_iface_speed(self, s_name):
        b_success, s_speed = self.o_fileutils.read_file_as_string("/sys/class/net/" + s_name + "/speed")

        s_speed = s_speed.strip()

        if b_success is False:
            s_speed = "-1"

        return b_success, s_speed

    def get_operstate(self, s_name):
        b_success, s_operstate = self.o_fileutils.read_file_as_string("/sys/class/net/" + s_name + "/operstate")

        s_operstate = s_operstate.strip()

        if b_success is False or s_operstate == "unknown":
            s_operstate = "?"

        return b_success, s_operstate

    def get_all_network_inferfaces_as_dict_of_objects(self):

        d_o_net_interfaces = {}

        b_success, a_s_net_interfaces = self.get_all_network_interfaces_as_array()

        i_unix_epoch = int(self.o_datetimeutils.get_unix_epoch())

        if b_success is True:
            for s_iface in a_s_net_interfaces:
                b_success_iface, s_speed = self.get_network_iface_speed(s_iface)
                b_success_main_ip, s_main_ip = self.get_main_ip_address_for_interface(s_iface)
                b_success_operstate, s_operstate = self.get_operstate(s_iface)
                b_success_collisions, i_collisions = self.get_collision_for_interface(s_iface)
                i_rx_total_error_count, i_rx_crc_errors = self.get_rx_total_error_count_for_interface(s_iface)
                i_tx_total_error_count, i_tx_aborted_errors = self.get_tx_total_error_count_for_interface(s_iface)
                # Get Data tx and rx
                b_success_rx_bytes, i_rx_bytes = self.get_rx_bytes_for_interface(s_iface)
                b_success_tx_bytes, i_tx_bytes = self.get_tx_bytes_for_interface(s_iface)

                if s_iface in self.d_o_known_interfaces:
                    o_network_interfacedo = self.d_o_known_interfaces[s_iface]
                    o_network_interfacedo.set_operstate(s_operstate)
                    # Update Data that may have changed
                    o_network_interfacedo.s_speed = s_speed
                    o_network_interfacedo.s_main_ip = s_main_ip
                    o_network_interfacedo.set_last_epoch_time_updated(i_unix_epoch)
                else:
                    o_network_interfacedo = NetworkInterfaceDO(s_iface, s_speed, s_main_ip)
                    o_network_interfacedo.set_first_epoch_time_seen(i_unix_epoch)
                    o_network_interfacedo.set_last_epoch_time_updated(i_unix_epoch)
                    self.d_o_known_interfaces[s_iface] = o_network_interfacedo

                # Update the errors on the interface
                o_network_interfacedo.set_collisions(i_collisions)
                o_network_interfacedo.set_total_rx_errors(i_rx_total_error_count)
                o_network_interfacedo.set_total_tx_errors(i_tx_total_error_count)
                o_network_interfacedo.set_rx_crc_errors(i_rx_crc_errors)
                o_network_interfacedo.set_tx_aborted_errors(i_tx_aborted_errors)

                i_tx_bytes_prev = o_network_interfacedo.get_tx_bytes()
                i_rx_bytes_prev = o_network_interfacedo.get_rx_bytes()
                o_network_interfacedo.set_tx_bytes(i_tx_bytes)
                o_network_interfacedo.set_rx_bytes(i_rx_bytes)
                o_network_interfacedo.set_tx_bytes_prev(i_tx_bytes_prev)
                o_network_interfacedo.set_rx_bytes_prev(i_rx_bytes_prev)

                f_tx_gib = i_tx_bytes / 1024.00 / 1024.00 / 1024.00
                o_network_interfacedo.set_tx_data_gib(f_tx_gib)
                f_rx_gib = i_rx_bytes / 1024.00 / 1024.00 / 1024.00
                o_network_interfacedo.set_rx_data_gib(f_rx_gib)

                o_network_interfacedo.set_tx_data_gib_2decimals(("%.2f" % f_tx_gib))
                o_network_interfacedo.set_rx_data_gib_2decimals(("%.2f" % f_rx_gib))

                d_o_net_interfaces[s_iface] = o_network_interfacedo

            self.clean_interfaces_not_updated_before(i_unix_epoch)

            #return b_success, d_o_net_interfaces
            return b_success, self.d_o_known_interfaces
        else:
            return False, {}

    def clean_interfaces_not_updated_before(self, i_epoch_time):

        # We use this to prevent RuntimeError: dictionary changed size during iteration
        d_o_known_interfaces_copy = self.d_o_known_interfaces.copy()

        for s_iface in self.d_o_known_interfaces:
            o_network_interfacedo = self.d_o_known_interfaces[s_iface]
            if o_network_interfacedo.get_last_epoch_time_updated() < i_epoch_time:
                del d_o_known_interfaces_copy[s_iface]

        self.d_o_known_interfaces = d_o_known_interfaces_copy

    def get_main_ip_address_for_interface(self, s_interface_name):
        """ Gets the ip address for a particular interface.
            May need to be changed if multiple ips on an interface are to be supported
            Currently only returns the primary ip address of the interface
        """

        b_success = False

        try:
            s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            s_main_ip = socket.inet_ntoa(fcntl.ioctl(
                s.fileno(),
                0x8915,  # SIOCGIFADDR
                struct.pack('256s', s_interface_name[:15].encode('utf-8'))
            )[20:24])
            s.close()
        except IOError:
            s_main_ip = "unknown"

        return b_success, s_main_ip

    def get_rx_total_error_count_for_interface(self, s_iface_name):
        i_total_errors = 0

        b_success, i_rx_crc_errors = self.get_rx_crc_errors_for_interface(s_iface_name)
        if b_success is True:
            i_total_errors = i_total_errors + i_rx_crc_errors

        return i_total_errors, i_rx_crc_errors

    def get_rx_crc_errors_for_interface(self, s_iface_name):
        i_rx_crc_errors = 0

        b_success, s_rx_crc_errors = self.o_fileutils.read_file_as_string("/sys/class/net/" +
                                                                          s_iface_name +
                                                                 "/statistics/rx_crc_errors")
        if b_success is True:
            i_rx_crc_errors = int(s_rx_crc_errors)

        return b_success, i_rx_crc_errors

    def get_tx_total_error_count_for_interface(self, s_iface_name):
        i_total_errors = 0

        b_success, i_aborted_errors = self.get_tx_aborted_errors_for_interface(s_iface_name)
        if b_success is True:
            i_total_errors = i_total_errors + i_aborted_errors

        return i_total_errors, i_aborted_errors

    def get_tx_aborted_errors_for_interface(self, s_iface_name):
        i_aborted_errors = 0

        b_success, s_aborted_errors = self.o_fileutils.read_file_as_string("/sys/class/net/" +
                                                                           s_iface_name +
                                                                 "/statistics/tx_aborted_errors")

        if b_success is True:
            i_aborted_errors = int(s_aborted_errors)

        return b_success, i_aborted_errors

    def get_collision_for_interface(self, s_iface_name):
        i_collisions = 0

        b_success, s_collisions = self.o_fileutils.read_file_as_string("/sys/class/net/" + s_iface_name + "/statistics/collisions")

        if b_success is True:
            i_collisions = int(s_collisions)

        return b_success, i_collisions

    def get_rx_bytes_for_interface(self, s_iface_name):
        i_bytes = 0

        b_success, s_bytes = self.o_fileutils.read_file_as_string("/sys/class/net/" + s_iface_name + "/statistics/rx_bytes")

        if b_success is True:
            i_bytes = int(s_bytes)

        return b_success, i_bytes

    def get_tx_bytes_for_interface(self, s_iface_name):
        i_bytes = 0

        b_success, s_bytes = self.o_fileutils.read_file_as_string("/sys/class/net/" + s_iface_name + "/statistics/tx_bytes")

        if b_success is True:
            i_bytes = int(s_bytes)

        return b_success, i_bytes
