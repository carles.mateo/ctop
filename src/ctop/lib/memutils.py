#
# CPU Utils
#
# Author: Carles Mateo
# Creation Date: 2019-11-26 12:40 Ireland.
# Last updated: 2021-03-08 17:14 Ireland. Modifications for supporting Python 2 too.
# Description: Class to deal with CPUs
#


class MemUtils:

    def __init__(self, o_fileutils=None, o_stringutils=None):
        # Dictionary that will hold the data under keys "${PROCESSNAME}-${PID}"
        self.d_s_meminfo = {}
        self.i_processor_count = 0

        self.o_fileutils = o_fileutils
        self.o_stringutils = o_stringutils

        self.update_meminfo()

    def read_proc_meminfo_as_array(self):
        """
        Read /proc/meminfo
        :return: boolean, array
        """

        s_filename = "/proc/meminfo"
        b_result, a_s_meminfo = self.o_fileutils.read_file_as_lines(s_filename)

        return b_result, a_s_meminfo

    def update_meminfo(self):
        """
        Updates the d_s_meminfo dict from /proc/meminfo
        Example:
        MemTotal:       65639012 kB
        MemFree:         2837536 kB
        MemAvailable:    6291188 kB
        Buffers:         1209868 kB
        Cached:          8753908 kB
        SwapCached:            0 kB
        Active:         30979152 kB
        Inactive:        8098196 kB
        Active(anon):   29115936 kB
        Inactive(anon):  6329360 kB
        Active(file):    1863216 kB
        Inactive(file):  1768836 kB
        Unevictable:          64 kB
        Mlocked:              64 kB
        SwapTotal:             0 kB
        SwapFree:              0 kB
        :return:
        """

        self.i_processor_count = 0

        b_result, a_s_meminfo = self.read_proc_meminfo_as_array()
        if b_result is True:
            for s_line in a_s_meminfo:
                a_s_line = s_line.split(":")
                if len(a_s_line) < 1 or (len(a_s_line) > 1 and a_s_line[1] == ""):
                    break
                a_s_line[0] = a_s_line[0].replace(' ', '')
                a_s_line[0] = a_s_line[0].replace("\t", '').lstrip().rstrip()
                if a_s_line[0] == "MemTotal" and len(a_s_line) > 1:
                    self.d_s_meminfo["memtotal"] = a_s_line[1].replace("\n", '').replace("\t", '').lstrip().rstrip()
                if a_s_line[0] == "MemFree" and len(a_s_line) > 1:
                    self.d_s_meminfo["memfree"] = a_s_line[1].replace("\n", '').replace("\t", '').lstrip().rstrip()
                if a_s_line[0] == "MemAvailable" and len(a_s_line) > 1:
                    self.d_s_meminfo["memavailable"] = a_s_line[1].replace("\n", '').replace("\t", '').lstrip().rstrip()
                if a_s_line[0] == "Buffers" and len(a_s_line) > 1:
                    self.d_s_meminfo["buffers"] = a_s_line[1].replace("\n", '').replace("\t", '').lstrip().rstrip()
                if a_s_line[0] == "Cached" and len(a_s_line) > 1:
                    self.d_s_meminfo["cached"] = a_s_line[1].replace("\n", '').replace("\t", '').lstrip().rstrip()
                if a_s_line[0] == "SwapTotal" and len(a_s_line) > 1:
                    self.d_s_meminfo["swaptotal"] = a_s_line[1].replace("\n", '').replace("\t", '').lstrip().rstrip()
                if a_s_line[0] == "SwapFree" and len(a_s_line) > 1:
                    self.d_s_meminfo["swapfree"] = a_s_line[1].replace("\n", '').replace("\t", '').lstrip().rstrip()

    def get_mem_total(self):
        """
        Returns the value from /proc/meminfo under MemTotal.
        Return something like: 65638452 kB
        :return: string
        """
        s_memtotal = ""
        if "memtotal" in self.d_s_meminfo:
            s_memtotal = self.d_s_meminfo['memtotal']

        return s_memtotal

    def get_memfree(self):
        """
        Returns the value from /proc/meminfo under MemFree.
        Return something like: 5638452 kB
        :return: string
        """
        s_memfree = ""
        if "memfree" in self.d_s_meminfo:
            s_memfree = self.d_s_meminfo['memfree']

        return s_memfree

    def get_memavailable(self):
        """
        Returns the value from /proc/meminfo under MemFree.
        Return something like: 5638452 kB
        :return: string
        """
        s_memavailable = ""
        if "memavailable" in self.d_s_meminfo:
            s_memavailable = self.d_s_meminfo['memavailable']

        return s_memavailable

    def get_swaptotal(self):
        s_swaptotal = ""
        if "swaptotal" in self.d_s_meminfo:
            s_swaptotal = self.d_s_meminfo['swaptotal']

        return s_swaptotal

    def get_swapfree(self):
        s_swapfree = ""
        if "swapfree" in self.d_s_meminfo:
            s_swapfree = self.d_s_meminfo['swapfree']

        return s_swapfree

    def get_swapinuse_in_kb(self, b_add_unit=True):
        """
        Returns swap in use in KB.
        :return:
        """
        s_swaptotal_with_units = self.get_swaptotal()
        s_swaptotal_without_units = s_swaptotal_with_units.upper().replace(" KB", "")
        f_swaptotal = float(s_swaptotal_without_units)

        s_swapfree_with_units = self.get_swapfree()
        s_swapfree_without_units = s_swapfree_with_units.upper().replace(" KB", "")
        f_swapfree = float(s_swapfree_without_units)

        f_swapinuse_kb = f_swaptotal - f_swapfree
        i_swapinuse_kb = int(f_swapinuse_kb)

        s_swapinuse_kb = str(i_swapinuse_kb)

        if b_add_unit is True:
            s_swapinuse_kb = s_swapinuse_kb  + " kB"

        # f_swapinuse_gb = float(self.o_stringutils.convert_kb_to_gb(s_swapinuse_kb, b_add_units=False))
        # s_swapinuse_gb = str(f_swapinuse_gb)

        return s_swapinuse_kb
