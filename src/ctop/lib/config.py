

class Config:

    VERSION_NAME = "CTop"
    VERSION_NUMBER = "0.8.9"
    VERSION_CODE = "Catalan Republic"

    b_debug = False

    def get_debug(self):
        return self.b_debug

    def set_debug(self, b_debug):
        self.b_debug = b_debug
