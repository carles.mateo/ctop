from lib.fileutils import FileUtils


class KernelUtils:

    o_fileutils = None

    s_swappiness = "N/A"

    def __init__(self, o_fileutils=FileUtils()):
        self.o_fileutils = o_fileutils

    def get_swappiness(self):
        """Simple getter
        :return: s_swapiness: String
        """
        return self.s_swappiness

    def get_system_swappiness(self):
        """
        Read the swappines from /proc/sys/vm/swappiness
        :return: Boolean True for success, info as string
        """
        b_success = False
        s_swappiness = ""

        b_success, s_swappiness = self.o_fileutils.read_file_as_string("/proc/sys/vm/swappiness")
        s_swappiness = s_swappiness.replace(chr(0), ' ').strip()

        return b_success, s_swappiness

    def update_system_swappiness(self):
        """Updates the internal values for swappiness"""
        b_success_swappiness, s_swappiness = self.get_system_swappiness()

        if b_success_swappiness is False:
            s_swappiness = "N/A"

        self.s_swappiness = s_swappiness
