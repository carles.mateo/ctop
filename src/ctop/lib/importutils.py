#
# ImportUtils
#
# Author: Carles Mateo
# Creation Date: 2014 Barcelona
# Last Update: 2020-10-14 19:08 Barcelona
# Description: Imports libraries and returns the reference to the object
#

import importlib


class ImportUtils:

    o_fileutils = None
    o_stringutils = None

    def __init__(self, o_fileutils, o_stringutils):
        self.o_fileutils = o_fileutils
        self.o_stringutils = o_stringutils

    def import_module(self, s_path_to_lib_file):
        """
        Path separated with .
        :type s_path_to_lib_file: String
        :return: boolean, o_library
        """

        b_success = True
        o_module = None

        try:
            o_module = importlib.import_module(s_path_to_lib_file)
        except:
            b_success = False

        return b_success, o_module

    def get_method_handler(self, o_class, s_method):
        method_to_call = getattr(o_class, s_method)

        return method_to_call

    def import_plugin(self, s_module_name, s_folder="plugins", s_cfg_filename="plugin.cfg"):
        """"
        s_module_name has to contain the path, like "plugins.rsync." + s_module_name
        """

        b_success = False
        b_success_cfg = False
        b_enabled = False
        d_s_config_values = {}

        s_plugin_path = s_folder + "." + s_module_name + "."
        s_module_path = s_plugin_path + s_module_name
        s_module_path_fs = s_plugin_path.replace(".", "/")
        s_module_path_fs_file = s_module_path_fs + s_cfg_filename

        b_success_cfg, d_s_config_values = self.o_fileutils.read_config_file_values(s_module_path_fs_file)
        if b_success_cfg is True:
            b_exists, s_value = self.o_stringutils.get_dict_value(s_key='Enabled', d_dict=d_s_config_values, m_default="0")
            if s_value == '1':
                b_enabled = True
                b_success, o_module = self.import_module(s_path_to_lib_file=s_module_path)
                if b_success is False:
                    print("Cannot import module: " + s_module_path)
            else:
                # Plugin is Disabled
                pass
        else:
            # Config file Load failed
            print("Failed to load Config file")
            return False, False, None, "", d_s_config_values

        b_exists, s_main_class = self.o_stringutils.get_dict_value(s_key='MainClass', d_dict=d_s_config_values, m_default="")
        if b_exists is False or s_main_class == "":
            return False, b_enabled, None, "", d_s_config_values

        # Load the Default action for that plugin
        b_exists_default_action, s_default_action = self.o_stringutils.get_dict_value(s_key='DefaultAction',
                                                                                      d_dict=d_s_config_values,
                                                                                      m_default="")

        if b_exists is False or s_main_class == "" or b_exists_default_action is False or s_default_action == "":
            return False, b_enabled, None, "", d_s_config_values

        if b_success_cfg is False or b_success is False:
            return False, b_enabled, None, "", d_s_config_values

        # print(o_rsync_module)
        # class_ = getattr(o_rsync_module, class_name)
        # s_class_name = s_module_name.title()
        o_plugin = getattr(o_module, s_main_class)
        # o_rsync_object = o_rsync(None, None, None)
        # print(o_rsync_object)

        # return True, b_enabled, o_rsync_object
        return True, b_enabled, o_plugin, s_default_action, d_s_config_values
