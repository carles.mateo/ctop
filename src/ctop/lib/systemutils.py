#
# Class to work with Typical System Operations, like exiting the App with an error code
#
# Author: Carles Mateo
# Creation Date: 2019-11-20 17:02 IST
#

import time
from lib.screenutils import ScreenUtils
from lib.datetimeutils import DateTimeUtils


class SystemUtils:

    o_screenutils = None
    o_datetimeutils = None

    def __init__(self, o_screenutils=ScreenUtils(), o_datetimeutils=DateTimeUtils()):
        self.o_screenutils = o_screenutils
        self.o_datetimeutils = o_datetimeutils

    def exit_ctop(self, s_text="", i_exit_code=0):
        """
        A main Exit Method, this will allow us to send error codes and to log to an external file if we want
        :param s_text:
        :param i_exit_code:
        :return:
        """
        if i_exit_code > 0:
            s_text = self.o_datetimeutils.get_datetime() + " " + s_text
            self.o_screenutils.perror(s_text)
        else:
            if s_text != "":
                s_text = self.o_datetimeutils.get_datetime() + " " + s_text
                self.o_screenutils.p(s_text)

        # Exit with the Exist Code passed, 0 for success, other for error
        exit(i_exit_code)

    def sleep(self, i_seconds=1):
        """
        Sleep the number of seconds indicated
        :param i_seconds:
        :return:
        """
        time.sleep(i_seconds)
