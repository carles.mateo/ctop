#
# Processes Utils
#
# Author: Carles Mateo
# Creation Date: 2019-11-20 18:18 IST
# Description: Class to deal with Processes
#

from lib.fileutils import FileUtils
from do.processdo import ProcessDO


class ProcessesUtils:

    EXIT_CODE_NO_ERRORS = 0
    EXIT_CODE_ERROR_NO_ROOT = 1
    EXIT_CODE_ERROR_NO_LINUX = 3
    EXIT_CODE_ERROR_PROCESSES = 11
    EXIT_CODE_ERROR_UNKNOWN = 100

    def __init__(self, o_fileutils=FileUtils()):
        self.o_fileutils = o_fileutils

        # Dictionary that will hold the data under keys "${PROCESSNAME}-${PID}"
        self.d_processes = {}
        # Dictionary that keeps the count of processes are seen on the system
        self.d_s_o_processes_repeated = {}
        # This will have numeric index
        self.d_is_processes_repeated_index_seen_times = {}

        self.a_processes_sorted_by_max_seen_times = []

        self.i_processes_running = 0
        self.i_fds = 0

        self.d_s_o_processes_repeated = {}
        self.d_is_processes_repeated_index_seen_times = {}
        self.a_processes_sorted_by_max_seen_times = []

        # Max values found
        self.s_higher_pid = "1"
        self.i_length_max_pid = 3

    def calc_processes_running(self):
        b_success, a_s_proc_stat = self.o_fileutils.read_file_as_lines("/proc/stat")
        if b_success is True:
            b_success = False
            for s_line in a_s_proc_stat:
                a_s_values = s_line.split(" ")
                if len(a_s_values) > 1:
                    if a_s_values[0] == "procs_running":
                        b_success = True
                        self.i_processes_running = int(a_s_values[1])

        return b_success

    def get_array_of_processes(self):
        """
        Returns an array with the processes by querying /proc/
        :return:
        """
        a_pids_sanitized = []

        # Will get a list with all the pid directories from /proc/
        b_success, a_pids = self.o_fileutils.get_all_files_or_directories_with_mask("/proc/[0-9]*/")

        if b_success is True:
            for s_pid in a_pids:
                s_pid = s_pid.replace("/proc/", "")
                # Remove the trailing / from the directory
                s_pid = s_pid.replace("/", "")

                try:
                    i_pid = int(s_pid)
                    # Store the PID, now sanitized, as Integer
                    a_pids_sanitized.append(s_pid)

                except:
                    # Something went unexpected in the conversion. Probably there is something not expected
                    # under /proc/ starting with a number and other characters, but is not a valid PID
                    b_success = False
                    break

        return b_success, a_pids_sanitized

    def get_process_info_as_string(self, s_pid):
        """
        Read process info from /proc/${PID}/stat File
        :param s_pid:
        :return: Boolean True for success, info as string
        """
        b_success = False
        s_process_info = ""

        b_success, s_process_info = self.o_fileutils.read_file_as_string("/proc/" + s_pid + "/stat")

        return b_success, s_process_info

    def get_kernel_fds_in_use(self):
        """
        Gets the number of File Descriptors that are in use now.
        The info /proc/sys/fs/file-nr can be read as regular user.
        :return: Boolean True for success read, s_max_fds as String
        """

        b_success_fds, s_fds = self.o_fileutils.read_file_as_string("/proc/sys/fs/file-nr")
        # Remove possible chr(0)
        s_fds = s_fds.strip()
        # Break into 3 positions array
        a_fds = s_fds.split()

        i_opened_fds_since_start = int(a_fds[0])
        i_released_fds = int(a_fds[1])
        i_open_fds_in_use = i_opened_fds_since_start - i_released_fds

        return b_success_fds, i_open_fds_in_use, i_opened_fds_since_start, i_released_fds

    def get_kernel_max_fds(self):
        """
        Read the max number of File Descriptors
        :return: Boolean True for success read, s_max_fds as String
        """
        b_success_max_fd, s_max_fds = self.o_fileutils.read_file_as_string("/proc/sys/fs/file-max")

        s_max_fds = s_max_fds.strip()

        return b_success_max_fd, s_max_fds

    def get_process_fds(self, s_pid):
        """
        Read File Descriptors /proc/${PID]/fd/
        man 5 proc
        http://man7.org/linux/man-pages/man5/proc.5.html
        :para, s_pid: the PID of the process as String
        :return: a_fds
        """
        a_fds = []

        b_success_fd, a_fds = self.o_fileutils.get_all_files_or_directories_with_mask("/proc/" + s_pid + "/fd/*")

        return b_success_fd, a_fds

    def get_process_io_as_string(self, s_pid):
        """
        Read process io information from /proc/${PID}/io File
        :param s_pid:
        :return: Boolean True for success, info as string
        """
        b_success = False
        s_process_info = ""

        b_success, s_process_info = self.o_fileutils.read_file_as_string("/proc/" + s_pid + "/io")

        return b_success, s_process_info

    def get_process_cmdline_as_string(self, s_pid):
        """
        Read process info from /proc/${PID}/stat File
        :param s_pid:
        :return: Boolean True for success, info as string
        """
        b_success = False
        s_process_info = ""

        b_success, s_process_info = self.o_fileutils.read_file_as_string("/proc/" + s_pid + "/cmdline")
        s_process_info = s_process_info.replace(chr(0), ' ')

        return b_success, s_process_info

    def get_process_name_from_stat_file_string(self, s_stat_string):
        """
        Returns the process name from /proc/PID/stat which is between parenthesis
        Note: We will return the parenthesis too
        @param s_stat_string:
        @return: boolean, string
        """
        i_ini_pos = s_stat_string.find('(')
        i_end_pos = s_stat_string.rfind(')')
        # It's annoying but there are processes stored in /proc/PID/stat like:
        # ((sd-pam))
        # And if you execute ps -ax you get (sd-pam) as name of process.

        if i_ini_pos > -1 and i_end_pos > -1:
            s_process_name = s_stat_string[i_ini_pos:i_end_pos + 1]
            return True, s_process_name

        return False, ""

    def get_process_info(self, s_pid):
        b_success, s_process_info = self.get_process_info_as_string(s_pid)
        b_success_cmdline, s_cmdline = self.get_process_cmdline_as_string(s_pid)
        b_success_io, s_io = self.get_process_io_as_string(s_pid)

        if b_success is True:
            #a_process_properties = s_process_info.split()
            # @TODO: This is not correct
            # [UVM Tools Event] will make it be displayed as (UVM
            #s_process_name = a_process_properties[1]
            # Sanitize name, basically remove parenthesis
            #s_process_name = s_process_name.replace('(', '').replace(')', '')
            # Note, we will not sanitize. It makes sense to know when we don't get the full cmdline
            b_success_process_name, s_process_name = self.get_process_name_from_stat_file_string(s_process_info)
            if b_success_process_name is False:
                # Seems like this Linux doen't use the format (name) in /proc/PID/stat
                # Hopefully uses the same format as second column containing binary name
                a_process_properties = s_process_info.split()
                s_process_name = a_process_properties[1]
            # @TODO get more values

            # @TODO: Constructor will be updated when I get more values
            o_processdo = ProcessDO(s_pid, s_process_name)
            if b_success_cmdline is True and len(s_cmdline) > 1:
                o_processdo.set_cmdline(s_cmdline)
            else:
                o_processdo.set_cmdline(s_process_name)

            if b_success_io is True:
                o_processdo.set_io_from_string(s_io)

            # Get the File Descriptors in use
            b_success_fds, a_fds = self.get_process_fds(s_pid)
            if b_success_fds is True:
                o_processdo.set_fds(a_fds)

        else:
            o_processdo = None

        return b_success, o_processdo

    def update_list_of_processes(self):
        """
        Will update the information of Processes.
        Note: The normal way would be to use a library like psutil.
              However I think that the objective of the exercise is to show that I know how Linux works.
              I raised this question to Human Resources but they said that questions are not allowed for the code test.
        :return: boolean True on success or False on failure
        """

        # First, reset the dictionary holding the Data
        self.d_processes = {}

        # Set to the minimum
        self.i_length_max_pid = 3
        self.s_higher_pid = "1"

        l_i_fds = 0

        b_success, a_pids = self.get_array_of_processes()

        if b_success is False:
            return b_success

        # Now we have to get the information of each process
        # We have to check them all, to see if one died.
        # We can check the timestamp as well to see when it was created
        for s_pid in a_pids:
            b_success, o_processdo = self.get_process_info(s_pid)

            if b_success is True:
                # Reserve the space so the table is not broken. Bug #36
                if s_pid > self.s_higher_pid:
                    self.s_higher_pid = s_pid
                if len(s_pid) > self.i_length_max_pid:
                    self.i_length_max_pid = len(s_pid)

                # Update counter of fds
                l_i_fds = l_i_fds + o_processdo.get_count_fds()
                s_key_pid = o_processdo.s_name + "-" + s_pid
                if s_key_pid in self.d_processes:
                    # The process already existed in the previous iteration
                    # @TODO: Update the Average stats for CPU and Memory
                    o_processdo.increase_seen_times()
                    self.d_processes[s_key_pid] = o_processdo
                else:
                    self.d_processes[s_key_pid] = o_processdo
            else:
                # There was an error with at least one process, something totally unexpected but things happen
                # Also could happen that the process ended and is not available in /proc/${PID}
                # We don't want this to get silent, so will report the error in the main body
                # return False
                # We will ignore the process from the watch
                continue

        b_success = self.calc_processes_running()

        # Update Total Process FD's number count
        # Note: 2021-05-22 Carles Mateo I change this method from reading from the Kernel
        # The results are more accurate and show x10 times more fds in use.
        # self.i_fds = l_i_fds
        b_success_fds, self.i_fds, i_opened_fds_since_start, i_released_fds = self.get_kernel_fds_in_use()
        if b_success_fds is False:
            self.i_fds = -1

        return b_success

    def render_processes_as_string(self, i_max_processes=0):
        """
        This method will render in a string the list of processes, so it will be able to be printed from the main body.
        It will use the previously computed in update_list_of_processes()
        Please note: This should be in a TemplateUtils class, but I'm running out of time and I want to show something
        Apologies for that. :(
        I would love to invest a bit more time, but you asked 5 hours and rules are rules.
        Many more optimizations would go here, like using an array of Strings instead of generating a new String every
        time by appending, but unfortunately I don't have more time and I need to show at least a basic output.
        :return: String
        """

        s_output = ""

        i_counter_processes = 0
        for s_key in self.d_processes:
            i_counter_processes += 1
            o_processdo = self.d_processes[s_key]
            s_output = s_output + "Process Name: " + o_processdo.s_name + " PID: " + o_processdo.s_pid + "\n"
            if i_counter_processes == i_max_processes:
                break

        return s_output

    def render_processes_as_dictionary(self, i_max_processes=0, i_page_processes=0, i_processes_per_page=10, i_max_len=20):
        """
        This method will render in a string the list of processes, so it will be able to be printed from the main body.
        It will use the previously computed in update_list_of_processes()
        Please note: This should be in a TemplateUtils class, but I'm running out of time and I want to show something
        Apologies for that. :(
        I would love to invest a bit more time, but you asked 5 hours and rules are rules.
        Many more optimizations would go here, like using an array of Strings instead of generating a new String every
        time by appending, but unfortunately I don't have more time and I need to show at least a basic output.
        :return: Dictionary of String
        """

        d_s_s_output = {}
        i_page_current = 0

        i_total_pages = int(len(self.d_processes) / i_processes_per_page)
        if i_page_processes > i_total_pages:
            i_page_processes = 0

        i_counter_processes = 0
        i_counter_loop = 0
        for s_key in self.d_processes:

            i_page_current = int(i_counter_loop / i_processes_per_page)
            i_counter_loop = i_counter_loop + 1

            if i_page_current == i_page_processes:
                o_processdo = self.d_processes[s_key]
                #d_s_s_output[i_counter_processes] =  o_processdo.s_pid.rjust(5, ' ') + " " + o_processdo.get_name()
                d_s_s_output[i_counter_processes] = o_processdo.s_pid.rjust(self.i_length_max_pid, ' ') + " " + o_processdo.get_cmdline()[0:i_max_len]

                i_counter_processes += 1
                if i_counter_processes == i_max_processes:
                    break

        return d_s_s_output

    def get_num_current_processes_in_dict(self):
        """
        Returns the number of processes accounted
        :return:
        """
        return len(self.d_processes)

    def get_num_processes_running(self):
        return self.i_processes_running

    def update_repeated_processes_count(self):
        self.d_s_o_processes_repeated = {}
        self.d_is_processes_repeated_index_seen_times = {}
        self.a_processes_sorted_by_max_seen_times = []

        for s_key in self.d_processes:
            o_process = self.d_processes[s_key]
            s_process_name = o_process.get_name()
            if s_process_name in self.d_s_o_processes_repeated:
                self.d_s_o_processes_repeated[s_process_name].increase_seen_times()
            else:
                o_process_repeated = ProcessDO(s_pid="", s_name=s_process_name)
                self.d_s_o_processes_repeated[s_process_name] = o_process_repeated

        a_processes_list = list(self.d_s_o_processes_repeated.values())
        self.a_processes_sorted_by_max_seen_times = sorted(a_processes_list, key=lambda x: x.get_seen_times(), reverse=True)

    def get_processes_sorted_by_max_seen_times(self):
        return self.a_processes_sorted_by_max_seen_times
