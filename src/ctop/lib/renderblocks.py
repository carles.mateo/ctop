#
# Renderblocks Utils
#
# Author: Carles Mateo
# Creation Date: 2014 in Barcelona
# Last Modification: 2021-03-06 21:25 Ireland. Removed UTF-8 characters.
# Description: Class to deal with rendering of virtual screen and colors
#


from lib.screenutils import ScreenUtils
from lib.cpuutils import CpuUtils
from do.stringcolordo import StringColorDO
from do.networkinterfacedo import NetworkInterfaceDO


class RenderBlocks:

    i_display_max_len_process_name = 50

    # /proc/PID/stat cuts to 15 + 2 for the parenthesis
    i_display_max_len_in_repeated_binary_name = 17

    i_block_hostname_y_position = 1
    i_block_cpus_y_position = 2
    i_block_cpus_use_y_position = 3
    i_block_mem_y_position = 4
    i_block_processes_title_y_position = 5
    i_block_errors_log_nfs_timeout_title_y_position = 7
    i_block_errors_log_out_of_memory_title_y_position = 9
    i_block_errors_log_rabbitmq_title_y_position = 10
    i_block_processes_processes_y_position = 12
    i_block_network_y_position = 5
    # This indicates where the block finish
    # NOTE: At this point probably will make sense to have an Object Block
    i_block_network_y_position_last = 5

    i_block_listening_ports_y_position = 3

    i_block_free_space_y_position_last = i_block_network_y_position_last + 2

    # Aligned Right
    i_block_uptime_y_position = 1

    def __init__(self, o_screenutils, o_datetimeutils, o_appmessages, o_processesutils, o_cpuutils,
                 o_memutils, o_networkutils=None, o_osutils=None, o_kernelutils=None, o_logerrorscheckerutils=None,
                 o_stringutils=None):
        self.o_screenutils = o_screenutils
        self.o_stringutils = o_stringutils
        self.o_datetimeutils = o_datetimeutils
        self.o_appmessages = o_appmessages
        self.o_processesutils = o_processesutils
        self.o_cpuutils = o_cpuutils
        self.o_memutils = o_memutils
        self.o_networkutils = o_networkutils
        self.o_osutils = o_osutils
        self.o_kernelutils = o_kernelutils
        self.o_logerrorscheckerutils = o_logerrorscheckerutils

        # Variables for the Host line (second), to show the Kernel option according to the Terminal width
        self.i_width_block_uid_and_block_uptime = 0
        self.i_width_block_hostname = 0
        self.i_width_block_distribution = 0

        # For positioning Listening ports block
        self.i_width_cpu_and_cloud = 15

    def render_virtual_terminal(self, i_page_processes=0):
        self.render_block_header()

        # Right
        self.render_block_uptime()

        self.render_block_uid()
        self.render_block_hostinfo()
        self.render_block_cpuinfo()
        self.render_block_listening_ports()
        self.render_block_network()
        self.render_block_meminfo()
        self.render_block_processes(i_page_processes)
        self.render_block_error_logs_nfs_timedout()
        self.render_block_error_logs_memory_read()
        self.render_block_error_logs_out_of_memory()
        self.render_block_error_rabbitmq()

        # Right. This should be processed after rendering block_network
        self.render_block_free_space()
        # This should be processed after render_block_free_space()
        self.render_block_repeated_processes()

        self.render_block_bottom()

    def render_block_header(self):
        s_time_and_epoch = self.o_datetimeutils.get_datetime() + " " + self.o_datetimeutils.get_unix_epoch()
        # Project Version
        # self.o_screenutils.vprint_in_x_y(0, 0, self.get_version_name())
        self.o_screenutils.vprint_in_x_y_stringcolor(0,
                                                     0,
                                                     self.o_appmessages.get_stringcolor("version_and_code_and_python_version_with_label"))
        # Local time on top right
        self.o_screenutils.vprint_aligned_to_right_y(i_x_margin=0, i_y=0, s_text=s_time_and_epoch)

    def render_block_uptime(self):
        """
        Renders the Virtual Screen for the block Uptime, aligned to the right.
        This block is absolute to the right.
        """

        i_y = self.i_block_uptime_y_position

        b_success, s_uptime = self.o_osutils.get_uptime()

        s_line_uptime_bw = "Uptime:" + " "
        s_line_uptime_color = self.o_screenutils.color_label("Uptime:") + " "
        if b_success is True:
            s_line_uptime_bw = s_line_uptime_bw + s_uptime
            s_line_uptime_color = s_line_uptime_color + s_uptime
        else:
            s_line_uptime_bw = s_line_uptime_bw + "Error reading /proc/uptime"
            s_line_uptime_color = s_line_uptime_color + self.o_screenutils.color_alert("Error reading /proc/uptime")

        o_line_uptime_stringcolor = StringColorDO(s_line_uptime_bw, s_line_uptime_color)

        i_x = self.o_screenutils.vcalc_x_aligned_to_right_y_with_virtual_line_y(i_x_margin=0,
                                                                                i_y=i_y,
                                                                                s_text=s_line_uptime_bw)

        self.o_screenutils.vprint_in_x_y_stringcolor(i_x=i_x, i_y=i_y, o_stringcolor=o_line_uptime_stringcolor)

    def render_block_uid(self):
        """
        Renders the block User Id to be to the left of Uptime.
        This block is relative.
        :return:
        """
        i_terminal_width = self.o_screenutils.get_terminal_width_to_use()

        i_y = self.i_block_uptime_y_position

        s_line_userid_bw, s_line_userid_color = self.get_string_for_block_uid()

        o_line_userid_stringcolor = StringColorDO(s_line_userid_bw, s_line_userid_color)

        i_x = self.o_screenutils.vcalc_x_aligned_to_right_y_with_virtual_line_y(i_x_margin=0,
                                                                                i_y=i_y,
                                                                                s_text=s_line_userid_bw)

        # Space for render_block_uptime
        i_x = i_x - 28

        self.i_width_block_uid_and_block_uptime = len(s_line_userid_bw) + 28

        self.o_screenutils.vprint_in_x_y_stringcolor(i_x=i_x, i_y=i_y, o_stringcolor=o_line_userid_stringcolor)

    def get_string_for_block_uid(self):
        """
        Get string
        :return:
        """

        i_userid = self.o_osutils.get_euid()

        s_userid = str(i_userid)
        b_success_username, s_username = self.o_osutils.get_username()

        s_line_userid_bw = "User Id:" + " "
        s_line_userid_color = self.o_screenutils.color_label(s_line_userid_bw.rstrip()) + " "

        s_line_userid_bw = s_line_userid_bw + s_userid + " " + s_username
        s_line_userid_color = s_line_userid_color + s_userid + " " + s_username

        return s_line_userid_bw, s_line_userid_color

    def get_width_block_uid_and_block_uptime(self):
        return self.i_width_block_uid_and_block_uptime

    def get_width_cpu_and_cloud(self):
        return self.i_width_cpu_and_cloud

    def get_max_width_for_platform(self):
        """
        Returns the maximum width for the data about the Platform, Distribution...
        :return:
        """
        i_terminal_width = self.o_screenutils.get_terminal_width_to_use()

        i_terminal_width_available = i_terminal_width - self.get_width_block_uid_and_block_uptime()

        # Space
        i_terminal_width_available = i_terminal_width_available - 1
        i_terminal_width_available = i_terminal_width_available - self.i_width_block_hostname
        i_terminal_width_available = i_terminal_width_available - 1

        i_terminal_width_available = i_terminal_width_available - self.i_width_block_distribution
        i_terminal_width_available = i_terminal_width_available - 1

        return i_terminal_width_available

    def get_string_for_block_hostinfo_hostname(self):
        s_hostname = self.o_osutils.get_platform_hostname()
        s_hostline_bw = "hostname: " + s_hostname
        s_hostline_color = self.o_screenutils.color_label("hostname:") + " " + s_hostname

        self.i_width_block_hostname = len(s_hostline_bw)

        return s_hostline_bw, s_hostline_color

    def get_string_for_distribution_description(self):
        s_distribution_description = self.o_osutils.get_distribution_description()

        if len(s_distribution_description) > 2:
            # Something more than ""
            s_distribution_description_bw = "Distro:" + " " + s_distribution_description
            s_distribution_description_color = self.o_screenutils.color_label("Distro:") + " " + s_distribution_description
        else:
            s_distribution_description_bw = ""
            s_distribution_description_color = ""

        self.i_width_block_distribution = len(s_distribution_description_bw)

        return s_distribution_description_bw, s_distribution_description_color

    def render_block_hostinfo(self):
        """
        Renders:
        hostname:                                                    User Id: 1000 carles   Uptime: 504631
        CPUs:
        Use:               Unable to decode DMI
        :return:
        """

        s_platform = self.o_osutils.get_platform_system()
        s_machine = self.o_osutils.get_platform_machine()
        s_release = self.o_osutils.get_platform_release()

        s_hostline_bw, s_hostline_color = self.get_string_for_block_hostinfo_hostname()
        s_distribution_description_bw, s_distribution_description_color = self.get_string_for_distribution_description()

        i_max_width_for_platform = self.get_max_width_for_platform()

        s_hostinfo_line_bw = s_hostline_bw + " " + s_distribution_description_bw
        s_hostinfo_line_color = s_hostline_color + " " + s_distribution_description_color

        s_platform_bw = "Platform:" + " " + s_platform
        s_platform_color = self.o_screenutils.color_label("Platform:") + " " + s_platform

        if len(s_platform_bw) < i_max_width_for_platform:
            s_hostinfo_line_bw = s_hostinfo_line_bw + " " + s_platform_bw
            s_hostinfo_line_color = s_hostinfo_line_color + " " + s_platform_color
            i_max_width_for_platform = i_max_width_for_platform - len(s_platform_bw) - 1

        s_machine_bw = s_machine
        s_machine_color = self.o_screenutils.color_additional_info(s_machine)

        if len(s_release) < i_max_width_for_platform:
            s_hostinfo_line_bw = s_hostinfo_line_bw + " " + s_release
            s_hostinfo_line_color = s_hostinfo_line_color + " " + s_release
            i_max_width_for_platform = i_max_width_for_platform - len(s_release) - 1

        if len(s_machine_bw) < i_max_width_for_platform:
            s_hostinfo_line_bw = s_hostinfo_line_bw + " " + s_machine_bw
            s_hostinfo_line_color = s_hostinfo_line_color + " " + s_machine_color
            i_max_width_for_platform = i_max_width_for_platform - len(s_machine_bw) - 1

        o_hostinfo_line_stringcolor = StringColorDO(s_hostinfo_line_bw, s_hostinfo_line_color)
        self.o_screenutils.vprint_in_x_y_stringcolor(0, self.i_block_hostname_y_position, o_hostinfo_line_stringcolor)

        # Next line

        b_in_amazon = self.o_osutils.get_is_in_amazon()
        b_in_google = self.o_osutils.get_is_in_google()
        b_in_docker = self.o_osutils.get_is_in_docker()
        b_in_lxc = self.o_osutils.get_is_in_lxc()
        b_in_vbox = self.o_osutils.get_is_in_virtualbox()
        b_in_openstack = self.o_osutils.get_is_in_openstack()
        b_error_accessing_dmi = self.o_osutils.get_error_accessing_dmi()

        s_in_amazon_bw = ""
        s_in_amazon_color = ""
        s_in_google_bw = ""
        s_in_google_color = ""
        s_in_docker_bw = ""
        s_in_docker_color = ""
        s_in_lxc_bw = ""
        s_in_lxc_color = ""
        s_in_openstack_bw = ""
        s_in_openstack_color = ""
        s_in_vbox_bw = ""
        s_in_vbox_color = ""

        b_render = False

        if b_error_accessing_dmi is True:
            # Normal reasons:
            # - Not logged as root
            # - Linux distro does not has this file
            # - Hardware does not support it (Raspberry Pi)

            if self.o_cpuutils.get_cpu_family() == CpuUtils.s_CPU_INTEL or self.o_cpuutils.get_cpu_family() == CpuUtils.s_CPU_AMD:
                s_log_as_root_bw = " Unable to decode DMI"
                s_log_as_root_color = " " + self.o_screenutils.color_warning(s_log_as_root_bw.lstrip())

                o_in_virtualized_stringcolor = StringColorDO(s_log_as_root_bw, s_log_as_root_color)
                self.o_screenutils.vprint_in_x_y_stringcolor(15, self.i_block_cpus_use_y_position,
                                                             o_in_virtualized_stringcolor)

                self.i_width_cpu_and_cloud = 15 + len(" Unable to decode DMI") + 1

            # Leave now
            return

        if b_in_amazon is True:
            s_in_amazon_bw = " " + "Running in Amazon"
            s_in_amazon_color = " " + self.o_screenutils.color_warning("Running in Amazon")
            b_render = True

        if b_in_google is True:
            s_in_google_bw = " " + "Running in Google"
            s_in_google_color = " " + self.o_screenutils.color_warning("Running in Google")
            b_render = True

        if b_in_docker is True:
            s_in_docker_bw = " " + "Running in Docker"
            s_in_docker_color = " " + self.o_screenutils.color_warning("Running in Docker")
            b_render = True

        if b_in_lxc is True:
            s_in_lxc_bw = " " + "Running in LXC"
            s_in_lxc_color = " " + self.o_screenutils.color_warning("Running in LXC")
            b_render = True

        if b_in_openstack is True:
            s_in_openstack_bw = " " + "Running in OpenStack"
            s_in_openstack_color = " " + self.o_screenutils.color_warning(s_in_openstack_bw.lstrip())
            b_render = True

        if b_in_vbox is True:
            s_in_vbox_bw = " " + "Running in VBOX"
            s_in_vbox_color = " " + self.o_screenutils.color_warning("Running in VBOX")
            b_render = True

        if b_render is True:
            s_in_virtualized_bw = s_in_amazon_bw + s_in_google_bw + s_in_docker_bw + s_in_lxc_bw + s_in_openstack_bw + s_in_vbox_bw
            s_in_virtualized_color = s_in_amazon_color + s_in_google_color + s_in_docker_color + s_in_lxc_color + s_in_openstack_color + s_in_vbox_color

            o_in_virtualized_stringcolor = StringColorDO(s_in_virtualized_bw, s_in_virtualized_color)
            self.o_screenutils.vprint_in_x_y_stringcolor(15, self.i_block_cpus_use_y_position,
                                                         o_in_virtualized_stringcolor)

            self.i_width_cpu_and_cloud = 15 + len(s_in_virtualized_bw) + 1

    def render_block_cpuinfo(self):
        i_processor_count = self.o_cpuutils.get_processor_count()
        s_processor_count = str(i_processor_count)
        s_processor_name = self.o_cpuutils.get_processor_name()

        i_core_count = self.o_cpuutils.get_core_count()
        s_core_count = str(i_core_count)
        f_cpu_use, s_cpu_use = self.o_cpuutils.get_cpu_use()
        if f_cpu_use < 50:
            s_cpu_use_color = self.o_screenutils.color_ok(s_cpu_use)
        elif f_cpu_use < 69:
            s_cpu_use_color = self.o_screenutils.color_warning(s_cpu_use)
        else:
            # High usage of the CPU
            s_cpu_use_color = self.o_screenutils.color_alert(s_cpu_use)

        # @TODO: Consider if this should be an AppMessages responsibility
        s_cpu_line_color = self.o_screenutils.color_label("CPUs:") + \
                           " " + s_processor_count + \
                           " x " + \
                           self.o_screenutils.color_string(self.o_screenutils.BLUE, s_processor_name) + \
                           " " + self.o_screenutils.color_label("Total Cores:") + " " + s_core_count
        s_cpu_line_bw = "CPUs:" + \
                        " " + s_processor_count + \
                        " x " + \
                        s_processor_name + \
                        " " + "Total Cores:" + " " + s_core_count

        o_cpu_stringcolor = StringColorDO(s_cpu_line_bw, s_cpu_line_color)

        s_cpu_use_line_color = self.o_screenutils.color_label("Use:") + " " + s_cpu_use_color
        s_cpu_use_line_bw = "Use:" + " " + s_cpu_use
        o_cpu_use_stringcolor = StringColorDO(s_cpu_use_line_bw, s_cpu_use_line_color)

        #self.o_screenutils.vprint_in_x_y(0, 1, s_cpu_line)
        self.o_screenutils.vprint_in_x_y_stringcolor(0, self.i_block_cpus_y_position, o_cpu_stringcolor)
        self.o_screenutils.vprint_in_x_y_stringcolor(0, self.i_block_cpus_use_y_position, o_cpu_use_stringcolor)

    def render_block_error_logs_nfs_timedout(self):
        i_y = self.i_block_errors_log_nfs_timeout_title_y_position
        i_x = 0

        # Write title
        s_title_error_logs = "Errors in logs"
        s_title_error_logs_color = self.o_screenutils.color_label(s_title_error_logs)
        o_title_error_logs_stringcolor = StringColorDO(s_title_error_logs,
                                                       s_title_error_logs_color)
        self.o_screenutils.vprint_in_x_y_stringcolor(i_x=i_x, i_y=i_y-1,
                                                     o_stringcolor=o_title_error_logs_stringcolor)

        i_errors_nfs_timeout = self.o_logerrorscheckerutils.get_errors_nfs_timed_out()
        s_title_error_logs_nfs_timeout = "- NFS Timed out :"

        s_errors_nfs_timeout_bw = str(i_errors_nfs_timeout)
        if i_errors_nfs_timeout > 0:
            s_errors_nfs_timeout_color = self.o_screenutils.color_alert(s_errors_nfs_timeout_bw)
        else:
            s_errors_nfs_timeout_color = s_errors_nfs_timeout_bw

        s_title_error_logs_nfs_timeout_color = self.o_screenutils.color_label(s_title_error_logs_nfs_timeout) + \
                                               " " + s_errors_nfs_timeout_color
        s_title_error_logs_nfs_timeout = s_title_error_logs_nfs_timeout + " " + s_errors_nfs_timeout_bw

        o_title_error_logs_nfs_timeout_stringcolor = StringColorDO(s_title_error_logs_nfs_timeout,
                                                                   s_title_error_logs_nfs_timeout_color)
        self.o_screenutils.vprint_in_x_y_stringcolor(i_x=i_x, i_y=i_y, o_stringcolor=o_title_error_logs_nfs_timeout_stringcolor)

    def render_block_error_logs_memory_read(self):
        i_y = self.i_block_errors_log_nfs_timeout_title_y_position + 1
        i_x = 0

        i_errors_memory_read = self.o_logerrorscheckerutils.get_errors_memory_read()
        s_title_error_logs_memory_read = "- Memory read   :"

        s_errors_memory_read_bw = str(i_errors_memory_read)
        if i_errors_memory_read > 0:
            s_errors_memory_read_color = self.o_screenutils.color_alert(s_errors_memory_read_bw)
        else:
            s_errors_memory_read_color = s_errors_memory_read_bw

        s_title_error_logs_memory_read_color = self.o_screenutils.color_label(s_title_error_logs_memory_read) + \
                                               " " + s_errors_memory_read_color
        s_title_error_logs_memory_read = s_title_error_logs_memory_read + " " + s_errors_memory_read_bw

        o_title_error_logs_memory_read_stringcolor = StringColorDO(s_title_error_logs_memory_read,
                                                                   s_title_error_logs_memory_read_color)
        self.o_screenutils.vprint_in_x_y_stringcolor(i_x=i_x,
                                                     i_y=i_y,
                                                     o_stringcolor=o_title_error_logs_memory_read_stringcolor)

    def render_block_error_logs_out_of_memory(self):
        i_y = self.i_block_errors_log_out_of_memory_title_y_position
        i_x = 0

        i_errors_out_of_memory = self.o_logerrorscheckerutils.get_errors_out_of_memory()
        s_title_error_logs_out_of_memory = "- Out of Memory :"

        s_errors_out_of_memory = str(i_errors_out_of_memory)
        if i_errors_out_of_memory > 0:
            s_errors_out_of_memory_read_color = self.o_screenutils.color_alert(s_errors_out_of_memory)
        else:
            s_errors_out_of_memory_read_color = s_errors_out_of_memory

        s_title_error_logs_out_of_memory_color = self.o_screenutils.color_label(s_title_error_logs_out_of_memory) + \
                                                 " " + s_errors_out_of_memory_read_color
        s_title_error_logs_out_of_memory = s_title_error_logs_out_of_memory + " " + s_errors_out_of_memory

        o_title_error_logs_out_of_memory_stringcolor = StringColorDO(s_title_error_logs_out_of_memory,
                                                                     s_title_error_logs_out_of_memory_color)
        self.o_screenutils.vprint_in_x_y_stringcolor(i_x=i_x,
                                                     i_y=i_y,
                                                     o_stringcolor=o_title_error_logs_out_of_memory_stringcolor)

    def render_block_error_rabbitmq(self):
        i_y = self.i_block_errors_log_rabbitmq_title_y_position
        i_x = 0

        i_errors_rabbitmq = self.o_logerrorscheckerutils.get_errors_rabbitmq()
        s_title_error_rabbitmq = "- RabbitMQ      :"

        s_errors_rabbitmq = str(i_errors_rabbitmq)
        if i_errors_rabbitmq > 0:
            s_errors_rabbitmq_color = self.o_screenutils.color_alert(s_errors_rabbitmq)
        else:
            s_errors_rabbitmq_color = s_errors_rabbitmq

        s_title_error_rabbitmq_color = self.o_screenutils.color_label(s_title_error_rabbitmq) + \
                                                 " " + s_errors_rabbitmq_color
        s_title_error_rabbitmq = s_title_error_rabbitmq + " " + s_errors_rabbitmq

        o_title_error_rabbitmq_stringcolor = StringColorDO(s_title_error_rabbitmq,
                                                           s_title_error_rabbitmq_color)
        self.o_screenutils.vprint_in_x_y_stringcolor(i_x=i_x,
                                                     i_y=i_y,
                                                     o_stringcolor=o_title_error_rabbitmq_stringcolor)

    def get_max_width_for_ports(self):
        """
        Returns the maximum width available for listenning ports.
        :return:
        """

        i_terminal_width = self.o_screenutils.get_terminal_width_to_use()
        i_terminal_width_available = i_terminal_width - self.get_width_cpu_and_cloud()
        i_terminal_width_available = i_terminal_width_available - 1

        return i_terminal_width_available

    def render_block_listening_ports(self):
        i_y = self.i_block_listening_ports_y_position

        a_listening_ports = self.o_networkutils.get_listening_ports()

        s_title_listening_ports = "Listening ports:"

        s_line_port = ""
        for i_port in a_listening_ports:
            s_line_port = s_line_port + " " + str(i_port)

        i_width_available_ports = self.get_max_width_for_ports()

        s_listening_ports_bw = ""

        if len(s_title_listening_ports) < i_width_available_ports:
            s_listening_ports_bw = s_title_listening_ports
            i_width_available_ports = i_width_available_ports - len(s_title_listening_ports) - 1
        else:
            return

        if len(s_line_port) + 1 > i_width_available_ports:
            if i_width_available_ports > 2:
                s_line_port = s_line_port[0:i_width_available_ports - 2] + ".."
            else:
                s_line_port = ""

        s_listening_ports_bw = s_listening_ports_bw + " " + s_line_port

        i_x = self.o_screenutils.vcalc_x_aligned_to_right_y_with_virtual_line_y(i_x_margin=0,
                                                                                i_y=i_y,
                                                                                s_text=s_listening_ports_bw)

        s_title_listening_ports_color = self.o_screenutils.color_label(s_title_listening_ports)
        s_listening_ports_color = s_title_listening_ports_color + " " + s_line_port
        o_listening_ports_stringcolor = StringColorDO(s_listening_ports_bw, s_listening_ports_color)
        self.o_screenutils.vprint_in_x_y_stringcolor(i_x=i_x, i_y=i_y, o_stringcolor=o_listening_ports_stringcolor)

    def render_block_network(self):
        i_y = self.i_block_network_y_position
        b_success, d_s_o_network_devices = self.o_networkutils.get_all_network_inferfaces_as_dict_of_objects()

        i_terminal_width = self.o_screenutils.get_i_terminal_width()

        s_title_network_interfaces = 'Network Interface'
        s_title_tx_gib = "tx GiB"
        s_title_rx_gib = "rx GiB"
        s_title_tx_mib_sec = "tx MiB/sec"
        s_title_rx_mib_sec = "rx MiB/sec"
        s_title_collisions = 'Coll'
        s_title_tx_errors = 'TxErr'
        s_title_rx_errors = 'RxErr'

        if b_success is True:
            #s_title_network_iface_padded = "Network Interfaces".ljust(20, ' ')
            s_title_network_iface_bw = s_title_network_interfaces + " " + "State" + " " + "Speed".rjust(7, ' ') + \
                                       " " + "Main Ip".rjust(15, ' ')

            if i_terminal_width > 80:
                s_title_network_iface_bw = s_title_network_iface_bw + \
                                           " |  " + s_title_tx_gib + " |  " + s_title_rx_gib

            if i_terminal_width > 105:
                s_title_network_iface_bw = s_title_network_iface_bw + \
                                           " " + s_title_tx_mib_sec + " " + s_title_rx_mib_sec

            if i_terminal_width > 115:
                s_title_network_iface_bw = s_title_network_iface_bw + \
                                           " " + s_title_collisions + " " + s_title_tx_errors + " " + s_title_rx_errors

            i_x = self.o_screenutils.vcalc_x_aligned_to_right_y_with_virtual_line_y(i_x_margin=0,
                                                                                    i_y=i_y,
                                                                                    s_text=s_title_network_iface_bw)
            #self.o_screenutils.vprint_aligned_to_right_y(i_x_margin=0, i_y=i_y, s_text=s_title_network_iface_padded)
            s_title_network_iface_color = self.o_screenutils.color_label(s_title_network_iface_bw)
            o_title_network_iface_stringcolor = StringColorDO(s_title_network_iface_bw, s_title_network_iface_color)
            self.o_screenutils.vprint_in_x_y_stringcolor(i_x=i_x, i_y=i_y, o_stringcolor=o_title_network_iface_stringcolor)

            i_y = i_y + 1
            for s_network_iface in d_s_o_network_devices:
                o_network_iface = d_s_o_network_devices[s_network_iface]
                s_network_iface_padded = s_network_iface.rjust(len(s_title_network_interfaces), ' ')

                s_speed = o_network_iface.get_speed()
                if s_speed == "-1":
                    s_speed = ""

                s_main_ip = o_network_iface.get_main_ip()
                s_operstate = o_network_iface.get_operstate()

                i_collisions = o_network_iface.get_collisions()
                i_rx_total_errors = o_network_iface.get_total_rx_errors()
                i_tx_total_errors = o_network_iface.get_total_tx_errors()

                s_rx_data_gib_2decimals = o_network_iface.get_rx_data_gib_2decimals()
                s_tx_data_gib_2decimals = o_network_iface.get_tx_data_gib_2decimals()

                #i_avg_tx_bytes_second = o_network_iface.get_avg_tx_bytes()
                i_avg_tx_bytes_second = o_network_iface.get_tx_bytes() - o_network_iface.get_tx_bytes_prev()
                s_avg_tx_mbytes_second = ("%.3f" % (i_avg_tx_bytes_second / 1024 / 1024))
                #i_avg_rx_bytes_second = o_network_iface.get_avg_rx_bytes()
                i_avg_rx_bytes_second = o_network_iface.get_rx_bytes() - o_network_iface.get_rx_bytes_prev()
                s_avg_rx_mbytes_second = ("%.3f" % (i_avg_rx_bytes_second / 1024 / 1024))

                s_line = s_network_iface_padded + " " + s_operstate.rjust(5, ' ') + " " + s_speed.rjust(7, ' ') + " " + s_main_ip.rjust(15, ' ')
                s_line_color = s_network_iface_padded + " "
                if s_operstate == "down":
                    s_line_color = s_line_color + self.o_screenutils.color_alert(s_operstate.rjust(5, ' '))
                elif s_operstate == "up":
                    s_line_color = s_line_color + self.o_screenutils.color_ok(s_operstate.rjust(5, ' '))
                else:
                    s_line_color = s_line_color + s_operstate.rjust(5, ' ')
                s_line_color = s_line_color + " " + s_speed.rjust(7, ' ') + " " + s_main_ip.rjust(15, ' ')

                if i_terminal_width > 90:
                    # Separations space plus " |  " (2 spaces for more capacity)
                    s_line = s_line + " |" + str(s_tx_data_gib_2decimals).rjust(len(s_title_tx_gib) + 2, ' ')
                    s_line = s_line + " |" + str(s_rx_data_gib_2decimals).rjust(len(s_title_rx_gib) + 2, ' ')

                    s_line_color = s_line_color + self.o_screenutils.color_label(" |") + str(s_tx_data_gib_2decimals).rjust(len(s_title_tx_gib) + 2, ' ')
                    s_line_color = s_line_color + self.o_screenutils.color_label(" |") + str(s_tx_data_gib_2decimals).rjust(len(s_title_rx_gib) + 2, ' ')
                if i_terminal_width > 115:
                    s_line = s_line + " " + str(s_avg_tx_mbytes_second).rjust(len(s_title_tx_mib_sec), ' ')
                    s_line = s_line + " " + str(s_avg_rx_mbytes_second).rjust(len(s_title_rx_mib_sec), ' ')

                    s_line_color = s_line_color + " " + str(s_avg_tx_mbytes_second).rjust(len(s_title_tx_mib_sec), ' ')
                    s_line_color = s_line_color + " " + str(s_avg_tx_mbytes_second).rjust(len(s_title_rx_mib_sec), ' ')
                if i_terminal_width > 125:
                    s_line = s_line + " " + str(i_collisions).rjust(len(s_title_collisions), ' ')
                    s_line = s_line + " " + str(i_tx_total_errors).rjust(len(s_title_tx_errors), ' ')
                    s_line = s_line + " " + str(i_rx_total_errors).rjust(len(s_title_rx_errors), ' ')

                    s_line_color = s_line_color + " " + str(i_collisions).rjust(len(s_title_collisions), ' ')
                    s_line_color = s_line_color + " " + str(i_tx_total_errors).rjust(len(s_title_tx_errors), ' ')
                    s_line_color = s_line_color + " " + str(i_rx_total_errors).rjust(len(s_title_rx_errors), ' ')

                o_iface_stringcolor = StringColorDO(s_line, s_line_color)

                # self.o_screenutils.vprint_aligned_to_right_y(i_x_margin=0, i_y=i_y, s_text=s_line)
                self.o_screenutils.vprint_in_x_y_stringcolor(i_x=i_x, i_y=i_y, o_stringcolor=o_iface_stringcolor)
                i_y = i_y + 1

            # The block ends here
            self.i_block_network_y_position_last = i_y - 1

    def get_mem_line(self, i_width_available, s_swappiness, s_percent_free_bw, s_percent_free_color, s_memfree_gb,
                           s_percent_available_color, s_percent_available_bw, s_memavailable_gb, s_memtotal_gb,
                           s_swap_in_use_gb, s_swap_in_use_gb_color, s_swaptotal_gb):
        """
        Prepares the information line for Memory and Swap
        :param i_width_available:
        :param i_swappiness:
        :param s_percent_free_bw:
        :param s_percent_free_color:
        :param s_memfree_gb:
        :param s_percent_available_color:
        :param s_percent_available_bw:
        :param s_memavailable_gb:
        :param s_memtotal_gb:
        :param s_swap_in_use_gb:
        :param s_swap_in_use_gb_color:
        :param s_swaptotal_gb:
        :return: s_total_memory_bw, s_total_memory_color
        """

        s_swappiness_in_color = s_swappiness
        i_swappiness = int(s_swappiness)

        if i_swappiness <= 10:
            s_swappiness_in_color = self.o_screenutils.color_ok(s_swappiness)
        elif i_swappiness > 10 and i_swappiness < 30:
            s_swappiness_in_color = self.o_screenutils.color_warning(s_swappiness)
        else:
            s_swappiness_in_color = self.o_screenutils.color_alert(s_swappiness)

        # Test different width, to see if they exceed the capacity.
        # Depending on the amount of memory and swap, this will be longer from system to system
        # Enough width to print the line
        s_total_memory = self.o_screenutils.color_label("Free Mem:") + \
                         " " + s_percent_free_color + \
                         " " + s_memfree_gb + " " + \
                         self.o_screenutils.color_label("Avail:") + \
                         " " + s_percent_available_color + \
                         " " + s_memavailable_gb + " " + \
                         self.o_screenutils.color_label("Total:") + " " + s_memtotal_gb + \
                         " " + self.o_screenutils.color_label("Swap in use:") + \
                         " " + s_swap_in_use_gb_color + " / " + s_swaptotal_gb + \
                         " " + \
                         self.o_screenutils.color_label("Swappiness:") + \
                         " " + s_swappiness_in_color

        s_total_memory_bw = "Free Mem:" + \
                            " " + s_percent_free_bw + \
                            " " + s_memfree_gb + " " + \
                            "Avail:" + \
                            " " + s_percent_available_bw + \
                            " " + s_memavailable_gb + " " + \
                            "Total: " + s_memtotal_gb + \
                            " " + "Swap in use:" + \
                            " " + s_swap_in_use_gb + " / " + s_swaptotal_gb + \
                            " " + \
                            "Swappiness:" + \
                            " " + s_swappiness

        if len(s_total_memory_bw) > i_width_available:
            s_total_memory = self.o_screenutils.color_label("Free Mem:") + \
                             " " + s_percent_free_color + \
                             " " + s_memfree_gb + " " + \
                             self.o_screenutils.color_label("Avail:") + \
                             " " + s_percent_available_color + \
                             " " + s_memavailable_gb + " " + \
                             self.o_screenutils.color_label("T:") + " " + s_memtotal_gb + \
                             " " + self.o_screenutils.color_label("Swap:") + \
                             " " + s_swap_in_use_gb_color + " / " + s_swaptotal_gb + \
                             " " + \
                             self.o_screenutils.color_label("(") + \
                             s_swappiness_in_color + \
                             self.o_screenutils.color_label(")")

            s_total_memory_bw = "Free Mem:" + \
                                " " + s_percent_free_bw + \
                                " " + s_memfree_gb + " " + \
                                "Avail:" + \
                                " " + s_percent_available_bw + \
                                " " + s_memavailable_gb + " " + \
                                "T: " + s_memtotal_gb + \
                                " " + "Swap:" + \
                                " " + s_swap_in_use_gb + " / " + s_swaptotal_gb + \
                                " " + \
                                "(" + \
                                s_swappiness + \
                                ")"

        if len(s_total_memory_bw) > i_width_available:
            s_total_memory = self.o_screenutils.color_label("F Mem:") + \
                             " " + s_percent_free_color + \
                             " " + s_memfree_gb + " " + \
                             self.o_screenutils.color_label("A:") + \
                             " " + s_percent_available_color + \
                             " " + s_memavailable_gb + " " + \
                             self.o_screenutils.color_label("T:") + " " + s_memtotal_gb + \
                             " " + self.o_screenutils.color_label("Swap:") + \
                             " " + s_swap_in_use_gb_color + " / " + s_swaptotal_gb + \
                             " " + \
                             self.o_screenutils.color_label("(") + \
                             s_swappiness_in_color + \
                             self.o_screenutils.color_label(")")

            s_total_memory_bw = "F Mem:" + \
                                " " + s_percent_free_bw + \
                                " " + s_memfree_gb + " " + \
                                "A:" + \
                                " " + s_percent_available_bw + \
                                " " + s_memavailable_gb + " " + \
                                "T: " + s_memtotal_gb + \
                                " " + "Swap:" + \
                                " " + s_swap_in_use_gb + " / " + s_swaptotal_gb + \
                                " " + \
                                "(" + \
                                s_swappiness + \
                                ")"

        # Ultra short
        if len(s_total_memory_bw) > i_width_available:
            s_total_memory = self.o_screenutils.color_label("F M:") + \
                             " " + s_percent_free_color + \
                             " " + s_memfree_gb + " " + \
                             self.o_screenutils.color_label("A:") + \
                             " " + s_percent_available_color + \
                             " " + s_memavailable_gb + " " + \
                             self.o_screenutils.color_label("T:") + " " + s_memtotal_gb + \
                             " " + self.o_screenutils.color_label("S:") + \
                             " " + s_swap_in_use_gb_color + " / " + s_swaptotal_gb + \
                             " " + \
                             self.o_screenutils.color_label("(") + \
                             s_swappiness_in_color + \
                             self.o_screenutils.color_label(")")

            s_total_memory_bw = "F M:" + \
                                " " + s_percent_free_bw + \
                                " " + s_memfree_gb + " " + \
                                "A:" + \
                                " " + s_percent_available_bw + \
                                " " + s_memavailable_gb + " " + \
                                "T: " + s_memtotal_gb + \
                                " " + "S:" + \
                                " " + s_swap_in_use_gb + " / " + s_swaptotal_gb + \
                                " " + \
                                "(" + \
                                s_swappiness + \
                                ")"

        return s_total_memory_bw, s_total_memory

    def render_block_meminfo(self):

        i_terminal_width = self.o_screenutils.get_i_terminal_width()

        self.o_memutils.update_meminfo()

        s_memtotal = self.o_memutils.get_mem_total()
        s_memtotal_bytes, s_memtotal_kb, s_memtotal_mb, s_memtotal_gb, s_memtotal_tb, s_memtotal_pb, s_memtotal_best_units = \
            self.o_stringutils.convert_to_multiple_units(s_memtotal, b_add_units=False, i_decimals=0)
        i_memtotal_kb = int(s_memtotal_kb)

        s_memtotal_gb = self.o_stringutils.convert_kb_to_gb(s_memtotal)

        s_memfree = self.o_memutils.get_memfree()
        s_memfree_bytes, s_memfree_kb, s_memfree_mb, s_memfree_gb, s_memfree_tb, s_memtotal_pb, s_memtotal_best_units = \
            self.o_stringutils.convert_to_multiple_units(s_memfree, b_add_units=False, i_decimals=0)
        i_memfree_kb = int(s_memfree_kb)

        s_memfree_gb = self.o_stringutils.convert_kb_to_gb(s_memfree)

        s_memavailable = self.o_memutils.get_memavailable()
        s_memavailable_bytes, s_memavailable_kb, s_memavailable_mb, s_memavailable_gb, s_memavailable_tb, s_memavailable_pb, s_memavailable_best_units = \
            self.o_stringutils.convert_to_multiple_units(s_memavailable, b_add_units=False, i_decimals=0)
        i_memavailable_kb = int(s_memavailable_kb)

        s_memavailable_gb = self.o_stringutils.convert_kb_to_gb(s_memavailable)

        f_percent_free, i_percent_free, s_percent_decimals, s_percent_free_bw = self.o_stringutils.get_percent(i_memfree_kb,
                                                                                                               i_memtotal_kb,
                                                                                                               b_add_percent=True)

        s_percent_free_color = self.get_mem_percent_free_in_color(i_percent_free, s_percent_free_bw)

        f_percent_available, i_percent_available, s_percent_available_decimals, s_percent_available_bw = self.o_stringutils.get_percent(i_memavailable_kb,
                                                                                                                                        i_memtotal_kb,
                                                                                                                                        b_add_percent=True)

        if i_percent_available < 10:
            s_percent_available_color = self.o_screenutils.color_alert(s_percent_available_bw)
        elif i_percent_available < 50:
            s_percent_available_color = self.o_screenutils.color_warning(s_percent_available_bw)
        else:
            s_percent_available_color = self.o_screenutils.color_ok(s_percent_available_bw)

        s_swaptotal_kib = self.o_memutils.get_swaptotal()
        s_swaptotal_bytes, s_swaptotal_kb, s_swaptotal_mb, s_swaptotal_gb, s_swaptotal_tb, s_swaptotal_pb, s_swaptotal_best_units = \
            self.o_stringutils.convert_to_multiple_units(s_swaptotal_kib, b_add_units=False)
        s_swapfree = self.o_memutils.get_swapfree()

        # Get bytes
        s_swapfree_bytes, s_swapfree_kb, s_swapfree_mb, s_swapfree_gb, s_swapfree_tb, s_swapfree_pb, s_swapfree_best_units = \
            self.o_stringutils.convert_to_multiple_units(s_swapfree, b_add_units=False)

        i_swaptotal_bytes = int(s_swaptotal_bytes)
        i_swapfree_bytes = int(s_swapfree_bytes)
        i_swap_in_use_bytes = i_swaptotal_bytes - i_swapfree_bytes

        # s_swap_in_use_bytes = str(i_swap_in_use_bytes)
        # i_swap_in_use_kib = int(i_swap_in_use_bytes / 1024)
        # s_swap_in_use_kib = str(i_swap_in_use_kib) + " KiB"

        # Get the Units
        s_swaptotal_bytes, s_swaptotal_kb, s_swaptotal_mb, s_swaptotal_gb, s_swaptotal_tb, s_swaptotal_pb, s_swaptotal_best_units = \
           self.o_stringutils.convert_to_multiple_units(s_swaptotal_bytes, b_add_units=True)

        # s_swaptotal_bytes, s_swaptotal_kb, s_swaptotal_mb, s_swaptotal_gb, s_swaptotal_tb = \
        #    self.o_stringutils.convert_to_multiple_units(s_swaptotal_kib, b_add_units=True, i_decimals=2)

        s_swapinuse_in_kb = self.o_memutils.get_swapinuse_in_kb(b_add_unit=False)
        i_swap_in_use_bytes = int(s_swapinuse_in_kb) * 1024
        s_swap_in_use_bytes = str(i_swap_in_use_bytes)

        s_swap_in_use_bytes, s_swap_in_use_kb, s_swap_in_use_mb, s_swap_in_use_gb, s_swap_in_use_tb, s_swap_in_use_pb, s_swap_in_use_best_units = \
            self.o_stringutils.convert_to_multiple_units(s_swap_in_use_bytes, b_add_units=True, i_decimals=2)


        if i_swap_in_use_bytes > 0:
            s_swap_in_use_gb_color = self.o_screenutils.color_alert(s_swap_in_use_gb)
        else:
            s_swap_in_use_gb_color = s_swap_in_use_gb

        s_swappiness = self.o_kernelutils.get_swappiness()

        s_total_memory_bw, s_total_memory = self.get_mem_line(i_width_available=i_terminal_width, s_swappiness=s_swappiness,
                                                              s_percent_free_bw=s_percent_free_bw, s_percent_free_color=s_percent_free_color,
                                                              s_memfree_gb=s_memfree_gb,
                                                              s_percent_available_color=s_percent_available_color, s_percent_available_bw=s_percent_available_bw,
                                                              s_memavailable_gb=s_memavailable_gb, s_memtotal_gb=s_memtotal_gb,
                                                              s_swap_in_use_gb=s_swap_in_use_gb, s_swap_in_use_gb_color=s_swap_in_use_gb_color,
                                                              s_swaptotal_gb=s_swaptotal_gb)

        o_memtotal_stringcolor = StringColorDO(s_total_memory_bw, s_total_memory)

        self.o_screenutils.vprint_in_x_y_stringcolor(0, self.i_block_mem_y_position, o_memtotal_stringcolor)

    def get_mem_percent_free_in_color(self, i_percent_free, s_percent_free_bw):
        if i_percent_free < 10:
            s_percent_free_color = self.o_screenutils.color_alert(s_percent_free_bw)
        elif i_percent_free < 50:
            s_percent_free_color = self.o_screenutils.color_warning(s_percent_free_bw)
        else:
            s_percent_free_color = self.o_screenutils.color_ok(s_percent_free_bw)

        return s_percent_free_color

    def render_block_processes(self, i_page_processes):
        """
        Prints in the virtual screen the line indicating:
        Total Processes: x Running: y

        And several lines down, the process list.
        """
        i_position = self.i_block_processes_title_y_position

        i_num_processes = self.o_processesutils.get_num_current_processes_in_dict()
        i_num_running_processes = self.o_processesutils.get_num_processes_running()
        s_num_running_processes = str(i_num_running_processes)

        i_core_count = self.o_cpuutils.get_core_count()

        if i_num_running_processes < i_core_count:
            s_color_running_processes = self.o_screenutils.color_ok(s_num_running_processes)
        elif i_num_running_processes == i_core_count:
            s_color_running_processes = self.o_screenutils.color_warning(s_num_running_processes)
        else:
            s_color_running_processes = self.o_screenutils.color_alert(s_num_running_processes)

        s_processes_color = self.o_screenutils.color_label("Total Processes:") + \
                            " " + str(i_num_processes) + \
                            " " + \
                            self.o_screenutils.color_label("Running:") + \
                            " " + s_color_running_processes
        s_processes_bw = "Total Processes:" + \
                         " " + str(i_num_processes) + \
                         " " + \
                         "Running:" + \
                         " " + s_num_running_processes

        o_processes_stringcolor = StringColorDO(s_processes_bw, s_processes_color)

        self.o_screenutils.vprint_in_x_y_stringcolor(0, i_position, o_processes_stringcolor)

        #self.o_screenutils.vprint_in_x_y(0, i_position, "Total Processes: " + str(i_num_processes))

        # PID Process Name
        i_position = self.i_block_processes_processes_y_position - 1
        s_processes_title_color = self.o_screenutils.color_label("PID".rjust(self.o_processesutils.i_length_max_pid)) + " " + \
                                  self.o_screenutils.color_label("Process Name")
        s_processes_title_bw = "PID".rjust(self.o_processesutils.i_length_max_pid) + " " + "Process Name"
        o_processes_title_stringcolor = StringColorDO(s_processes_title_bw, s_processes_title_color)
        self.o_screenutils.vprint_in_x_y_stringcolor(0, i_position, o_processes_title_stringcolor)

        # List of Processes (actual window of processes)
        i_position = self.i_block_processes_processes_y_position

        i_height_for_processes = self.get_height_for_processes(i_position)

        d_s_processes = self.o_processesutils.render_processes_as_dictionary(i_height_for_processes,
                                                                             i_page_processes,
                                                                             i_height_for_processes,
                                                                             i_max_len=self.i_display_max_len_process_name)

        for s_key in d_s_processes:
            self.o_screenutils.vprint_in_x_y(0, i_position, d_s_processes[s_key])
            i_position = i_position + 1

    def render_block_free_space(self):
        i_y = self.i_block_network_y_position_last + 2

        s_title_type = "Type"
        s_title_size_gib = "Size GiB"
        s_title_free_gib = "Free GiB"
        s_title_inodes = "Free inodes"
        s_title_fds = "File Descriptors in use"

        s_title_free_space_root_bw = "Path" + " " + s_title_type + " " + s_title_size_gib + " " + s_title_free_gib + " " + s_title_inodes
        s_title_free_space_root_color = self.o_screenutils.color_label(s_title_free_space_root_bw)
        o_title_free_space_root_stringcolor = StringColorDO(s_title_free_space_root_bw, s_title_free_space_root_color)

        self.o_screenutils.vprint_aligned_to_right_stringcolor(i_x_margin=0,
                                                               i_y=i_y,
                                                               o_stringcolor=o_title_free_space_root_stringcolor)
        i_y += 1

        i_total_space_gib, i_free_space_gib, s_total_space_gib, s_free_space_gib = self.o_osutils.get_total_and_free_space_in_gib(s_folder='/', b_add_units=False)
        i_total_inodes, i_free_inodes, f_free_inodes_percent = self.o_osutils.get_inodes_in_use_and_free('/')
        s_free_inodes_percent = (("%.2f" % f_free_inodes_percent) + "%").rjust(len(s_title_inodes), ' ')
        b_success_type, s_type = self.o_osutils.get_partition_type_for_mountpoint('/')
        # We limit to max x (len title) characters to avoid breaking the formatting
        s_type_formatted = s_type.rjust(len(s_title_type), ' ')[0:len(s_title_type)]
        s_total_space_gib_formatted = s_total_space_gib.rjust(len(s_title_size_gib), ' ')
        s_line_bw = "/   " + " " + s_type_formatted + " " + s_total_space_gib_formatted + " "
        s_line_color = s_line_bw
        s_free_space_gib_formatted = s_free_space_gib.rjust(len(s_title_free_gib), ' ')
        s_line_bw = s_line_bw + s_free_space_gib_formatted + " "
        if i_free_space_gib <= 1:
            s_line_color = s_line_color + self.o_screenutils.color_alert(s_free_space_gib_formatted)
        elif i_free_space_gib <= 10:
            s_line_color = s_line_color + self.o_screenutils.color_warning(s_free_space_gib_formatted)
        else:
            s_line_color = s_line_color + self.o_screenutils.color_ok(s_free_space_gib_formatted)

        s_line_color = s_line_color + " "

        s_line_bw = s_line_bw + s_free_inodes_percent
        if f_free_inodes_percent < 5:
            s_line_color = s_line_color + self.o_screenutils.color_alert(s_free_inodes_percent)
        elif f_free_inodes_percent < 25:
            s_line_color = s_line_color + self.o_screenutils.color_warning(s_free_inodes_percent)
        else:
            s_line_color = s_line_color + self.o_screenutils.color_ok(s_free_inodes_percent)

        o_line_stringcolor = StringColorDO(s_line_bw, s_line_color)
        self.o_screenutils.vprint_aligned_to_right_stringcolor(i_x_margin=0,
                                                               i_y=i_y,
                                                               o_stringcolor=o_line_stringcolor)

        i_y += 1

        b_success_max_fds, s_max_fds = self.o_processesutils.get_kernel_max_fds()
        if b_success_max_fds is False or s_max_fds == "":
            s_max_fds = "?"

        s_line_bw = s_title_fds + ":"
        s_line_color = self.o_screenutils.color_label(s_line_bw)
        s_line_bw = s_line_bw + " " + str(self.o_processesutils.i_fds) + " / " + s_max_fds
        s_line_color = s_line_color + " " + str(self.o_processesutils.i_fds) + " / " + s_max_fds

        o_line_stringcolor = StringColorDO(s_line_bw, s_line_color)
        self.o_screenutils.vprint_aligned_to_right_stringcolor(i_x_margin=0,
                                                               i_y=i_y,
                                                               o_stringcolor=o_line_stringcolor)

        i_y += 1

        self.i_block_free_space_y_position_last = i_y - 1

    def render_block_repeated_processes(self):
        i_y = self.i_block_free_space_y_position_last + 2
        i_display_processes = self.get_height_for_processes(i_y+1)
        i_displayed = 0

        s_title_processes_repeated_bw = "Most repeated binaries"
        s_title_processes_repeated_color = self.o_screenutils.color_label("Most repeated binaries")
        o_title_processes_repeated_stringcolor = StringColorDO(s_title_processes_repeated_bw, s_title_processes_repeated_color)

        self.o_screenutils.vprint_aligned_to_right_stringcolor(i_x_margin=0,
                                                               i_y=i_y,
                                                               o_stringcolor=o_title_processes_repeated_stringcolor)
        i_y += 1

        a_processes_sorted_by_max_seen_times = self.o_processesutils.get_processes_sorted_by_max_seen_times()
        for o_process_repeated in a_processes_sorted_by_max_seen_times:
            s_line = o_process_repeated.get_name()[0:self.i_display_max_len_in_repeated_binary_name].ljust(self.i_display_max_len_in_repeated_binary_name, ' ') + \
                     " " + str(o_process_repeated.get_seen_times()).rjust(5, ' ')
            self.o_screenutils.vprint_aligned_to_right_y(i_x_margin=0, i_y=i_y, s_text=s_line)
            i_y += 1
            i_displayed += 1
            if i_displayed >= i_display_processes:
                break

    def render_block_bottom(self):
        s_msg = "Press CTRL + C to interrupt at any moment..." + \
                " Rendering at: " + \
                str(self.o_screenutils.get_terminal_width_to_use()) + \
                "x" + \
                str(self.o_screenutils.get_terminal_height_to_use())
        self.o_screenutils.vprint_in_bottom(s_msg)

    # Helpers for processes block
    def get_height_for_processes(self, i_position):
        """
        Returns the amount of height available to processes
        :param: i_position: int:
        :return: int: Height available for processes.
        """
        i_terminal_height = self.o_screenutils.get_terminal_height_to_use()
        i_height_for_processes = i_terminal_height - i_position - 2

        if i_height_for_processes < 10:
            i_height_for_processes = 10

        return i_height_for_processes

    def get_i_block_processes_y_position(self):
        return self.i_block_processes_title_y_position

    def get_processes_per_page(self):
        return self.get_height_for_processes(self.i_block_processes_processes_y_position)
