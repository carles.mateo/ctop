#
# String Utils
#
# Author: Carles Mateo
# Creation Date: 2019-12-04 15:29 IST
# Description: Class to deal with Files and Directories
#

import os
import glob


class StringUtils:

    def convert_string_to_integer(self, s_amount):
        """
        Convert a string to Integer, checking for errors.
        :param s_amount:
        :return: Boolean Success/False, Integer number if success or 0 otherwise
        """

        b_success = True
        i_value = 0

        if s_amount == "":
            i_value = 0
        else:
            try:
                i_value = int(s_amount)
            except:
                b_success = False
                i_value = 0

        return b_success, i_value

    def convert_kb_to_gb(self, s_amount, b_add_units=True):
        """
        Converts a String that is in KB, with the units, like: 24kb to a String with two decimals
        representing GB. We divide by 1024, not by 1000.
        :param s_amount:
        :param b_add_units:
        :return: String in GB without the unit or String in GB with final "GB"
        """

        s_amount = s_amount.strip()

        if len(s_amount) < 2:
            if b_add_units is True:
                return "0GB"
            else:
                return "0"

        s_unit = s_amount[-2:].upper()

        if s_unit == "KB":
            s_value = s_amount.replace(' ', '')
            # Remove "KB"
            s_value = s_value[0:-2]
            f_value = float(s_value)
            i_value = int(f_value)
            f_value_mb = i_value/1024.00
            f_value_gb = f_value_mb/1024.00
            # s_value = str(f_value_gb)[0:4]
            s_value = ("%.2f" % f_value_gb)

            if b_add_units is True:
                return s_value + "GB"
            else:
                return s_value

        # @TODO: Fix this case

        # Unknown
        return s_amount

    def convert_bytes_to_best_size(self, i_bytes):
        """
        Converts a number of bytes to the highest values, and adds the units
        :param i_bytes:
        :return: s_biggest_suggested
        """
        s_value_bytes, s_value_kb, s_value_mb, s_value_gb, s_value_tb, s_value_pb, s_biggest_suggested = self.convert_bytes_to_multiple_units(
            i_bytes)

        return s_biggest_suggested

    def convert_bytes_to_multiple_units(self, i_amount):
        s_value_bytes, s_value_kb, s_value_mb, s_value_gb, s_value_tb, s_value_pb, s_biggest_suggested = self.convert_to_multiple_units(str(i_amount), b_add_units=True)
        return s_value_bytes, s_value_kb, s_value_mb, s_value_gb, s_value_tb, s_value_pb, s_biggest_suggested

    def convert_to_multiple_units(self, s_amount, b_add_units=False, i_decimals=2, b_remove_decimals_if_ge_1000=True):
        """
        Getting the bytes or Kbytes, we return multiple units.
        :param s_amount:
        :param b_add_units: If we suffix the units, like GiB
        :param i_decimals: Number of decimal positions
        :param b_remove_decimals_if_ge_1000: Will remove decimals part if the unit is greater or equal to 1000
        :return: String in Bytes, String in KBytes, String in MBytes, String in GBytes, String in TBytes,
                 str in PBytes, biggest suggested unit
        """
        # @TODO: Finish this to support multi unit

        # Init values
        s_value_bytes = "0"
        i_value_bytes = 0
        s_value_kb = "0"
        i_value_kb = 0
        s_value_mb = "0"
        f_value_mb = 0
        s_value_gb = "0"
        f_value_gb = 0
        s_value_tb = "0"
        f_value_tb = 0
        s_value_pb = "0"
        f_value_pb = 0

        s_biggest_suggested = ""

        s_amount = s_amount.strip()

        s_unit = s_amount[-2:].upper()
        s_unit3 = s_amount[-3:].upper()

        s_mask = "{:." + str(i_decimals) + "f}"  # "{:.2f}"

        if s_unit == "KB" or s_unit3 == "KIB":
            s_value = s_amount.replace(' ', '')
            # Remove "KB"
            if s_unit == 'KB':
                s_value = s_value[0:-2]
            elif s_unit3 == 'KIB':
                s_value = s_value[0:-3]
            i_value = int(s_value)
            i_value_bytes = i_value * 1024
            # Convert to Bytes
            s_amount = str(i_value_bytes)

        if s_amount.isdigit() is True:
            # Is Bytes
            i_value = int(s_amount)
            i_value_bytes = i_value
            s_value_bytes = str(i_value_bytes)
            i_value_kb = i_value_bytes / 1024.00
            s_value_kb = s_mask.format(i_value_kb)
            f_value_mb = i_value_bytes / 1024 / 1024.00
            s_value_mb = s_mask.format(f_value_mb)
            f_value_gb = i_value_bytes / 1024 / 1024 / 1024.00
            s_value_gb = s_mask.format(f_value_gb)
            f_value_tb = i_value_bytes / 1024 / 1024 / 1024 / 1024.00
            s_value_tb = s_mask.format(f_value_tb)
            f_value_pb = i_value_bytes / 1024 / 1024 / 1024 / 1024 / 1024.00
            s_value_pb = s_mask.format(f_value_pb)

        if b_remove_decimals_if_ge_1000 is True:
            if i_value_kb >= 1000:
                i_value_kb = int(i_value_kb)
                s_value_kb = str(i_value_kb)
            if f_value_mb >= 1000:
                f_value_mb = int(f_value_mb)
                s_value_mb = str(f_value_mb)
            if f_value_gb >= 1000:
                f_value_gb = int(f_value_gb)
                s_value_gb = str(f_value_gb)
            if f_value_tb >= 1000:
                f_value_tb = int(f_value_tb)
                s_value_tb = str(f_value_tb)
            if f_value_pb >= 1000:
                f_value_pb = int(f_value_pb)
                s_value_pb = str(f_value_pb)
        else:
            s_value_mb = s_mask.format(f_value_mb)
            s_value_gb = s_mask.format(f_value_gb)
            s_value_tb = s_mask.format(f_value_tb)
            s_value_pb = s_mask.format(f_value_pb)

        if b_add_units is True:
            if i_value_bytes > 1 or i_value_bytes == 0:
                s_value_bytes = s_value_bytes + "Bytes"
            elif i_value_bytes == 1:
                s_value_bytes = s_value_bytes + "Byte"

            s_value_kb = s_value_kb + "KB"
            s_value_mb = s_value_mb + "MB"
            s_value_gb = s_value_gb + "GB"
            s_value_tb = s_value_tb + "TB"
            s_value_pb = s_value_pb + "PB"

        if f_value_pb >= 1:
            s_biggest_suggested = s_value_pb
        elif f_value_tb >= 1:
            s_biggest_suggested = s_value_tb
        elif f_value_gb >= 1:
            s_biggest_suggested = s_value_gb
        elif f_value_mb >= 1:
            s_biggest_suggested = s_value_mb
        elif i_value_kb >= 1:
            s_biggest_suggested = s_value_kb
        else:
            s_biggest_suggested = s_value_bytes

        return s_value_bytes, s_value_kb, s_value_mb, s_value_gb, s_value_tb, s_value_pb, s_biggest_suggested

    def get_dict_value(self, s_key, d_dict, m_default=""):
        """
        Returns the value of the dictionary for key or default
        :param s_key:
        :param d_dict:
        :param m_default:
        :return: Boolean found or not, value
        """
        if s_key in d_dict:
            return True, d_dict[s_key]

        return False, m_default

    def get_bytes_per_second(self, i_bytes, f_seconds):
        """
        Returns the speed in bytes/seconds
        :param i_bytes:
        :param f_seconds:
        :return: i_speed_bytes_per_second, s_biggest_suggested
        """

        if f_seconds == 0:
            # If 0 seconds were sent as interval, we consider that it's 1
            # This way we avoid a Division by zero
            f_seconds = 1

        i_speed_bytes_per_second = int(i_bytes / f_seconds)
        s_value_bytes, s_value_kb, s_value_mb, s_value_gb, s_value_tb, s_value_pb, s_biggest_suggested = \
            self.convert_bytes_to_multiple_units(i_speed_bytes_per_second)

        return i_speed_bytes_per_second, s_biggest_suggested

    def get_percent(self, i_partial, i_total, b_add_percent=True):
        """
        Gets the percent from two integers.
        Is designed to work with Python 3 and Python 2.
        Note: Corner case i_total == 0 considered as 100%.
        """

        if i_total == 0:
            # We avoid division by zero
            f_percent_free = 0
            i_percent_free = 0
            s_percent_decimals = "100"
            s_percent_no_decimals = "100"
        else:
            f_percent_free = (100.0 * i_partial) / i_total
            i_percent_free = int(f_percent_free)
            s_percent_no_decimals = str(i_percent_free)
            s_percent_decimals = str(round(f_percent_free, 2))

        if b_add_percent is True:
            s_percent_no_decimals = s_percent_no_decimals + "%"
            s_percent_decimals = s_percent_decimals + "%"

        return f_percent_free, i_percent_free, s_percent_decimals, s_percent_no_decimals

    def get_time_and_bytes_per_second(self, i_bytes, f_time_start, f_time_finish):
        """
        Calculates the time between start and end, and the bytes per second, and the best unit for it
        :param i_bytes:
        :param f_time_start:
        :param f_time_finish:
        :return: f_time, s_time, i_bytes_per_second, s_best_unit
        """

        f_time = f_time_finish - f_time_start
        s_time = str(round(f_time, 2))

        i_bytes_per_second, s_best_unit = self.get_bytes_per_second(i_bytes, f_time)

        return f_time, s_time, i_bytes_per_second, s_best_unit

    def convert_integer_to_string_thousands(self, i_number, s_thousand_sep=","):
        """
        Puts thousand separator
        :param i_number:
        :param s_thousand_sep:
        :return: s_number_formatted
        """

        s_number = str(i_number)
        s_number_formatted = ""
        i_pos = 0
        for i_index in range(len(s_number)-1, -1, -1):
            i_pos = i_pos + 1
            if i_pos % 3 == 1 and i_pos > 1 and s_number[i_index] != "-":
                s_number_formatted = s_number[i_index] + s_thousand_sep + s_number_formatted
            else:
                s_number_formatted = s_number[i_index] + s_number_formatted

        return s_number_formatted
