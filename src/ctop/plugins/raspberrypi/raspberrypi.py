#
# By Carles Mateo
# This plugin is just a sample to extend the functionality of CTOP.
# It is created for Raspberrys with the Christmas Tree LEDs.
#
# In order to make LED lights, follow the instructions in here:
#
# https://blog.carlesmateo.com/2021/02/16/raspberry-pi-solving-the-problem-gpio-setupself-number-gpio-in-self-gpio_pull_upsself-_pull-runtimeerror-not-running-on-a-rpi-in-ubuntu-20-04lts/
#
# This Plugin uses file tree.py from https://github.com/ThePiHut/rgbxmastree#rgbxmastree
# You should download it and put in this same directory plugins/raspberrypi/
#
# For playing sound do:
# sudo pip3 install playsound
#
# You will need to upload your sounds to this directory:
# systemishealthy.mp3
# warningcpu80percent.mp3
#
# I bought the Christmas LED tree on:
# https://thepihut.com/products/3d-rgb-xmas-tree-for-raspberry-pi
# Seems cheaper, simplest version can be acquired:
# https://thepihut.com/products/christmas-tree-programmable-kit
#
# To run this plugin execute:
# ./ctop.py -p=raspberrypi
#


from plugins.raspberrypi.tree import RGBXmasTree
try:
    from playsound import playsound
    # Alternative way
    # from pydub import AudioSegment
    # from pydub.playback import play
    # from pydub.utils import get_player_name
except:
    pass


class Raspberrypi:

    s_plugin_id = "raspberrypi"
    s_plugin_name = "Raspberry Pi LED plugin"
    s_plugin_version = "0.1"
    s_plugin_author = "Carles Mateo"
    s_plugin_url = ""

    b_led_work = False
    o_led_tree = None

    # Parameters passed by command line
    h_s_params = {}

    # Parameters in the config file
    h_s_config_values = {}

    i_last_time_played_sound = 0
    # After having played the sound wait for x seconds
    i_cooldwon_sound_default = 30
    # To avoid repeating the same message every 30 seconds
    s_last_played_file = ""
    s_file_systemishealthy = ""
    s_file_warningcpu80percent = ""
    h_preload = {}

    o_ctop = None

    def __init__(self, h_s_params, h_s_config_values, o_ctop):
        self.h_s_params = h_s_params
        self.h_s_config_values = h_s_config_values
        self.o_ctop = o_ctop

        # Instance variable initialized as None
        self.o_led_tree = None

        self.s_last_played_file = ""

        s_path = self.o_ctop.get_own_path() + "plugins/raspberrypi/"
        self.s_file_systemishealthy = s_path + "thesystemishealthy.mp3"
        self.s_file_warningcpu80percent = s_path + "warningcpu80percent.mp3"

        # Preload the sounds
        self.h_preload = {}
        # self.h_preload[self.s_file_systemishealthy] = AudioSegment.from_mp3(self.s_file_systemishealthy)
        # self.h_preload[self.s_file_warningcpu80percent] = AudioSegment.from_mp3(self.s_file_warningcpu80percent)


        self.b_led_work = True
        try:
            self.o_ctop.o_screenutils.p("Initializing LED")
            self.o_led_tree = RGBXmasTree()

            self.initialize_leds()

            # Register hooks
            if "ActionEachLoop" in h_s_config_values:
                # Registering Hook
                self.o_ctop.register_hook_action_each_loop(o_class=self, s_method=h_s_config_values["ActionEachLoop"])

            if "ActionExit" in h_s_config_values:
                # Registering Hook when CTOP exits
                self.o_ctop.register_hook_action_exit(o_class=self, s_method=h_s_config_values["ActionExit"])

        except:
            self.b_led_work = False
            self.o_ctop.ctop_exit("LED Initializing failed. You should run CTOP in a Raspberry Pi, as root", self.o_ctop.i_EXIT_CODE_ERROR_PLUGIN)

    # =====================
    #  Common hooks
    # =====================

    def default_start(self):
        """
        Starting method by default.
        It takes care of the parameters sent.
        :return: boolean
        """

        b_success = True

        h_s_actions = self.get_available_actions()

        for s_param in self.h_s_params:
            b_success, s_function = self.o_ctop.o_stringutils.get_dict_value(s_param, h_s_actions, m_default="")
            if b_success is True:
                _default_action = self.o_ctop.o_importutils.get_method_handler(o_class=self, s_method=s_function)
                b_success_command, s_command = _default_action()
                b_success = b_success_command

        return b_success

    def get_available_actions(self):
        """
        Return the Dictionary of Parameters and Actions.
        Samples:
        python3 ctop.py -ls=1 -p=raspberrypi
        :return: h_s_actions
        """

        h_s_actions = {'-ph': "get_help",
                       '--plugin-help': "get_help",
                       '-ls': "get_led_sensors",
                       '--led-sensors': "get_led_sensors"}

        return h_s_actions

    def get_plugin_id(self):
        return self.s_plugin_id

    def get_plugin_version(self):
        return self.s_plugin_id, self.s_plugin_name, self.s_plugin_version, self.s_plugin_author, self.s_plugin_url

    def get_help(self):
        s_help = "RSync can rsync your source to your target." + \
                 "It can also show the command that it will use with the parameter:" + \
                 "-gc or --get-command-to-backup"

        return s_help

    def check_dependencies(self):
        # This is called when loaded to make sure all the dependencies are met
        return True

    # =====================
    #  Particular hooks
    # =====================

    def check_sensors(self):
        """
        Triggered every 1 second
        """

        b_success = True

        f_cpu_use, s_use_formated = self.o_ctop.o_cpuutils.get_cpu_use()

        if f_cpu_use <= 25:
            self.set_led_colors([0, 1, 0])
            self.play_sound_if_cooldown_passed(self.s_file_systemishealthy)
        elif f_cpu_use > 25 and f_cpu_use <= 50:
            self.set_led_colors([1, 1, 1])
        elif f_cpu_use > 50 and f_cpu_use <= 80:
            self.set_led_colors([0, 1, 1])
        elif f_cpu_use >= 80:
            self.set_led_colors([1, 0, 0])
            self.play_sound_if_cooldown_passed(self.s_file_warningcpu80percent)

        return b_success


    # Internal use Methods

    def initialize_leds(self):
        self.set_led_colors([1, 1, 1])
        self.o_ctop.o_systemutils.sleep(0.5)
        self.set_led_colors([1, 0, 0])
        self.o_ctop.o_systemutils.sleep(0.5)
        self.set_led_colors([0, 0, 1])
        self.o_ctop.o_systemutils.sleep(0.5)
        self.set_led_colors([0, 1, 0])
        self.o_ctop.o_systemutils.sleep(0.5)
        self.turn_leds_off()

    def set_led_colors(self, a_colors=[0, 1, 0]):
        self.o_led_tree.color = a_colors

    def turn_leds_off(self):
        self.set_led_colors([0, 0, 0])

    def get_seconds_since_last_time_played_a_sound(self):
        s_now = self.o_ctop.o_datetimeutils.get_unix_epoch()
        i_now = int(s_now)

        return i_now - self.i_last_time_played_sound

    def update_last_time_played_a_sound(self):
        s_now = self.o_ctop.o_datetimeutils.get_unix_epoch()
        i_now = int(s_now)

        self.i_last_time_played_sound = i_now

    def play_sound_if_cooldown_passed(self, s_soundfile):
        try:
            if self.get_seconds_since_last_time_played_a_sound() >= self.i_cooldwon_sound_default and self.s_last_played_file != s_soundfile:
                # It's Ok to Play
                playsound(s_soundfile)

                # Alternative way
                # music = self.h_preload[s_soundfile]
                # play the file
                # play(music)
                self.update_last_time_played_a_sound()
                self.s_last_played_file = s_soundfile
        except:
            pass
